package com.example.natkobiscan.zoovrt.activities.admin;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.natkobiscan.zoovrt.R;
import com.example.natkobiscan.zoovrt.activities.ActivityUtility;
import com.example.natkobiscan.zoovrt.activities.HomeActivity;
import com.example.natkobiscan.zoovrt.dao.DAOProvider;
import com.example.natkobiscan.zoovrt.model.database.Guardian;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalSpecies;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity used for viewing and editing specific guardian's individuals to the administrator.
 *
 * @author Natko Bišćan
 * @author Domagoj Pluščec
 * @author Vedran Ivanušić
 * @version v2.0, 7.1.2017.
 */
public class AddGuardianActivity extends AppCompatActivity {

    /**
     * Extras id for transferring guardian instance through intent.
     */
    public static final String SELECTED_GUARDIAN_EDIT_EXTRAS_ID = "edit_guardian";

    /**
     * Current guardian used to display activity.
     */
    private Guardian currGuardian;

    /**
     * Current guardian cares list view.
     */
    private ListView lvCurrCares;
    /**
     * Current guardian cares list view data adapter.
     */
    private ArrayAdapter lvCurrCaresAdapter;
    /**
     * Current guardian cares names used as data for list view.
     */
    private List<String> currCaresNames;
    /**
     * Current cares instances.
     */
    private List<ZOOAnimalSpecies> currCares;

    /**
     * ListView for displaying possible new cares.
     */
    private ListView lvNewCares;
    /**
     * New guardian cares list view data adapter.
     */
    private ArrayAdapter lvNewCaresAdapter;
    /**
     * New guardian cares names used as data for list view.
     */
    private List<String> newCaresNames;
    /**
     * New cares instances.
     */
    private List<ZOOAnimalSpecies> newCares;

    /**
     * Image button for deleting care from current guardian cares.
     */
    private ImageButton ibCareDeleteButton;
    /**
     * Image button for adding care from new guardian cares.
     */
    private ImageButton ibCareAddButton;
    /**
     * Current edit type status. Possible values NO_EDIT, ADD and DELETE.
     */
    private ActivityUtility.EditType currState = ActivityUtility.EditType.NO_EDIT;

    /**
     * New cares list view adapter which enables addition of animal species.
     */
    private AdapterView.OnItemClickListener lvNewCaresListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            if (currState == ActivityUtility.EditType.ADD) {
                ZOOAnimalSpecies selectedCare = newCares.get(position);

                newCares.remove(position);
                newCaresNames.remove(position);
                lvNewCaresAdapter.notifyDataSetChanged();

                currCares.add(selectedCare);
                currCaresNames.add(selectedCare.getImeVrste());
                lvCurrCaresAdapter.notifyDataSetChanged();
                ibCareDeleteButton.setEnabled(true);

                lvNewCares.setBackgroundColor(Color.TRANSPARENT);
                currState = ActivityUtility.EditType.NO_EDIT;

                if (newCares.isEmpty()) {
                    ibCareAddButton.setEnabled(false);
                }

                ActivityUtility.ListUtils.setDynamicHeight(lvCurrCares);
                ActivityUtility.ListUtils.setDynamicHeight(lvNewCares);

                UpdateGuardiansTask updateGuardiansTask = new UpdateGuardiansTask();
                updateGuardiansTask.execute();

            }
        }
    };
    /**
     * Current cares list view adapter which enables deletion of animal species.
     */
    private AdapterView.OnItemClickListener lvCurrCaresListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            if (currState == ActivityUtility.EditType.DELETE) {
                ZOOAnimalSpecies selectedCare = currCares.get(position);

                currCares.remove(position);
                currCaresNames.remove(position);
                lvCurrCaresAdapter.notifyDataSetChanged();

                newCares.add(selectedCare);
                newCaresNames.add(selectedCare.getImeVrste());
                lvNewCaresAdapter.notifyDataSetChanged();
                ibCareAddButton.setEnabled(true);

                lvCurrCares.setBackgroundColor(Color.TRANSPARENT);
                currState = ActivityUtility.EditType.NO_EDIT;

                if (currCares.isEmpty()) {
                    ibCareDeleteButton.setEnabled(false);
                }
                ActivityUtility.ListUtils.setDynamicHeight(lvCurrCares);
                ActivityUtility.ListUtils.setDynamicHeight(lvNewCares);

                UpdateGuardiansTask updateGuardiansTask = new UpdateGuardiansTask();
                updateGuardiansTask.execute();
            }
        }
    };
    /**
     * Listener for adding cares to the guardian.
     */
    private View.OnClickListener careAddListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d("CARE-ADD", "State " + currState.toString());
            if (currState == ActivityUtility.EditType.DELETE) {
                lvCurrCares.setBackgroundColor(Color.TRANSPARENT);
                currState = ActivityUtility.EditType.ADD;
            } else if (currState == ActivityUtility.EditType.NO_EDIT) {
                lvNewCares.setBackgroundColor(Color.GREEN);
                currState = ActivityUtility.EditType.ADD;
            } else if (currState == ActivityUtility.EditType.ADD) {
                lvNewCares.setBackgroundColor(Color.TRANSPARENT);
                currState = ActivityUtility.EditType.NO_EDIT;
            }
        }
    };
    /**
     * Listener for deleting guardian cares.
     */
    private View.OnClickListener careDeleteListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d("CARE-DELETE", "State " + currState.toString());
            if (currState == ActivityUtility.EditType.DELETE) {
                lvCurrCares.setBackgroundColor(Color.TRANSPARENT);
                currState = ActivityUtility.EditType.NO_EDIT;
            } else if (currState == ActivityUtility.EditType.NO_EDIT) {
                lvCurrCares.setBackgroundColor(Color.RED);
                currState = ActivityUtility.EditType.DELETE;
            } else if (currState == ActivityUtility.EditType.ADD) {
                lvNewCares.setBackgroundColor(Color.TRANSPARENT);
                currState = ActivityUtility.EditType.DELETE;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_guardian);

        Intent intent = getIntent();

        if (intent.hasExtra(SELECTED_GUARDIAN_EDIT_EXTRAS_ID)) {
            currGuardian = (Guardian) intent.getExtras().get(SELECTED_GUARDIAN_EDIT_EXTRAS_ID);
            initGui();
        } else {
            Log.d("EditUserActivity", "Incompatible expression");
        }
    }

    /**
     * Method initializes GUI references and initializes GUI components.
     */
    private void initGui() {
        lvCurrCares = (ListView) findViewById(R.id.lvCurrCares);
        lvCurrCares.setOnItemClickListener(lvCurrCaresListener);
        lvNewCares = (ListView) findViewById(R.id.lvNewCares);
        lvNewCares.setOnItemClickListener(lvNewCaresListener);

        ibCareAddButton = (ImageButton) findViewById(R.id.ibCareAddButton);
        ibCareAddButton.setOnClickListener(careAddListener);
        ibCareDeleteButton = (ImageButton) findViewById(R.id.ibCareDeleteButton);
        ibCareDeleteButton.setOnClickListener(careDeleteListener);

        fillCurrCares();
        fillNewCares();
        ActivityUtility.ListUtils.setDynamicHeight(lvCurrCares);

    }

    /**
     * Method fills new cares guardian list view.
     */
    private void fillNewCares() {
        newCaresNames = new ArrayList<>();
        newCares = new ArrayList<>();
        FillNewCaresTask fillNewCaresTask = new FillNewCaresTask();
        fillNewCaresTask.execute();
    }

    /**
     * Method fills current cares guardian list view.
     */
    private void fillCurrCares() {
        currCares = currGuardian.getZivotinjskevrstes();
        if (currCares == null) {
            currCares = new ArrayList<>();
        }

        currCaresNames = new ArrayList<>();
        for (ZOOAnimalSpecies care : currCares) {
            currCaresNames.add(care.getImeVrste());
        }
        lvCurrCaresAdapter = new ArrayAdapter<>(AddGuardianActivity.this, R.layout.simplerow, currCaresNames);
        lvCurrCares.setAdapter(lvCurrCaresAdapter);
        if (currCares.isEmpty()) {
            ibCareDeleteButton.setEnabled(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.itHomeBack:
                Intent intent = new Intent(AddGuardianActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            default:
                super.onOptionsItemSelected(item);
                return true;
        }


    }

    /**
     * Async task that obtains new guardian cares from dao and fills the list view.
     */
    private class FillNewCaresTask extends AsyncTask<Object, Void, List<ZOOAnimalSpecies>> {
        @Override
        protected List<ZOOAnimalSpecies> doInBackground(Object... params) {
            List<ZOOAnimalSpecies> allSpecies = DAOProvider.getAdminDAO().getAnimalSpecies();
            return allSpecies;
        }

        @Override
        protected void onPostExecute(List<ZOOAnimalSpecies> zooAnimalSpecies) {
            for (ZOOAnimalSpecies spec : zooAnimalSpecies) {
                if (currCaresNames.contains(spec.getImeVrste())) {
                    continue;
                }
                newCares.add(spec);
                newCaresNames.add(spec.getImeVrste());
            }
            if (newCares.isEmpty()) {
                ibCareAddButton.setEnabled(false);
            }
            lvNewCaresAdapter = new ArrayAdapter<>(AddGuardianActivity.this, R.layout.simplerow, newCaresNames);
            lvNewCares.setAdapter(lvNewCaresAdapter);
            ActivityUtility.ListUtils.setDynamicHeight(lvNewCares);

        }
    }

    /**
     * Async activity that updates guardian cares.
     */
    private class UpdateGuardiansTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            currGuardian.setZivotinjskevrstes(currCares);
            DAOProvider.getAdminDAO().updateGuardianCares(currGuardian);
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AddGuardianActivity.this, UserListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }
}
