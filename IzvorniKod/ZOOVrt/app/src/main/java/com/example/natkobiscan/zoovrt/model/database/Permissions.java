package com.example.natkobiscan.zoovrt.model.database;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the ovlasti database table.
 */
public class Permissions implements Serializable {

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = -3871045955570132310L;

    /**
     * Permission id.
     */
    private int idOvlasti;

    /**
     * Permission name.
     */
    private String nazivOvlasti;

    /**
     * List of users with given permission.
     */
    private List<User> korisniks;

    /**
     * Java bean empty constructor.
     */
    public Permissions() {
    }


    /**
     * Method adds user to list of users with current permission.
     *
     * @param korisnik user to be added
     * @return reference to given user
     */
    public User addKorisnik(User korisnik) {
        if (korisniks == null) {
            korisniks = new ArrayList<>();
        }
        getKorisniks().add(korisnik);
        korisnik.setOvlasti(this);

        return korisnik;
    }

    /**
     * Getter method for permission id.
     *
     * @return permission id
     */
    public int getIdOvlasti() {
        return this.idOvlasti;
    }

    /**
     * Method that sets permission id.
     *
     * @param idOvlasti permission id
     */
    public void setIdOvlasti(int idOvlasti) {
        this.idOvlasti = idOvlasti;
    }

    /**
     * Getter method that returns users with given permission.
     *
     * @return list of users with current permission
     */
    public List<User> getKorisniks() {
        return this.korisniks;
    }

    /**
     * Method sets list of users with current permission.
     *
     * @param korisniks list of users with current permission
     */
    public void setKorisniks(List<User> korisniks) {
        this.korisniks = korisniks;
    }

    /**
     * Getter method that returns permission name.
     *
     * @return permission name
     */
    public String getNazivOvlasti() {
        return this.nazivOvlasti;
    }

    /**
     * Method sets permission name.
     *
     * @param nazivOvlasti permission name
     */
    public void setNazivOvlasti(String nazivOvlasti) {
        this.nazivOvlasti = nazivOvlasti;
    }

    /**
     * Method removes given user from user list.
     *
     * @param korisnik user to be removed
     * @return removed user
     */
    public User removeKorisnik(User korisnik) {
        getKorisniks().remove(korisnik);
        korisnik.setOvlasti(null);

        return korisnik;
    }

    /**
     * Method obtains user's usernames.
     *
     * @return list of user's usernames
     */
    public List<String> getUsersUsername() {
        List<String> ids = new ArrayList<>();
        for (User user : korisniks) {
            ids.add(user.getKorisnickoIme());
        }
        return ids;
    }

}
