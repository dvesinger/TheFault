package com.example.natkobiscan.zoovrt.model.database;

import java.io.Serializable;

/**
 * Class that models administrator.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
public class Admin extends User implements Serializable {

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = -216663928616285709L;

    /**
     * Constructor that initializes admin with given parameters.
     *
     * @param korisnickoIme username
     * @param lozinka       user's password
     * @param ime           name
     * @param prezime       lastname
     * @param email         user's email adress
     * @param godinaRod     user's birth year
     * @param grad          user's city
     * @param ovlasti       user's permissions
     */
    public Admin(String korisnickoIme, String lozinka, String ime,
                 String prezime, String email, Integer godinaRod, String grad, Permissions ovlasti) {
        super(korisnickoIme, lozinka, ime, prezime, email, godinaRod, grad, ovlasti);
    }

    /**
     * Java bean empty constructor.
     */
    public Admin() {
    }

    /**
     * Constructor that initializes user with general user attributes.
     *
     * @param user user instance
     */
    public Admin(User user) {
        super(user);
    }


}
