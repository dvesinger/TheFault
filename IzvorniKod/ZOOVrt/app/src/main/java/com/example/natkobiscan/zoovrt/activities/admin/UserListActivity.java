package com.example.natkobiscan.zoovrt.activities.admin;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.natkobiscan.zoovrt.R;
import com.example.natkobiscan.zoovrt.activities.HomeActivity;
import com.example.natkobiscan.zoovrt.dao.DAOProvider;
import com.example.natkobiscan.zoovrt.model.database.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity used for displaying user list to the administrator. It offers add guardian functionality and managing user permission functionality.
 *
 * @author Natko Bišćan
 * @author Domagoj Pluščec
 * @version v1.0, 2.1.2017.
 */
public class UserListActivity extends AppCompatActivity {
    /**
     * User's list view.
     */
    private ListView listView;
    /**
     * List view data adapter.
     */
    private ArrayAdapter<String> listAdapter;
    /**
     * User's username's list.
     */
    private List<String> usernameList;

    /**
     * Current user list.
     */
    private List<User> users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        listView = (ListView) findViewById(R.id.userListView);
        UserListTask userListTask = new UserListTask();
        userListTask.execute();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (listView != null) {
            UserListTask userListTask = new UserListTask();
            userListTask.execute();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.itHomeBack:
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            default:
                super.onOptionsItemSelected(item);
                return true;
        }


    }

    /**
     * Async task that obtains current user list and fills list view with obtained users.
     */
    private class UserListTask extends AsyncTask<Object, Void, List<User>> {
        @Override
        protected List<User> doInBackground(Object... params) {
            List<User> userList = DAOProvider.getAdminDAO().getAllUsers();
            users = userList;
            return userList;
        }

        @Override
        protected void onPostExecute(final List<User> users) {
            usernameList = new ArrayList();
            for (User u : users) {
                usernameList.add(u.getKorisnickoIme());
            }
            listAdapter = new ArrayAdapter<>(UserListActivity.this, R.layout.simplerow, usernameList);
            listView.setAdapter(listAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getApplicationContext(), EditUserActivity.class);
                    String username = listView.getItemAtPosition(position).toString();
                    intent.putExtra("username", username);
                    intent.putExtra(EditUserActivity.SELECTED_USER_EDIT_EXTRAS_ID, users.get(position));
                    startActivity(intent);

                }
            });
        }
    }
}
