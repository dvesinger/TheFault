package com.example.natkobiscan.zoovrt.dao;


import com.example.natkobiscan.zoovrt.model.database.ZOOIndividual;
import com.example.natkobiscan.zoovrt.model.database.ZOOInterestingFact;

/**
 * Data access object interface for guardian.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 25.12.2016.
 */
public interface DAOGuardian extends DAO {


    /**
     * Method persists new interesting fact.
     *
     * @param interestingFact interesting fact
     * @param individual      individual for which to add interesting fact
     * @throws DAOException if there was an error while accessing data
     */
    void addInterestingFact(ZOOInterestingFact interestingFact, ZOOIndividual individual)
            throws DAOException;

}
