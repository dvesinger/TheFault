package com.example.natkobiscan.zoovrt.model.database;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the jedinke database table.
 *
 * @author Domagoj Pluščec
 * @version v1.0, 25.12.2016.
 */
public class ZOOIndividual implements Serializable {
    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Individual id.
     */
    private Integer idJedinke;

    /**
     * Arrival date.
     */
    private Date datumDol;

    /**
     * Individual age.
     */
    private Integer dob;

    /**
     * Individual photography.
     */
    private String fotografija;

    /**
     * Individual's name.
     */
    private String ime;

    /**
     * Individuals brith place.
     */
    private String mjestoRod;

    /**
     * Individuals gender.
     */
    private String spol;

    /**
     * Animal species that individual belongs.
     */
    private ZOOAnimalSpecies zivotinjskevrste;

    /**
     * List of adopters.
     */
    private List<User> korisniks;

    /**
     * List of interesting facts.
     */
    private List<ZOOInterestingFact> zanimljivostis;

    /**
     * Java bean empty constructor.
     */
    public ZOOIndividual() {
    }

    /**
     * Method adds interesting fact to the individual.
     *
     * @param zanimljivosti interesting fact
     * @return reference to given interesting fact
     */
    public ZOOInterestingFact addZanimljivosti(ZOOInterestingFact zanimljivosti) {
        getZanimljivostis().add(zanimljivosti);
        zanimljivosti.setJedinke(this);

        return zanimljivosti;
    }

    /**
     * Method obtains individual's date from which it has been in the zoo.
     *
     * @return date
     */
    public Date getDatumDol() {
        return this.datumDol;
    }

    /**
     * Method sets date of arrival.
     *
     * @param datumDol date of arrival
     */
    public void setDatumDol(Date datumDol) {
        this.datumDol = datumDol;
    }

    /**
     * Method obtains individual's age.
     *
     * @return individual's age
     */
    public Integer getDob() {
        return this.dob;
    }

    /**
     * Method sets individual's age.
     *
     * @param dob individual's age
     */
    public void setDob(Integer dob) {
        this.dob = dob;
    }

    /**
     * Method obtains individual's photography URI.
     *
     * @return individual's photography URI
     */
    public String getFotografija() {
        return this.fotografija;
    }

    /**
     * Method sets individual's photography URI.
     *
     * @param fotografija photography URI
     */
    public void setFotografija(String fotografija) {
        this.fotografija = fotografija;
    }

    /**
     * Getter method for individual's id.
     *
     * @return individual's id
     */
    public Integer getIdJedinke() {
        return this.idJedinke;
    }

    /**
     * Setter method for individual's id.
     *
     * @param idJedinke individual's id
     */
    public void setIdJedinke(Integer idJedinke) {
        this.idJedinke = idJedinke;
    }

    /**
     * Getter method for individual's name.
     *
     * @return individual's name
     */
    public String getIme() {
        return this.ime;
    }

    /**
     * Setter method for individual's name.
     *
     * @param ime individual's name
     */
    public void setIme(String ime) {
        this.ime = ime;
    }

    /**
     * Method obtains list of adopters.
     *
     * @return list of adopters
     */
    public List<User> getKorisniks() {
        return this.korisniks;
    }

    /**
     * Setter method for list of adopters.
     *
     * @param korisniks list of adopters
     */
    public void setKorisniks(List<User> korisniks) {
        this.korisniks = korisniks;
    }

    /**
     * Method obtains individual's birth place.
     *
     * @return individual's birth place
     */
    public String getMjestoRod() {
        return this.mjestoRod;
    }

    /**
     * Method sets individual's birth place.
     *
     * @param mjestoRod birth place
     */
    public void setMjestoRod(String mjestoRod) {
        this.mjestoRod = mjestoRod;
    }

    /**
     * Method obtains individual's gender.
     *
     * @return individual's gender
     */
    public String getSpol() {
        return this.spol;
    }

    /**
     * Setter method for individual's gender.
     *
     * @param spol individual's gender
     */
    public void setSpol(String spol) {
        this.spol = spol;
    }

    /**
     * Method obtains list of individual's interesting facts.
     *
     * @return interesting facts
     */
    public List<ZOOInterestingFact> getZanimljivostis() {
        return this.zanimljivostis;
    }

    /**
     * Setter method for list of interesting facts.
     *
     * @param zanimljivostis list of interesting facts
     */
    public void setZanimljivostis(List<ZOOInterestingFact> zanimljivostis) {
        this.zanimljivostis = zanimljivostis;
    }

    /**
     * Method obtains individual's animal species.
     *
     * @return animal species
     */
    public ZOOAnimalSpecies getZivotinjskevrste() {
        return this.zivotinjskevrste;
    }

    /**
     * Method sets species of current animal individual.
     *
     * @param zivotinjskevrste animal species
     */
    public void setZivotinjskevrste(ZOOAnimalSpecies zivotinjskevrste) {
        this.zivotinjskevrste = zivotinjskevrste;
    }

    /**
     * Getter method for animal species id.
     *
     * @return animal species id
     */
    public Integer getZivotinjskevrsteId() {
        return this.zivotinjskevrste.getIdVrste();
    }

    /**
     * Method removes interesting fact from list of interesting facts.
     *
     * @param zanimljivosti interesting fact to be removed
     * @return reference to given interesting fact
     */
    public ZOOInterestingFact removeZanimljivosti(ZOOInterestingFact zanimljivosti) {
        getZanimljivostis().remove(zanimljivosti);
        zanimljivosti.setJedinke(null);

        return zanimljivosti;
    }

}
