package com.example.natkobiscan.zoovrt.activities.visitor;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.example.natkobiscan.zoovrt.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Activity which displays ZOO map.
 *
 * @author Lovro Kunović
 * @author Dunja Vesinger
 * @version v1.0, 2.1.2017.
 */
public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    /**
     * Zoo latitude and longitude.
     */
    private static final LatLng ZOO_LAT_LNG = new LatLng(45.82182, 16.019505);
    /**
     * Zoo marker name.
     */
    private static final String MARKER_NAME = "ZOO Maksimir";
    /**
     * Reference to google map object.
     */
    private GoogleMap mMap;
    /**
     * View map fragment.
     */
    private SupportMapFragment mapFragment;

    /**
     * Google map zoom level.
     */
    private static final int ZOOM_LEVEL = 17;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_static_map);
        setContentView(R.layout.activity_map);


        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.karta);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mMap.addMarker(new MarkerOptions().position(ZOO_LAT_LNG).title(MARKER_NAME)).showInfoWindow();
        mMap.addMarker(new MarkerOptions().position(new LatLng(45.821616, 16.019467)).title("Divlji kunić")).showInfoWindow();
        mMap.addMarker(new MarkerOptions().position(new LatLng(45.821572, 16.019272)).title("Crvena vjeverica")).showInfoWindow();
        mMap.addMarker(new MarkerOptions().position(new LatLng(45.821056, 16.020223)).title("Bengalski tigar")).showInfoWindow();
        mMap.addMarker(new MarkerOptions().position(new LatLng(45.821256, 16.018987)).title("Smeđi kapucin")).showInfoWindow();
        mMap.addMarker(new MarkerOptions().position(new LatLng(45.821292, 16.019223)).title("Brdska gorila")).showInfoWindow();
        mMap.addMarker(new MarkerOptions().position(new LatLng(45.821507, 16.019855)).title("Zelenokrila ara")).showInfoWindow();
        mMap.addMarker(new MarkerOptions().position(new LatLng(45.821137, 16.019642)).title("Velika ušara")).showInfoWindow();
        mMap.addMarker(new MarkerOptions().position(new LatLng(45.820808, 16.019677)).title("Kraljevska kobra")).showInfoWindow();
        mMap.addMarker(new MarkerOptions().position(new LatLng(45.820862, 16.020310)).title("Akvarij")).showInfoWindow();
        mMap.addMarker(new MarkerOptions().position(new LatLng(45.821327, 16.020180)).title("Insektarij")).showInfoWindow();

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ZOO_LAT_LNG, ZOOM_LEVEL));
    }


}
