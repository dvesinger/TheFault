/**
 * Package that contains REST DAO implementation and REST helper classes.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 28.12.2016.
 */
package com.example.natkobiscan.zoovrt.dao.rest;
