package com.example.natkobiscan.zoovrt.dao.rest;

import com.example.natkobiscan.zoovrt.dao.rest.structures.AdoptPair;
import com.example.natkobiscan.zoovrt.dao.rest.structures.InterestingFactTriple;
import com.example.natkobiscan.zoovrt.dao.rest.structures.VisitPair;
import com.example.natkobiscan.zoovrt.model.database.Admin;
import com.example.natkobiscan.zoovrt.model.database.Guardian;
import com.example.natkobiscan.zoovrt.model.database.Permissions;
import com.example.natkobiscan.zoovrt.model.database.User;
import com.example.natkobiscan.zoovrt.model.database.Visitor;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalClass;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalSpecies;
import com.example.natkobiscan.zoovrt.model.database.ZOOIndividual;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * REST web API interface specification.
 *
 * @author Vedran Ivanušić
 * @author Domagoj Pluščec
 * @version v1.0, 30.12.2016.
 */
public interface ApiInterface {

    /**
     * Method fetches user with given username.
     *
     * @param username user's username
     * @return user object or null if there was no user with given username.
     */
    @GET("users/{user}")
    Call<User> getUser(@Path("user") String username);

    /**
     * Method fetches guardian with given username.
     *
     * @param username guardian's username.
     * @return fetched guardian, if there was no guardian with given username it returns null
     */
    @GET("users/{user}")
    Call<Guardian> getGuardian(@Path("user") String username);

    /**
     * Method fetches admin with given username.
     *
     * @param username admin's username.
     * @return fetched admin, if there was no admin with given username it returns null
     */
    @GET("users/{user}")
    Call<Admin> getAdmin(@Path("user") String username);

    /**
     * Method fetches visitor with given username.
     *
     * @param username visitor's username.
     * @return fetched visitor, if there was no visitor with given username it returns null
     */
    @GET("users/{user}")
    Call<Visitor> getVisitor(@Path("user") String username);


    /**
     * Method fetches list of all visitors.
     *
     * @return list of visitors
     */
    @GET("visitors")
    Call<List<Visitor>> getAllVisitors();

    /**
     * Method fetches list of guardians.
     *
     * @return list of guardians
     */
    @GET("guardians")
    Call<List<Guardian>> getAllGuardians();

    /**
     * Method fetches list of all admins.
     *
     * @return list of admins
     */
    @GET("admins")
    Call<List<Admin>> getAllAdmins();

    /**
     * Method creates given user.
     *
     * @param user user to register
     * @return true if the user was created, false otherwise
     */
    @POST("register")
    Call<Boolean> createUser(@Body User user);

    /**
     * Method check if the given user is valid.
     *
     * @param user user to chek login informations
     * @return true if the user is valid, false otherwise
     */
    @POST("login")
    Call<Boolean> checkUser(@Body User user);

    /**
     * Method obtains user's permission status.
     *
     * @param username user's username
     * @return user's permission status
     */
    @GET("users/{user}/permission")
    Call<Permissions> getUserPermission(@Path("user") String username);

    /**
     * Method updates user permission to given permission id.
     *
     * @param username     user's username
     * @param permissionId new permission id
     * @return true if the permission was changed, false otherwise
     */
    @POST("users/{user}/permission_update/{permission_id}")
    Call<Boolean> updateUserPermission(@Path("user") String username, @Path("permission_id") Integer permissionId);

    /**
     * Method obtains all animal classes.
     *
     * @return list of animal classes
     */
    @GET("animal_classes")
    Call<List<ZOOAnimalClass>> getAnimalClasses();

    /**
     * Method obtains animal individual with given id.
     *
     * @param individualId animal individual id
     * @return animal individual
     */
    @GET("individuals/{individual}")
    Call<ZOOIndividual> getAnimalIndividual(@Path("individual") Integer individualId);

    /**
     * Method obtains all animal species.
     *
     * @return list of animal species
     */
    @GET("animal_species")
    Call<List<ZOOAnimalSpecies>> getAnimalSpecies();

    /**
     * Method updates administrator.
     *
     * @param admin new administrator information
     * @return void
     */
    @POST("admin/update")
    Call<Void> updateAdmin(@Body Admin admin);

    /**
     * Method updates guardian.
     *
     * @param guardian new guardian information
     * @return void
     */
    @POST("guardian/update")
    Call<Void> updateGuardian(@Body Guardian guardian);

    /**
     * Method updates visitor.
     *
     * @param visitor new visitor information
     * @return void
     */
    @POST("visitor/update")
    Call<Void> updateVisitor(@Body Visitor visitor);

    /**
     * Method obtains animal species with given id.
     *
     * @param speciesID animal species id
     * @return animal species
     */
    @GET("animal_species/{species_id}")
    Call<ZOOAnimalSpecies> getAnimalSpeciesId(@Path("species_id") Integer speciesID);

    /**
     * Method adopts animal individual.
     *
     * @param adoptInfo adopt informations
     * @return void
     */
    @POST("individuals/adopt")
    Call<Void> adoptIndividual(@Body AdoptPair adoptInfo);

    /**
     * Method visits animal species.
     *
     * @param visitInfo visit informations
     * @return void
     */
    @POST("animal_species/visit")
    Call<Void> visitSpecies(@Body VisitPair visitInfo);

    /**
     * Method deletes animal species.
     *
     * @param species animal speceis to delete
     * @return void
     */
    @POST("animal_species/delete")
    Call<Void> deleteAnimalSpecies(@Body ZOOAnimalSpecies species);

    /**
     * Method creates animal species with given information.
     *
     * @param animalClassId animal class id
     * @param speciesName   animal species name
     * @return true if the creation was successful, false otherwise
     */
    @POST("animal_species/create/{animal_class_id}/{animal_species_name}")
    Call<Boolean> createAnimalSpecies(@Path("animal_class_id") Integer animalClassId, @Path("animal_species_name") String speciesName);

    /**
     * Method creates animal individual with given information.
     *
     * @param animalSpeciesId animal species id
     * @param individualName  animal individual name
     * @return true if the creation was successful, false otherwise
     */
    @POST("animal_individual/create/{animal_species_id}/{animal_individual_name}")
    Call<Boolean> createAnimalIndividual(@Path("animal_species_id") Integer animalSpeciesId, @Path("animal_individual_name") String individualName);

    /**
     * Method deletes animal individual.
     *
     * @param individualDel animal individual to delete
     * @return void
     */
    @POST("animal_individual/delete")
    Call<Void> deleteAnimalIndividual(@Body ZOOIndividual individualDel);

    /**
     * Method deletes given user.
     *
     * @param username user's username
     * @return void
     */
    @DELETE("users/{user}")
    Call<Void> deleteUser(@Path("user") String username);

    /**
     * Method updates current guardian cares.
     *
     * @param username       guardian username
     * @param animalCaresIds list of guardian cares
     * @return true if update was successful, false otherwise
     */
    @POST("guardian/update/{guardianUsername}")
    Call<Boolean> updateGuardianCares(@Path("guardianUsername") String username, @Body List<Integer> animalCaresIds);

    /**
     * Method updates animal species with given information.
     *
     * @param currAnimalSpecies new animal species information
     * @return true if the update was successful, false otherwise
     */
    @POST("animal_species/edit")
    Call<Boolean> updateSpecies(@Body ZOOAnimalSpecies currAnimalSpecies);

    /**
     * Method updates animal individual with given information.
     *
     * @param currIndividual new individual information
     * @return true if the update was successful, false otherwise
     */
    @POST("animal_individual/edit")
    Call<Boolean> updateIndividual(@Body ZOOIndividual currIndividual);

    /**
     * Method adds interesting fact.
     *
     * @param interestingFactInfo interesting fact informations.
     * @return true if the addition was succesful, false otherwise
     */
    @POST("animal_individual/add_interesting_fact")
    Call<Boolean> addInterestingFact(@Body InterestingFactTriple interestingFactInfo);

    /**
     * Method obtains animal class with given id.
     *
     * @param classId animal class id.
     * @return animal class
     */
    @GET("animal_classes/{class_id}")
    Call<ZOOAnimalClass> getAnimalClassId(@Path("class_id") Integer classId);

    /**
     * Method obtains next animal species recommendation.
     *
     * @param username  user's username
     * @param speciesId current visit species id
     * @return next animal species
     */
    @GET("animal_species/recommend/{user_id}/{species_id}")
    Call<ZOOAnimalSpecies> getSpeciesRecommendation(@Path("user_id") String username, @Path("species_id") Integer speciesId);
}
