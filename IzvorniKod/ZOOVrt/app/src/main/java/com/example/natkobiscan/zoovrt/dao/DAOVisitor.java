package com.example.natkobiscan.zoovrt.dao;

import com.example.natkobiscan.zoovrt.model.database.Visitor;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalClass;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalSpecies;
import com.example.natkobiscan.zoovrt.model.database.ZOOIndividual;
import com.example.natkobiscan.zoovrt.model.database.ZOOInterestingFact;

import java.util.List;

/**
 * Data access object interface for visitor.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 25.12.2016.
 */
public interface DAOVisitor extends DAO {

    /**
     * Method obtains all animal classes.
     *
     * @return List of animal classes.
     * @throws DAOException if there was an error while accessing data
     */
    List<ZOOAnimalClass> getAnimalClasses() throws DAOException;


    /**
     * Method obtains interesting fact for an individual if given visitor is an
     * adopter of the individual, empty list otherwise.
     *
     * @param visitor    visitor instance
     * @param individual individual for which to obtain interesting facts
     * @return list of interesting facts
     * @throws DAOException if there was an error while accessing data
     */
    List<ZOOInterestingFact> getInterestingFacts(Visitor visitor, ZOOIndividual individual)
            throws DAOException;

    /**
     * Method obtains animal individual with given id.
     *
     * @param individualID animal individual id
     * @return animal individual
     * @throws DAOException if there was an error while accessing data
     */
    ZOOIndividual getAnimalIndividual(Integer individualID) throws DAOException;

    /**
     * Method adds visit.
     *
     * @param visitor visitor that made visit
     * @param species visited animal species
     */
    void addVisit(Visitor visitor, ZOOAnimalSpecies species);

    /**
     * Method adopts an individual.
     *
     * @param visitor        adopter
     * @param currIndividual adopted individual
     */
    void adoptIndividual(Visitor visitor, ZOOIndividual currIndividual);

    /**
     * Methd obtains animal species visit recommentadion.
     *
     * @param currentUser       visitor
     * @param currAnimalSpecies currently visited animal species
     * @return next animal species to visit
     */
    ZOOAnimalSpecies getSpeciesRecommendation(Visitor currentUser, ZOOAnimalSpecies currAnimalSpecies);
}
