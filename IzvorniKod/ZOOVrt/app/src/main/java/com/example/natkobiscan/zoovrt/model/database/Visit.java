package com.example.natkobiscan.zoovrt.model.database;

import java.io.Serializable;


/**
 * The persistent class for the posjecenost database table.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
public class Visit implements Serializable {
    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Visit's id.
     */
    private int idPosjet;

    /**
     * Species that has been visited.
     */
    private ZOOAnimalSpecies zivotinjskevrste;
    /**
     * Species id that has been visited.
     */
    private int zivotinjskevrsteId;
    /**
     * User that visited the animal species.
     */
    private User korisnik;

    /**
     * Java bean empty constructor.
     */
    public Visit() {
    }

    /**
     * Getter method for visit id.
     *
     * @return visit id
     */
    public int getIdPosjet() {
        return this.idPosjet;
    }

    /**
     * Method sets visit id.
     *
     * @param idPosjet visit id.
     */
    public void setIdPosjet(int idPosjet) {
        this.idPosjet = idPosjet;
    }

    /**
     * Method gets visit's user.
     *
     * @return user
     */
    public User getKorisnik() {
        return this.korisnik;
    }

    /**
     * Method sets visit's user.
     *
     * @param korisnik user that made visit
     */
    public void setKorisnik(User korisnik) {
        this.korisnik = korisnik;
    }

    /**
     * Method obtains visit's user username.
     *
     * @return username
     */
    public String getKorisnikUserName() {
        return this.korisnik.getKorisnickoIme();
    }

    /**
     * Method obtains visited animal species.
     *
     * @return visited animal species
     */
    public ZOOAnimalSpecies getZivotinjskevrste() {
        return this.zivotinjskevrste;
    }

    /**
     * Method sets animal species that has been visited.
     *
     * @param zivotinjskevrste visited animal species
     */
    public void setZivotinjskevrste(ZOOAnimalSpecies zivotinjskevrste) {
        this.zivotinjskevrste = zivotinjskevrste;
    }

    /**
     * Method returns visited animal species id.
     *
     * @return visited animal species id
     */
    public Integer getZivotinjskevrsteId() {

        return zivotinjskevrsteId;
    }

    /**
     * Method sets id for visit's animal species. Used as a mock for XML api.
     *
     * @param id visit's animal species id
     */
    public void setZivotinjskevrsteId(Integer id) {
        zivotinjskevrsteId = id;
    }

}
