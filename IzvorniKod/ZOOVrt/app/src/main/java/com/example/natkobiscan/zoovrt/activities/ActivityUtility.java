package com.example.natkobiscan.zoovrt.activities;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.example.natkobiscan.zoovrt.model.database.User;

import java.util.regex.Matcher;

/**
 * Utility methods used in activites.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 2.1.2017.
 */
public class ActivityUtility {

    /**
     * Utility class constructor.
     */
    private ActivityUtility() {
    }

    /**
     * Computes the widest view in an adapter, best used when you need to wrap_content on a ListView, please be careful
     * and don't use it on an adapter that is extremely numerous in items or it will take a long time.
     *
     * @param context Some context
     * @param adapter The adapter to process
     * @return The pixel width of the widest View
     */
    public static int getWidestView(Context context, Adapter adapter) {
        int maxWidth = 0;
        View view = null;
        FrameLayout fakeParent = new FrameLayout(context);
        for (int i = 0, count = adapter.getCount(); i < count; i++) {
            view = adapter.getView(i, view, fakeParent);
            view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            int width = view.getMeasuredWidth();
            if (width > maxWidth) {
                maxWidth = width;
            }
        }
        return maxWidth;
    }

    /**
     * Method validates email by user's valid email address regex.
     *
     * @param emailStr email string
     * @return true if the email regex was found, false otherwise
     */
    public static boolean validate(String emailStr) {
        Matcher matcher = User.VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }


    /**
     * Edit type status enumeration.
     */
    public enum EditType {
        /**
         * No edit status.
         */
        NO_EDIT,
        /**
         * Addition status.
         */
        ADD,
        /**
         * Edit status.
         */
        EDIT,
        /**
         * Deletion status.
         */
        DELETE;
    }

    /**
     * List view utilites.
     */
    public static class ListUtils {
        /**
         * Method sets dynamic height of a given list view.
         *
         * @param mListView list view to change height
         */
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }
}
