package com.example.natkobiscan.zoovrt.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.natkobiscan.zoovrt.R;
import com.example.natkobiscan.zoovrt.dao.DAOException;
import com.example.natkobiscan.zoovrt.dao.DAOProvider;
import com.example.natkobiscan.zoovrt.model.application.CurrentUser;
import com.example.natkobiscan.zoovrt.model.database.Visit;
import com.example.natkobiscan.zoovrt.model.database.Visitor;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalSpecies;
import com.example.natkobiscan.zoovrt.model.database.ZOOIndividual;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Activity for displaying animal species and indivduals contained in the species.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 3.1.2017.
 */
public class AnimalSpeciesActivity extends AppCompatActivity {

    /**
     * Extras ID used for transferring animal species.
     */
    public static final String ANIMAL_SPECIES_EXTRA_ID = "animal_species_bundle";
    /**
     * Extras ID used for transferring animal species id on qr request.
     */
    public static final String SPECIES_QR_ID_EXTRA_ID = "QR_species";

    /**
     * Current animal species.
     */
    private ZOOAnimalSpecies currAnimalSpecies;
    /**
     * Animal species description.
     */
    private TextView tvAnimalSpeciesDescription;
    /**
     * Animal species image.
     */
    private ImageView ivAnimalSpeciesImage;
    /**
     * Individuals list view.
     */
    private ExpandableListView lvAnimalSpecies;
    /**
     * Individuals list view adapter.
     */
    private ExpandableListAdapter listAdapter;
    /**
     * List of individuals.
     */
    private List<ZOOIndividual> individuals;
    /**
     * List of individual's names.
     */
    private List<String> individualNamesList;

    /**
     * Image button for adding animal species.
     */
    private ImageButton ibSpeciesAddButton;
    /**
     * Image button for editing animal species.
     */
    private ImageButton ibSpeciesEditButton;
    /**
     * Image button for deleting animal species.
     */
    private ImageButton ibSpeciesDeleteButton;

    /**
     * Group data for expandable list view.
     */
    private List<Map<String, String>> groupData;
    /**
     * Child data for expandable list view.
     */
    private List<List<Map<String, String>>> childData;

    /**
     * Button for visiting animal species.
     */
    private Button btnVisited;
    /**
     * Button for displaying animal species visit recommentadion.
     */
    private Button btnAnimalSpeciesRecommendation;

    /**
     * Current edit state.
     */
    private ActivityUtility.EditType currState = ActivityUtility.EditType.NO_EDIT;
    /**
     * Listener for changing visit status.
     */
    private View.OnClickListener visitListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            VisitTask visitTask = new VisitTask();
            visitTask.execute();
        }
    };

    /**
     * Task that persists animal visit informations.
     */
    private class VisitTask extends AsyncTask<Object, Void, ZOOAnimalSpecies> {
        @Override
        protected ZOOAnimalSpecies doInBackground(Object... params) {
            DAOProvider.getVisitorDAO().addVisit((Visitor) CurrentUser.getCurrentUser(), currAnimalSpecies);
            return DAOProvider.getVisitorDAO().getSpeciesRecommendation((Visitor) CurrentUser.getCurrentUser(), currAnimalSpecies);

        }

        @Override
        protected void onPostExecute(final ZOOAnimalSpecies recommendedSpecies) {
            CurrentUser.updateUser();
            btnVisited.setText("Posjećena vrsta");
            btnVisited.setEnabled(false);
            Toast toast = Toast.makeText(getApplicationContext(), "Nastamba posjećena", Toast.LENGTH_LONG);
            toast.show();
            btnAnimalSpeciesRecommendation = (Button) findViewById(R.id.btnAnimalSpeciesRecommendation);
            btnAnimalSpeciesRecommendation.setVisibility(View.VISIBLE);
            if (recommendedSpecies == null) {
                btnAnimalSpeciesRecommendation.setText("Nema preporuka za daljnji posjet.");
                btnAnimalSpeciesRecommendation.setEnabled(false);
            } else {
                btnAnimalSpeciesRecommendation.setText("Posjeti sljedeće: " + recommendedSpecies.getImeVrste());
            }
            btnAnimalSpeciesRecommendation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AnimalSpeciesActivity.this, AnimalSpeciesActivity.class);
                    intent.putExtra(AnimalSpeciesActivity.ANIMAL_SPECIES_EXTRA_ID, recommendedSpecies);
                    startActivity(intent);
                    btnAnimalSpeciesRecommendation.setVisibility(View.GONE);
                }
            });

        }
    }

    /**
     * Individuals select listener.
     */
    private ExpandableListView.OnChildClickListener individualSelectListener = new ExpandableListView.OnChildClickListener() {
        @Override
        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, final int childPosition, long id) {
            if (currState == ActivityUtility.EditType.NO_EDIT) {
                Intent intent = new Intent(AnimalSpeciesActivity.this, IndividualAnimalActivity.class);
                intent.putExtra(IndividualAnimalActivity.ANIMAL_INDIVIDUAL_EXTRA_ID, individuals.get(childPosition));
                startActivity(intent);
                return true;
            } else if (currState == ActivityUtility.EditType.DELETE) {
                AsyncTask<Object, Void, Boolean> deleteTask = new AsyncTask<Object, Void, Boolean>() {

                    private final int individualPosition = childPosition;
                    private ZOOIndividual individualDel;

                    @Override
                    protected void onPreExecute() {
                        individualDel = individuals.get(individualPosition);
                    }

                    @Override
                    protected Boolean doInBackground(Object... params) {
                        DAOProvider.getAdminDAO().deleteAnimalIndividual(individualDel);
                        individuals.remove(individualPosition);
                        return true;
                    }

                    @Override
                    protected void onPostExecute(Boolean aBoolean) {
                        CurrentUser.updateUser();
                        Toast toast = Toast.makeText(getApplicationContext(), "Izbrisana jedinka", Toast.LENGTH_SHORT);
                        toast.show();

                    }
                };
                deleteTask.execute();
                individualNamesList.remove(childPosition);
                lvAnimalSpecies.setBackgroundColor(Color.TRANSPARENT);
                currState = ActivityUtility.EditType.NO_EDIT;
                return true;

            }
            return true;
        }
    };
    /**
     * Listener for adding animal individual.
     */
    private View.OnClickListener addIndividualListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AnimalSpeciesActivity.this);
            builder.setTitle("Dodaj jedinku");
            final EditText input = new EditText(AnimalSpeciesActivity.this);
            input.setInputType(InputType.TYPE_CLASS_TEXT);

            builder.setView(input);

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    perserveIndividual(input.getText().toString());
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        }
    };
    /**
     * Species photography edit text.
     */
    private EditText etSpeciesPhotoUrl;
    /**
     * Species description edit text.
     */
    private EditText etSpeciesDescription;
    /**
     * Listener for handling editing of a species.
     */
    private View.OnClickListener editSpeciesListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AnimalSpeciesActivity.this);
            builder.setTitle("Uredi vrstu");
            LayoutInflater inflater = AnimalSpeciesActivity.this.getLayoutInflater();

            builder.setView(inflater.inflate(R.layout.dialog_edit_animal_species, null));


            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    currAnimalSpecies.setFotografija(etSpeciesPhotoUrl.getText().toString());
                    currAnimalSpecies.setOpisVrste(etSpeciesDescription.getText().toString());
                    UpdateSpeciesTask updateSpeciesTask = new UpdateSpeciesTask();
                    updateSpeciesTask.execute();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            Dialog editDialog = builder.create();
            editDialog.show();
            etSpeciesPhotoUrl = (EditText) editDialog.findViewById(R.id.etSpeciesPhotoUrl);
            etSpeciesPhotoUrl.setText((currAnimalSpecies.getFotografija() == null ? "" : currAnimalSpecies.getFotografija().toString()));
            etSpeciesDescription = (EditText) editDialog.findViewById(R.id.etSpeciesDescription);
            etSpeciesDescription.setText((currAnimalSpecies.getOpisVrste() == null ? "" : currAnimalSpecies.getOpisVrste().toString()));

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal_species);

        /*
         * Obtain data from intent.
         */
        Intent intent = getIntent();

        if (intent.hasExtra(SPECIES_QR_ID_EXTRA_ID)) {
            AsyncTask<Object, Void, ZOOAnimalSpecies> speciesIdTask = new AsyncTask<Object, Void, ZOOAnimalSpecies>() {
                private int currID;

                @Override
                protected void onPreExecute() {
                    Intent intent = getIntent();
                    currID = (int) intent.getExtras().get(SPECIES_QR_ID_EXTRA_ID);
                }

                @Override
                protected ZOOAnimalSpecies doInBackground(Object... params) {
                    return DAOProvider.getVisitorDAO().getAnimalSpecies(currID);
                }

                @Override
                protected void onPostExecute(ZOOAnimalSpecies zooSpecies) {
                    currAnimalSpecies = zooSpecies;
                    initGUI();
                    VisitTask visitTask = new VisitTask();
                    visitTask.execute();
                }
            };
            speciesIdTask.execute();
        } else {
            currAnimalSpecies = (ZOOAnimalSpecies) intent.getExtras().get(ANIMAL_SPECIES_EXTRA_ID);
            initGUI();
        }

    }

    @Override
    public void onBackPressed() {
        if (getIntent().hasExtra(SPECIES_QR_ID_EXTRA_ID)) {
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return;
        }

        super.onBackPressed();
    }

    /**
     * Method checks if the current user has visited current species.
     *
     * @return true if the species is already visited, false otherwise
     */
    private boolean isVisited() {
        List<Visit> visits = ((Visitor) CurrentUser.getCurrentUser()).getPosjecenosts();
        for (Visit visit : visits) {
            if (visit.getZivotinjskevrsteId().equals(currAnimalSpecies.getIdVrste())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method perserves new animal individual.
     *
     * @param individualName animal individual name
     */
    private void perserveIndividual(final String individualName) {
        if (individualName.trim().isEmpty() || individualNamesList.contains(individualName.trim())) {
            Toast toast = Toast.makeText(getApplicationContext(), "Neispravno ime za jedinku", Toast.LENGTH_SHORT);
            toast.show();
            return;
        }
        AsyncTask addIndividualTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {

                ZOOIndividual newIndividual = new ZOOIndividual();
                newIndividual.setZivotinjskevrste(currAnimalSpecies);
                newIndividual.setIme(individualName);
                DAOProvider.getAdminDAO().createAnimalIndividual(newIndividual);
                return null;
            }
        };
        addIndividualTask.execute();
        individualNamesList.add(individualName);
        ZOOIndividual newIndividual = new ZOOIndividual();
        CurrentUser.updateUser();
        newIndividual.setIme(individualName);
        individuals.add(newIndividual);


        finish();
        startActivity(getIntent());
    }

    /**
     * Methd for initializing GUI components.
     */
    private void initGUI() {
         /*
         * Initialize GUI references.
         */
        if (CurrentUser.isVisitor()) {
            btnVisited = (Button) findViewById(R.id.btnVisited);
            btnVisited.setVisibility(View.VISIBLE);
            if (!isVisited()) {
                btnVisited.setOnClickListener(visitListener);
            } else {
                btnVisited.setEnabled(false);
                btnVisited.setText("Posjećena vrsta");
            }
        }

        if (CurrentUser.isAdmin() || CurrentUser.isGuardian()) {
            ibSpeciesEditButton = (ImageButton) findViewById(R.id.ibSpeciesEditButton);
            ibSpeciesEditButton.setVisibility(View.VISIBLE);
            ibSpeciesEditButton.setOnClickListener(editSpeciesListener);
        }

        if (CurrentUser.isAdmin()) {
            ibSpeciesAddButton = (ImageButton) findViewById(R.id.ibSpeciesAddButton);
            ibSpeciesAddButton.setVisibility(View.VISIBLE);
            ibSpeciesAddButton.setOnClickListener(addIndividualListener);

            ibSpeciesDeleteButton = (ImageButton) findViewById(R.id.ibSpeciesDeleteButton);
            ibSpeciesDeleteButton.setVisibility(View.VISIBLE);
            ibSpeciesDeleteButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            lvAnimalSpecies.setBackgroundColor(currState == ActivityUtility.EditType.DELETE ? Color.TRANSPARENT : Color.RED);
                            currState = currState == ActivityUtility.EditType.DELETE ? ActivityUtility.EditType.NO_EDIT : ActivityUtility.EditType.DELETE;
                        }
                    });
        }


        tvAnimalSpeciesDescription = (TextView) findViewById(R.id.tvAnimalSpeciesDescription);
        tvAnimalSpeciesDescription.setText(currAnimalSpecies.getOpisVrste());
        setTitle(currAnimalSpecies.getImeVrste());
        ivAnimalSpeciesImage = (ImageView) findViewById(R.id.ivAnimalSpecies);
        lvAnimalSpecies = (ExpandableListView) findViewById(R.id.lvAnimalSpecies);
        if (currAnimalSpecies.getFotografija() != null) {
            AsyncTask imageTask = new AsyncTask<Object, Void, Bitmap>() {
                @Override
                protected Bitmap doInBackground(Object[] params) {
                    try {
                        Bitmap image = DAOProvider.getDAO().obtainImageFromUrl(currAnimalSpecies.getFotografija());
                        return image;
                    } catch (DAOException e) {
                        //return default image ?
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Bitmap image) {
                    if (image == null) {
                        return;
                    }
                    ivAnimalSpeciesImage.setVisibility(View.VISIBLE);
                    ivAnimalSpeciesImage.setEnabled(true);
                    ivAnimalSpeciesImage.setImageBitmap(image);
                }
            };
            imageTask.execute();


        }

        if (currAnimalSpecies.getJedinkes() != null && currAnimalSpecies.getJedinkes().size() != 0) {
            lvAnimalSpecies.setVisibility(View.VISIBLE);
            lvAnimalSpecies.setEnabled(true);


            individualNamesList = new ArrayList<>();
            individuals = new ArrayList<>();
            individuals.addAll(currAnimalSpecies.getJedinkes());

            for (ZOOIndividual indiv : individuals) {
                individualNamesList.add(indiv.getIme());
            }


            List<Map<String, String>> groupData = new ArrayList<>();
            List<List<Map<String, String>>> childData = new ArrayList<>();

            Map<String, String> curGroupMap = new HashMap<>();
            groupData.add(curGroupMap);
            curGroupMap.put("Category_key", "Jedinke");

            List<Map<String, String>> children = new ArrayList<>();
            for (int j = 0, end = individualNamesList.size(); j < end; j++) {
                Map<String, String> curChildMap = new HashMap<>();
                children.add(curChildMap);
                curChildMap.put("Subcategory_key", individualNamesList.get(j));
            }
            childData.add(children);


            listAdapter = new SimpleExpandableListAdapter(this, groupData,
                    android.R.layout.simple_expandable_list_item_1,
                    new String[]{"Category_key"}, new int[]{android.R.id.text1},
                    childData, android.R.layout.simple_expandable_list_item_2,
                    new String[]{"Subcategory_key"}, new int[]{android.R.id.text1});


            lvAnimalSpecies.setAdapter(listAdapter);
            lvAnimalSpecies.setOnChildClickListener(individualSelectListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.itHomeBack:
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            default:
                super.onOptionsItemSelected(item);
                return true;
        }


    }

    @Override
    protected void onResume() {
        if (currAnimalSpecies != null) {
            UpdateSpeciesTask updateTask = new UpdateSpeciesTask();
            updateTask.execute();
        }
        super.onResume();
    }

    /**
     * Async task that updates changes to current animal species.
     */
    private class UpdateSpeciesTask extends AsyncTask<Object, Void, ZOOAnimalSpecies> {
        @Override
        protected ZOOAnimalSpecies doInBackground(Object[] params) {
            DAOProvider.getDAO().updateSpecies(currAnimalSpecies);
            return DAOProvider.getDAO().getAnimalSpecies(currAnimalSpecies.getIdVrste());
        }

        @Override
        protected void onPostExecute(ZOOAnimalSpecies newSpeciesState) {
            currAnimalSpecies = newSpeciesState;
            initGUI();
        }
    }
}
