package com.example.natkobiscan.zoovrt.model.database;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * The persistent class for the vrstazanimljivosti database table.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
public class InterestingFactType implements Serializable {

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = -641646260691710762L;

    /**
     * Id interesting fact type.
     */
    private int idVrsteZanimljivosti;

    /**
     * Type of interesting fact.
     */
    private String vrstaZanimljivosti;

    /**
     * List of interesting facts with current type.
     */
    private List<ZOOInterestingFact> zanimljivostis;

    /**
     * Java bean empty constructor.
     */
    public InterestingFactType() {
    }

    /**
     * Method adds interesting fact to current interesting fact type.
     *
     * @param zanimljivosti interesting fact
     * @return reference to given interesting fact
     */
    public ZOOInterestingFact addZanimljivosti(ZOOInterestingFact zanimljivosti) {
        getZanimljivostis().add(zanimljivosti);
        zanimljivosti.setVrstazanimljivosti(this);

        return zanimljivosti;
    }

    /**
     * Getter method for interesting fact type id.
     *
     * @return interesting fact id
     */
    public int getIdVrsteZanimljivosti() {
        return this.idVrsteZanimljivosti;
    }

    /**
     * Method sets interesting fact type id.
     *
     * @param idVrsteZanimljivosti interesting fact type id
     */
    public void setIdVrsteZanimljivosti(int idVrsteZanimljivosti) {
        this.idVrsteZanimljivosti = idVrsteZanimljivosti;
    }

    /**
     * Getter method for interesting fact type.
     *
     * @return interesting fact type
     */
    public String getVrstaZanimljivosti() {
        return this.vrstaZanimljivosti;
    }

    /**
     * Method sets interesting fact type.
     *
     * @param vrstaZanimljivosti interesting fact type
     */
    public void setVrstaZanimljivosti(String vrstaZanimljivosti) {
        this.vrstaZanimljivosti = vrstaZanimljivosti;
    }

    /**
     * Method obtains interesting facts with current interesting fact type.
     *
     * @return list of interesting facts
     */
    public List<ZOOInterestingFact> getZanimljivostis() {
        return this.zanimljivostis;
    }

    /**
     * Method sets interesting fact list with current interesting fact type.
     *
     * @param zanimljivostis interesting fact list
     */
    public void setZanimljivostis(List<ZOOInterestingFact> zanimljivostis) {
        this.zanimljivostis = zanimljivostis;
    }

    /**
     * Method removes interesting fact from current interesting fact type.
     *
     * @param zanimljivosti interesting fact
     * @return reference to given interesting fact
     */
    public ZOOInterestingFact removeZanimljivosti(ZOOInterestingFact zanimljivosti) {
        getZanimljivostis().remove(zanimljivosti);
        zanimljivosti.setVrstazanimljivosti(null);

        return zanimljivosti;
    }

    /**
     * Method gets list of interesting fact id with current interesting fact
     * type.
     *
     * @return list of interesting fact id's
     */
    public List<Integer> getZanimljivostiIds() {
        List<Integer> ids = new ArrayList<>();
        for (ZOOInterestingFact interestingFact : zanimljivostis) {
            ids.add(interestingFact.getIdZanimljivosti());
        }
        return ids;
    }
}
