package com.example.natkobiscan.zoovrt.model.application;

import android.os.AsyncTask;
import android.util.Log;

import com.example.natkobiscan.zoovrt.dao.DAOProvider;
import com.example.natkobiscan.zoovrt.model.database.User;

/**
 * Saves a reference on the active user of the application.
 *
 * @author Dunja Vesinger
 * @version v1.0, 26.12.2016.
 */
public abstract class CurrentUser {

    /**
     * Active user.
     */
    private static User currentUser;

    /**
     * Private singleton class constructor.
     */
    private CurrentUser() {

    }

    /**
     * Returns the current user or null if no user is active.
     *
     * @return current user
     */
    public static User getCurrentUser() {
        return currentUser;
    }

    /**
     * Sets the given user as active.
     *
     * @param currentUser reference to the user that should be regarded as active current user
     */
    public static void setCurrentUser(User currentUser) {
        CurrentUser.currentUser = currentUser;
    }

    /**
     * Sets the active user to null.
     */
    public static void deactivateUser() {
        CurrentUser.currentUser = null;
    }

    /**
     * Checks if the current user is active.
     *
     * @return true if the user is active, false otherwise
     */
    public static boolean isActive() {
        return currentUser != null;
    }

    /**
     * Method checks if the current user is admin.
     *
     * @return true if the user is admin, false otherwise
     */
    public static boolean isAdmin() {
        return currentUser.getOvlasti().getIdOvlasti() == PermissionType.ADMIN.getValue();
    }

    /**
     * Method checks if the current user is guardian.
     *
     * @return true if the user is guardian, false otherwise
     */
    public static boolean isGuardian() {
        return currentUser.getOvlasti().getIdOvlasti() == PermissionType.GUARDIAN.getValue();
    }

    /**
     * Method cheks if the current user is visitor.
     *
     * @return true if the user is visitor, false otherwise
     */
    public static boolean isVisitor() {
        return currentUser.getOvlasti().getIdOvlasti() == PermissionType.VISITOR.getValue();
    }

    /**
     * Method updates current user.
     */
    public static void updateUser() {
        UpdateUserTask taske = new UpdateUserTask();
        taske.execute();

    }

    /**
     * Async task that updates current user.
     */
    private static class UpdateUserTask extends AsyncTask<Object, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Object... users) {

            User currUser = DAOProvider.getDAO().getUser(currentUser.getKorisnickoIme());
            if (currUser == null) {
                Log.d("Update user", "User null");
                return false;
            }
            String password = currUser.getLozinka();
            CurrentUser.setCurrentUser(currUser);
            CurrentUser.getCurrentUser().setLozinka(password);
            return true;
        }

    }


}
