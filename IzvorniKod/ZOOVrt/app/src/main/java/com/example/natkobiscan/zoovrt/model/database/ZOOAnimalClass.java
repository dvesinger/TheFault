package com.example.natkobiscan.zoovrt.model.database;

import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the razredzivotinja database table.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 27.12.2016.
 */
public class ZOOAnimalClass implements Serializable {
    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Animal class id.
     */
    private int idRazred;
    /**
     * Animal class name.
     */
    private String nazivRazreda;
    /**
     * List of animal species with current animal class.
     */
    private List<ZOOAnimalSpecies> zivotinjskevrstes;

    /**
     * Java bean empty constructor.
     */
    public ZOOAnimalClass() {
    }

    /**
     * Method adds animal species to the animal class.
     *
     * @param zivotinjskevrste animal species to be added
     * @return reference to given animal species
     */
    public ZOOAnimalSpecies addZivotinjskevrste(ZOOAnimalSpecies zivotinjskevrste) {
        getZivotinjskevrstes().add(zivotinjskevrste);
        zivotinjskevrste.setRazredzivotinja(this);

        return zivotinjskevrste;
    }

    /**
     * Method returns animal class id.
     *
     * @return animal class id
     */
    public int getIdRazred() {
        return this.idRazred;
    }

    /**
     * Method sets animal class id.
     *
     * @param idRazred animal class id
     */
    public void setIdRazred(int idRazred) {
        this.idRazred = idRazred;
    }

    /**
     * Method obtains animal class name.
     *
     * @return animal class name
     */
    public String getNazivRazreda() {
        return this.nazivRazreda;
    }

    /**
     * Method sets animal class name.
     *
     * @param nazivRazreda animal class name
     */
    public void setNazivRazreda(String nazivRazreda) {
        this.nazivRazreda = nazivRazreda;
    }

    /**
     * Getter method for obtaining animal species in current animal class.
     *
     * @return list of animal species from current class
     */
    public List<ZOOAnimalSpecies> getZivotinjskevrstes() {
        return this.zivotinjskevrstes;
    }

    /**
     * Method sets list of animal species in current animal class.
     *
     * @param zivotinjskevrstes list of animal species
     */
    public void setZivotinjskevrstes(List<ZOOAnimalSpecies> zivotinjskevrstes) {
        this.zivotinjskevrstes = zivotinjskevrstes;
    }

    /**
     * Method removes animal species from current animal class.
     *
     * @param zivotinjskevrste animal class to be removed
     * @return reference to given animal class
     */
    public ZOOAnimalSpecies removeZivotinjskevrste(ZOOAnimalSpecies zivotinjskevrste) {
        getZivotinjskevrstes().remove(zivotinjskevrste);
        zivotinjskevrste.setRazredzivotinja(null);

        return zivotinjskevrste;
    }

}
