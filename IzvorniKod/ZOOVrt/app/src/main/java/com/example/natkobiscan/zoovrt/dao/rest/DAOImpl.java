package com.example.natkobiscan.zoovrt.dao.rest;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.natkobiscan.zoovrt.dao.DAO;
import com.example.natkobiscan.zoovrt.dao.DAOException;
import com.example.natkobiscan.zoovrt.model.database.Admin;
import com.example.natkobiscan.zoovrt.model.database.Guardian;
import com.example.natkobiscan.zoovrt.model.database.Permissions;
import com.example.natkobiscan.zoovrt.model.database.User;
import com.example.natkobiscan.zoovrt.model.database.Visitor;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalClass;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalSpecies;
import com.example.natkobiscan.zoovrt.model.database.ZOOIndividual;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import retrofit2.Call;

/**
 * Class implements DAO interface and uses a REST web api to provide data persistency.
 *
 * @author Vedran Ivanušić
 * @author Domagoj Pluščec
 * @version v1.0, 30.12.2016.
 */
public class DAOImpl implements DAO {
    @Override
    public boolean checkUser(User user) throws DAOException {
        Log.d("checkUser", user.getKorisnickoIme() + " a");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Boolean> call = apiService.checkUser(user);


        try {
            Boolean response = call.execute().body();
            if (response == null) {
                Log.d("checkUser", "response null");
                response = false;
            }
            Log.d("onResponse", response.toString());
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }


        return false;
    }

    @Override
    public boolean createUser(User user) throws DAOException {
        Log.d("createUser", user.getKorisnickoIme() + " a");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Boolean> call = apiService.createUser(user);
        try {
            Boolean response = call.execute().body();
            Log.d("DAO-createUser response", String.valueOf(response));
            if (response == null) {
                response = false;
            }
            return response;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Returns false if username(user) doesn't exist, true otherwise.
     *
     * @param username user username
     * @return true if the username exists, false otherwise
     * @throws DAOException
     */
    @Override
    public boolean checkUsernameExists(String username) throws DAOException {
        Log.d("checkUsernameExists", username + " a");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<User> call = apiService.getUser(username);
        try {
            User response = call.execute().body();
            if (response == null) {
                return false;
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public User getUser(String username) throws DAOException {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        try {
            Call<Permissions> permissionsCall = apiService.getUserPermission(username);


            Permissions perm = permissionsCall.execute().body();
            if (perm == null) {
                Log.d("DAOImpl", "Permission null");
                return null;
            }
            switch (perm.getIdOvlasti()) {
                case 1:
                    Call<Admin> callAdmin = apiService.getAdmin(username);
                    Admin admin = callAdmin.execute().body();
                    return admin;
                case 2:
                    Call<Guardian> callGuardian = apiService.getGuardian(username);
                    Guardian guardian = callGuardian.execute().body();
                    return guardian;
                case 3:
                    Call<Visitor> callVisitor = apiService.getVisitor(username);
                    Visitor visitor = callVisitor.execute().body();
                    return visitor;
                default:
                    return null;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public Permissions getUserPermission(String username) throws DAOException {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Permissions> permissionsCall = apiService.getUserPermission(username);
        try {
            Permissions response = permissionsCall.execute().body();

            return response;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ZOOAnimalClass> getAnimalClasses() {
        Log.d("getAnimalClasses", "start");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<List<ZOOAnimalClass>> permissionsCall = apiService.getAnimalClasses();
        try {
            List<ZOOAnimalClass> response = permissionsCall.execute().body();
            return response;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Bitmap obtainImageFromUrl(String imageUrl) throws DAOException {
        Bitmap bmp = null;
        if (imageUrl.contains("{BASE_URL}")) {
            imageUrl = imageUrl.replace("{BASE_URL}", ApiClient.getBaseUrl());
        }
        try {
            URL url = new URL(imageUrl);
            bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (IOException ex) {
            throw new DAOException("IOException while obtaining image from url.");
        }
        return bmp;
    }

    @Override
    public ZOOAnimalSpecies getAnimalSpecies(int speciesID) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ZOOAnimalSpecies> permissionsCall = apiService.getAnimalSpeciesId(speciesID);
        try {
            ZOOAnimalSpecies response = permissionsCall.execute().body();
            return response;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateSpecies(ZOOAnimalSpecies currAnimalSpecies) {
        Log.d("updateSpecies", "start");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Boolean> call = apiService.updateSpecies(currAnimalSpecies);
        try {
            Log.d("updateSpecies", "" + call.request().body());
            Boolean response = call.execute().body();
            if (response == null) {
                response = false;
            }
            return;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return;
    }

    @Override
    public void updateIndividual(ZOOIndividual currIndividual) {
        Log.d("updateSpecies", "start");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Boolean> call = apiService.updateIndividual(currIndividual);
        try {
            Boolean response = call.execute().body();
            if (response == null) {
                response = false;
            }
            return;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return;
    }

    @Override
    public ZOOAnimalClass getAnimalClass(int classId) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ZOOAnimalClass> permissionsCall = apiService.getAnimalClassId(classId);
        try {
            ZOOAnimalClass response = permissionsCall.execute().body();
            return response;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
