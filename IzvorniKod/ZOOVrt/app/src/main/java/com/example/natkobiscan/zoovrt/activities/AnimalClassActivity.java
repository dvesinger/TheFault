package com.example.natkobiscan.zoovrt.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.natkobiscan.zoovrt.R;
import com.example.natkobiscan.zoovrt.dao.DAOProvider;
import com.example.natkobiscan.zoovrt.model.application.CurrentUser;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalClass;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalSpecies;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity for displaying an animal class.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 2.1.2017.
 */
public class AnimalClassActivity extends AppCompatActivity {
    /**
     * Extras key for transferring an animal class.
     */
    public static final String ANIMAL_CLASS_EXTRA_ID = "animal_class_bundle";
    /**
     * Current animal class.
     */
    private ZOOAnimalClass currAnimalClass;

    /**
     * Species list view.
     */
    private ListView lvSpecies;
    /**
     * Species names list adapter.
     */
    private ArrayAdapter<String> listAdapter;
    /**
     * List of species.
     */
    private List<ZOOAnimalSpecies> species;
    /**
     * List of species names.
     */
    private List<String> speciesNamesList;

    /**
     * ImageButton for adding animal class.
     */
    private ImageButton ibClassAddButton;
    /**
     * Image button for deleting an animal class.
     */
    private ImageButton ibClassDeleteButton;

    /**
     * Current edit state.
     */
    private ActivityUtility.EditType currState = ActivityUtility.EditType.NO_EDIT;
    /**
     * Species select listener.
     */
    private AdapterView.OnItemClickListener speciesSelectListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            if (currState == ActivityUtility.EditType.NO_EDIT) {
                Intent intent = new Intent(AnimalClassActivity.this, AnimalSpeciesActivity.class);
                intent.putExtra(AnimalSpeciesActivity.ANIMAL_SPECIES_EXTRA_ID, species.get(position));
                startActivity(intent);
            } else if (currState == ActivityUtility.EditType.DELETE) {
                AsyncTask<Object, Void, Boolean> deleteTask = new AsyncTask<Object, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Object... params) {
                        DAOProvider.getAdminDAO().deleteAnimalSpecies(species.get(position));
                        species.remove(position);
                        return true;
                    }

                    @Override
                    protected void onPostExecute(Boolean aBoolean) {
                        CurrentUser.updateUser();
                        Toast toast = Toast.makeText(getApplicationContext(), "Izbrisana vrsta", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                };
                deleteTask.execute();
                listAdapter.remove(speciesNamesList.get(position));

                lvSpecies.setBackgroundColor(Color.TRANSPARENT);
                currState = ActivityUtility.EditType.NO_EDIT;
            } else if (currState == ActivityUtility.EditType.EDIT) {

            }
        }
    };

    /**
     * Method initializes GUI components.
     */
    private void initGUI() {
        species = new ArrayList<>();
        species.addAll(currAnimalClass.getZivotinjskevrstes());
        speciesNamesList = new ArrayList<>();
        for (ZOOAnimalSpecies spec : species) {
            speciesNamesList.add(spec.getImeVrste());
        }

        listAdapter = new ArrayAdapter<>(AnimalClassActivity.this, R.layout.simplerow, speciesNamesList);
        lvSpecies.setAdapter(listAdapter);
        lvSpecies.getLayoutParams().width = (int) (ActivityUtility.getWidestView(AnimalClassActivity.this, listAdapter) * 1.05);
        lvSpecies.setOnItemClickListener(speciesSelectListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal_class);

        /*
         * Obtain data from intent.
         */
        Intent intent = getIntent();
        currAnimalClass = (ZOOAnimalClass) intent.getExtras().get(ANIMAL_CLASS_EXTRA_ID);

        /*
         * Initialize GUI references.
         */
        lvSpecies = (ListView) findViewById(R.id.lvAnimalClass);
        setTitle(currAnimalClass.getNazivRazreda().toUpperCase());

        if (CurrentUser.isAdmin()) {
            ibClassAddButton = (ImageButton) findViewById(R.id.ibClassAddButton);
            ibClassAddButton.setVisibility(View.VISIBLE);
            ibClassAddButton.setOnClickListener(

                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(AnimalClassActivity.this);
                            builder.setTitle("Dodaj vrstu");
                            final EditText input = new EditText(AnimalClassActivity.this);
                            input.setInputType(InputType.TYPE_CLASS_TEXT);
                            builder.setView(input);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    perserveSpecies(input.getText().toString());
                                }
                            });
                            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                            builder.show();
                        }
                    });

            ibClassDeleteButton = (ImageButton) findViewById(R.id.ibClassDeleteButton);
            ibClassDeleteButton.setVisibility(View.VISIBLE);
            ibClassDeleteButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            lvSpecies.setBackgroundColor(currState == ActivityUtility.EditType.DELETE ? Color.TRANSPARENT : Color.RED);
                            currState = currState == ActivityUtility.EditType.DELETE ? ActivityUtility.EditType.NO_EDIT : ActivityUtility.EditType.DELETE;
                        }
                    });
        }

        initGUI();

    }

    /**
     * Method perserves new animal species and refreshes GUI appearance.
     *
     * @param speciesName animal species name
     */
    private void perserveSpecies(final String speciesName) {
        if (speciesName.trim().isEmpty() || speciesNamesList.contains(speciesName.trim())) {
            Toast toast = Toast.makeText(getApplicationContext(), "Neispravno ime za vrstu", Toast.LENGTH_LONG);
            toast.show();
            return;
        }
        AsyncTask addSpeciesTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                ZOOAnimalSpecies newSpecies = new ZOOAnimalSpecies();
                newSpecies.setImeVrste(speciesName);
                newSpecies.setRazredzivotinja(currAnimalClass);
                DAOProvider.getAdminDAO().createAnimalSpecies(newSpecies);

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                UpdateClassTask updateTask = new UpdateClassTask();
                updateTask.execute();
                CurrentUser.updateUser();

            }
        };
        addSpeciesTask.execute();
        /*speciesNamesList.add(speciesName);
        ZOOAnimalSpecies newSpecies = new ZOOAnimalSpecies();
        newSpecies.setImeVrste(speciesName);
        species.add(newSpecies);*/
        //finish();
        //startActivity(getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.itHomeBack:
                Intent intent = new Intent(AnimalClassActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            default:
                super.onOptionsItemSelected(item);
                return true;
        }


    }

    /**
     * Async task that updates animal class and reinitializes GUI.
     */
    private class UpdateClassTask extends AsyncTask<Object, Void, ZOOAnimalClass> {
        @Override
        protected ZOOAnimalClass doInBackground(Object[] params) {

            return DAOProvider.getDAO().getAnimalClass(currAnimalClass.getIdRazred());
        }

        @Override
        protected void onPostExecute(ZOOAnimalClass newSpeciesState) {
            currAnimalClass = newSpeciesState;
            initGUI();
        }
    }

    @Override
    protected void onResume() {
        if (currAnimalClass != null) {
            UpdateClassTask updateTask = new UpdateClassTask();
            updateTask.execute();
        }
        super.onResume();
    }
}
