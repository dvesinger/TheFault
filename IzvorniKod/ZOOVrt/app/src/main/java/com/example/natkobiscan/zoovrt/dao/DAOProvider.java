package com.example.natkobiscan.zoovrt.dao;

import com.example.natkobiscan.zoovrt.dao.rest.DAOAdminImpl;
import com.example.natkobiscan.zoovrt.dao.rest.DAOGuardianImpl;
import com.example.natkobiscan.zoovrt.dao.rest.DAOImpl;
import com.example.natkobiscan.zoovrt.dao.rest.DAOVisitorImpl;

/**
 * DAOProvider class implementation.
 *
 * @author Domagoj Pluscec
 * @version v1.2, 26.12.2016.
 */
public class DAOProvider {

    /**
     * Reference to dao implementation object.
     */
    private static DAOAdmin daoAdmin = new DAOAdminImpl();

    /**
     * Reference to dao implementation object.
     */
    private static DAOGuardian daoGuardian = new DAOGuardianImpl();

    /**
     * Reference to dao implementation object.
     */
    private static DAOVisitor daoVisitor = new DAOVisitorImpl();

    /**
     * Reference to dao implementation object.
     */
    private static DAO dao = new DAOImpl();

    /**
     * Private utility class constructor.
     */
    private DAOProvider() {

    }

    /**
     * DAO object getter method.
     *
     * @return general dao
     */
    public static DAO getDAO() {
        return dao;
    }

    /**
     * DAOAdmin object getter method.
     *
     * @return admin's dao
     */
    public static DAOAdmin getAdminDAO() {
        return daoAdmin;
    }

    /**
     * DAOGuardian object getter method.
     *
     * @return guardian's dao
     */
    public static DAOGuardian getGuadrianDAO() {
        return daoGuardian;
    }

    /**
     * DAOVisitor object getter method.
     *
     * @return visitor's dao
     */
    public static DAOVisitor getVisitorDAO() {
        return daoVisitor;
    }
}
