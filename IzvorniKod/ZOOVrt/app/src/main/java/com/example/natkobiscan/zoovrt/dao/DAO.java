package com.example.natkobiscan.zoovrt.dao;


import android.graphics.Bitmap;

import com.example.natkobiscan.zoovrt.model.database.Permissions;
import com.example.natkobiscan.zoovrt.model.database.User;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalClass;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalSpecies;
import com.example.natkobiscan.zoovrt.model.database.ZOOIndividual;

import java.util.List;

/**
 * Data access object interface for general user.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.6.2015.
 */
public interface DAO {
    /**
     * Method checks if the user with user name and password is available.
     *
     * @param user user object containing at least user name and password
     * @return true if the user is contained, false otherwise
     * @throws DAOException if there was an error while accessing data
     */
    boolean checkUser(User user) throws DAOException;

    /**
     * Method persists new user.
     *
     * @param user new user
     * @return true if the user has been created, false otherwise
     * @throws DAOException if there was an error while accessing data
     */
    boolean createUser(User user) throws DAOException;

    /**
     * Method checks if the user with given username exists.
     *
     * @param username user username
     * @return true if the username exists in user table, false otherwise
     * @throws DAOException if there was an error while accessing data
     */
    boolean checkUsernameExists(String username) throws DAOException;

    /**
     * Method obtains user with given user name.
     *
     * @param username user identification
     * @return User object
     * @throws DAOException if there was an error while accessing data
     */
    User getUser(String username) throws DAOException;

    /**
     * Method obtains user permission.
     *
     * @param username user's username
     * @return permission object
     * @throws DAOException if there was an error while accessing data
     */
    Permissions getUserPermission(String username) throws DAOException;

    /**
     * Method obtains all animal classes.
     *
     * @return list of animal classes
     * @throws DAOException if there was an error while accessing data
     */
    List<ZOOAnimalClass> getAnimalClasses() throws DAOException;

    /**
     * Method obtains image from url.
     *
     * @param imageUrl image url
     * @return bitmap of the image
     * @throws DAOException if there was an error while accessing data
     */
    Bitmap obtainImageFromUrl(String imageUrl) throws DAOException;

    /**
     * Method obtains animal species with given id.
     *
     * @param currID animal species id
     * @return animal species
     */
    ZOOAnimalSpecies getAnimalSpecies(int currID);

    /**
     * Method updates animal species with given information.
     *
     * @param currAnimalSpecies new animal species information
     */
    void updateSpecies(ZOOAnimalSpecies currAnimalSpecies);

    /**
     * Method updates animal individual with given object.
     *
     * @param currIndividual animal individual to be updated
     */
    void updateIndividual(ZOOIndividual currIndividual);

    /**
     * Method obtains animal class with given id.
     *
     * @param idRazred animal class id
     * @return animal class
     */
    ZOOAnimalClass getAnimalClass(int idRazred);
}
