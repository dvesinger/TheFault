package com.example.natkobiscan.zoovrt.model.application;

/**
 * Enumeration that models currently supported zoo interesting fact types.
 */
public enum ZOOInterestingFactType {
    /**
     * Text type.
     */
    TEXT(1),
    /**
     * Image type.
     */
    IMAGE(2),
    /**
     * Video type.
     */
    VIDEO(3);

    /**
     * Zoo interesting fact type id.
     */
    private final int value;

    /**
     * Constructor that initializes interesting fact id.
     *
     * @param value interesting fact type id
     */
    ZOOInterestingFactType(int value) {
        this.value = value;
    }


    /**
     * Method that map interesting fact type id to enumeration.
     *
     * @param typeId interesting fact type id
     * @return interesting fact type
     */
    public static ZOOInterestingFactType getTypeFromId(int typeId) {
        switch (typeId) {
            case 1:
                return TEXT;
            case 2:
                return IMAGE;
            case 3:
                return VIDEO;
            default:
                return null;
        }
    }

    /**
     * Method return interesting fact type id.
     *
     * @return interesting fact type id
     */
    public int getValue() {
        return value;
    }
}
