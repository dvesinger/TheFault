package com.example.natkobiscan.zoovrt.dao.rest;

import com.example.natkobiscan.zoovrt.dao.DAOException;
import com.example.natkobiscan.zoovrt.dao.DAOVisitor;
import com.example.natkobiscan.zoovrt.dao.rest.structures.AdoptPair;
import com.example.natkobiscan.zoovrt.dao.rest.structures.VisitPair;
import com.example.natkobiscan.zoovrt.model.database.Visitor;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalSpecies;
import com.example.natkobiscan.zoovrt.model.database.ZOOIndividual;
import com.example.natkobiscan.zoovrt.model.database.ZOOInterestingFact;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;

/**
 * Class implements DAOVisitor interface for visitor and uses a REST web api to provide data persistency.
 *
 * @author Vedran Ivanušić
 * @author Domagoj Pluščec
 * @version v1.0, 2.1.2017.
 */

public class DAOVisitorImpl extends DAOImpl implements DAOVisitor {


    @Override
    public List<ZOOInterestingFact> getInterestingFacts(Visitor visitor, ZOOIndividual individual) throws DAOException {
        return null;
    }

    @Override
    public ZOOIndividual getAnimalIndividual(Integer individualID) throws DAOException {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ZOOIndividual> permissionsCall = apiService.getAnimalIndividual(individualID);
        try {
            ZOOIndividual response = permissionsCall.execute().body();
            return response;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addVisit(Visitor visitor, ZOOAnimalSpecies species) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        VisitPair visitInfo = new VisitPair(visitor.getKorisnickoIme(), species.getIdVrste());
        Call<Void> permissionsCall = apiService.visitSpecies(visitInfo);
        try {
            permissionsCall.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void adoptIndividual(Visitor currVisitor, ZOOIndividual currIndividual) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        AdoptPair adoptInfo = new AdoptPair(currVisitor.getKorisnickoIme(), currIndividual.getIdJedinke());
        Call<Void> permissionsCall = apiService.adoptIndividual(adoptInfo);
        try {
            permissionsCall.execute();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ZOOAnimalSpecies getSpeciesRecommendation(Visitor currentUser, ZOOAnimalSpecies currAnimalSpecies) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ZOOAnimalSpecies> permissionsCall = apiService.getSpeciesRecommendation(currentUser.getKorisnickoIme(), currAnimalSpecies.getIdVrste());
        try {
            ZOOAnimalSpecies response = permissionsCall.execute().body();
            return response;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
