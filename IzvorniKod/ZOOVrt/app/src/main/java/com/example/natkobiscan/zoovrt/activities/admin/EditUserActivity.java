package com.example.natkobiscan.zoovrt.activities.admin;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.natkobiscan.zoovrt.R;
import com.example.natkobiscan.zoovrt.dao.DAOProvider;
import com.example.natkobiscan.zoovrt.model.application.CurrentUser;
import com.example.natkobiscan.zoovrt.model.application.PermissionType;
import com.example.natkobiscan.zoovrt.model.database.User;
import com.example.natkobiscan.zoovrt.model.database.Visitor;

/**
 * Activity used for editing user information. It is possible for an administrator to change user's role to guardian using a button.
 *
 * @author Natko Bišćan
 * @author Domagoj Pluščec
 * @version v1.0, 2.1.2017.
 */

public class EditUserActivity extends AppCompatActivity {

    public static final String SELECTED_USER_EDIT_EXTRAS_ID = "edit_user";
    /**
     * Text views containing user's information.
     */
    private TextView tvUsername, tvFullName, tvBirthyear, tvCity, tvEmail;
    /**
     * Button used for creating a new guardian or checking a list of individuals he takes care of.
     */
    private Button btnAddGuardian, btnEditInd;


    /**
     * Button used to submit changes to user's data.
     */
    private Button btnEditUser;
    /**
     * Image button for editing user.
     */
    private ImageButton ibUserEditButton;
    /**
     * Image button for user deletion.
     */
    private ImageButton ibUserDeleteButton;
    /**
     * Changed user's data waiting to be submitted.
     */
    private EditText etName, etSurname, etCity, etBirthYear, etEmail;
    /**
     * Currently selecte user.
     */
    private User selectedUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);

        tvUsername = (TextView) findViewById(R.id.tvUsername);
        tvFullName = (TextView) findViewById(R.id.tvFullName);
        tvBirthyear = (TextView) findViewById(R.id.tvBirthyear);
        tvCity = (TextView) findViewById(R.id.tvCity);
        tvEmail = (TextView) findViewById(R.id.tvEmail);

        btnAddGuardian = (Button) findViewById(R.id.btnAddGuardian);
        btnEditInd = (Button) findViewById(R.id.btnEditIndividuals);
        btnEditUser = (Button) findViewById(R.id.btnEditUser);


        etName = (EditText) findViewById(R.id.etNewName);
        etSurname = (EditText) findViewById(R.id.etNewSurname);
        etCity = (EditText) findViewById(R.id.etNewCity);
        etBirthYear = (EditText) findViewById(R.id.etNewBirthyear);
        etEmail = (EditText) findViewById(R.id.etNewEmail);
        ibUserEditButton = (ImageButton) findViewById(R.id.ibUserEditButton);
        ibUserDeleteButton = (ImageButton) findViewById(R.id.ibUserDeleteButton);

        ibUserDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncTask<Object, Void, Boolean> deleteTask = new AsyncTask<Object, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Object... params) {
                        DAOProvider.getAdminDAO().deleteUser(selectedUser);
                        return true;
                    }

                    @Override
                    protected void onPostExecute(Boolean aBoolean) {
                        CurrentUser.updateUser();
                        Toast toast = Toast.makeText(getApplicationContext(), "Korisnik uspješno izbrisan", Toast.LENGTH_SHORT);
                        toast.show();
                        finish();
                    }
                };
                deleteTask.execute();
            }
        });

        ibUserEditButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int visibility = etName.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE;

                        etName.setVisibility(visibility);
                        etSurname.setVisibility(visibility);
                        etCity.setVisibility(visibility);
                        etBirthYear.setVisibility(visibility);
                        etEmail.setVisibility(visibility);
                        btnEditUser.setVisibility(visibility);
                    }
                }
        );

        Intent intent = getIntent();
        selectedUser = (User) intent.getExtras().get(SELECTED_USER_EDIT_EXTRAS_ID);
        initGUI();


        btnEditInd.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              Intent intent = new Intent(getApplicationContext(), AddGuardianActivity.class);
                                              intent.putExtra(AddGuardianActivity.SELECTED_GUARDIAN_EDIT_EXTRAS_ID, selectedUser);
                                              startActivity(intent);
                                          }
                                      }
        );

        btnAddGuardian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddGuardianTask addGuardianTask = new AddGuardianTask((Visitor) selectedUser);
                addGuardianTask.execute();

            }
        });

        btnEditUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!etName.getText().toString().isEmpty()) {
                    selectedUser.setIme(etName.getText().toString());
                }
                if (!etSurname.getText().toString().isEmpty()) {
                    selectedUser.setPrezime(etSurname.getText().toString());
                }
                if (!etBirthYear.getText().toString().isEmpty()) {
                    selectedUser.setGodinaRod(Integer.parseInt(etBirthYear.getText().toString()));
                }
                if (!etCity.getText().toString().isEmpty()) {
                    selectedUser.setGrad(etCity.getText().toString());
                }
                if (!etEmail.getText().toString().isEmpty()) {
                    selectedUser.setEmail(etEmail.getText().toString());
                }


                AsyncTask editUserTask = new AsyncTask<Object, Void, User>() {
                    @Override
                    protected void onPreExecute() {
                        //intent mu je poslao username kliknutog usera
                    }

                    @Override
                    protected User doInBackground(Object[] params) {
                        DAOProvider.getAdminDAO().updateUser(selectedUser);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(User selectedUser) {
                        finish();
                    }
                };
                editUserTask.execute();

            }
        });


    }

    /**
     * Method initializes GUI components.
     */
    private void initGUI() {
        if (selectedUser.getOvlasti().getIdOvlasti() == 3) {
            btnAddGuardian.setVisibility(View.VISIBLE);
        } else if (selectedUser.getOvlasti().getIdOvlasti() == 2) {
            btnEditInd.setVisibility(View.VISIBLE);
        }

        tvUsername.setText(selectedUser.getKorisnickoIme());
        tvFullName.setText(selectedUser.getIme() + " " + selectedUser.getPrezime());
        tvCity.setText(selectedUser.getGrad());
        tvBirthyear.setText(selectedUser.getGodinaRod().toString());
        tvEmail.setText(selectedUser.getEmail());
    }

    /**
     * Async task that changes user status from visitor to guardian.
     */
    private class AddGuardianTask extends AsyncTask<Object, Void, User> {
        /**
         * Visitor to be changed to guardian.
         */
        private Visitor newGuardian;

        /**
         * Constructor that initializes task parameters.
         *
         * @param currentVisitor visitor to be changed to guardian
         */
        public AddGuardianTask(Visitor currentVisitor) {
            super();
            this.newGuardian = currentVisitor;

        }

        @Override
        protected User doInBackground(Object[] params) {
            DAOProvider.getAdminDAO().updateUserPermission(newGuardian, PermissionType.GUARDIAN);
            User updatedUser = DAOProvider.getAdminDAO().getUser(newGuardian.getKorisnickoIme());
            return updatedUser;
        }

        @Override
        protected void onPostExecute(User updatedUser) {
            selectedUser = updatedUser;
            Intent intent = new Intent(getApplicationContext(), AddGuardianActivity.class);
            intent.putExtra(AddGuardianActivity.SELECTED_GUARDIAN_EDIT_EXTRAS_ID, selectedUser);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        if (selectedUser != null) {
            AsyncTask updateUser = new AsyncTask<Object, Void, User>() {
                @Override
                protected User doInBackground(Object[] params) {
                    return DAOProvider.getAdminDAO().getUser(selectedUser.getKorisnickoIme());
                }

                @Override
                protected void onPostExecute(User user) {
                    selectedUser = user;
                    initGUI();
                }
            };
        }
        super.onResume();
    }


}