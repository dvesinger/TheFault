package com.example.natkobiscan.zoovrt.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.natkobiscan.zoovrt.R;
import com.example.natkobiscan.zoovrt.dao.DAOProvider;
import com.example.natkobiscan.zoovrt.model.application.CurrentUser;
import com.example.natkobiscan.zoovrt.model.database.User;
import com.example.natkobiscan.zoovrt.model.database.Visitor;

/**
 * Activity used for registration.
 *
 * @author Natko Bišćan
 * @author Domagoj Pluščec
 * @version v1.0, 30.12.2016.
 */
public class RegisterActivity extends AppCompatActivity {
    /**
     * Name text edit.
     */
    private EditText etName;
    /**
     * Surname text edit.
     */
    private EditText etSurname;
    /**
     * Email text edit.
     */
    private EditText etEmail;
    /**
     * Birth year text edit.
     */
    private EditText etBirthYear;
    /**
     * Username text edit.
     */
    private EditText etUsername;
    /**
     * Password text edit.
     */
    private EditText etPassword;
    /**
     * City text edit.
     */
    private EditText etCity;
    /**
     * Error text view.
     */
    private TextView tvError;
    /**
     * Register button.
     */
    private Button btnRegisterMe;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etName = (EditText) findViewById(R.id.etName);
        etSurname = (EditText) findViewById(R.id.etSurname);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etBirthYear = (EditText) findViewById(R.id.etBirthYear);
        etUsername = (EditText) findViewById(R.id.etNick);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etCity = (EditText) findViewById(R.id.etCity);
        tvError = (TextView) findViewById(R.id.tvError);
        btnRegisterMe = (Button) findViewById(R.id.btnRegisterMe);

        btnRegisterMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Visitor visitor = new Visitor();

                String name = etName.getText().toString();
                String lastName = etSurname.getText().toString();
                String email = etEmail.getText().toString();
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                String city = etCity.getText().toString();

                if (!username.isEmpty()) {
                    visitor.setKorisnickoIme(username);

                } else {
                    tvError.setText("Pogresan unos korisničkog imena.");
                    return;
                }

                if (!password.isEmpty()) {
                    visitor.setLozinka(password);
                } else {
                    tvError.setText("Pogresan unos lozinke.");
                    return;
                }

                if (!name.isEmpty()) {
                    visitor.setIme(name);
                } else {
                    tvError.setText("Pogresan unos imena.");
                    return;
                }

                if (!lastName.isEmpty()) {
                    visitor.setPrezime(lastName);
                } else {
                    tvError.setText("Pogresan unos prezimena.");
                    return;
                }

                int yearofBirth = 0;
                try {
                    Log.d("Register activity", "Godina rodenja: " + etBirthYear.getText().toString());
                    yearofBirth = Integer.parseInt(etBirthYear.getText().toString());
                } catch (NumberFormatException ex) {
                    tvError.setText("Pogresan unos godine rodenja.");
                    return;
                }
                if (yearofBirth > 1900 && yearofBirth < 2017) {
                    visitor.setGodinaRod(Integer.parseInt(etBirthYear.getText().toString().replaceAll("\\.", "")));
                } else {
                    tvError.setText("Pogresan unos godine rodenja.");
                    return;
                }

                if (!email.isEmpty() && ActivityUtility.validate(email)) {
                    visitor.setEmail(email);
                } else {
                    tvError.setText("Pogresan unos emaila.");
                    return;
                }

                if (!city.isEmpty()) {
                    visitor.setGrad(city);
                } else {
                    tvError.setText("Pogresan unos grada.");
                    return;
                }

                final Visitor asyncVisitor = new Visitor(visitor);
                AsyncTask asyncTask = new AsyncTask<Object, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Object... users) {
                        if (!DAOProvider.getDAO().checkUser(asyncVisitor)) {
                            Log.d("Register activity", "User ne postoji");
                            if (!DAOProvider.getDAO().createUser(asyncVisitor)) {
                                Log.d("Register activity", "User nije moguće stvoriti");
                                return false;
                            }
                            User currUser = DAOProvider.getDAO().getUser(asyncVisitor.getKorisnickoIme());
                            if (currUser == null) {
                                //pravi se da je ok
                                Log.d("Register activity", "User nije moguće dohvatiti");
                                CurrentUser.setCurrentUser(asyncVisitor);
                            } else {

                                CurrentUser.setCurrentUser(currUser);
                                CurrentUser.getCurrentUser().setLozinka(asyncVisitor.getLozinka());
                                Log.d("Register activity", "Uspješna registracija");
                            }
                            return true;
                        } else {
                            Log.d("Register activity", "User postoji");
                            return false;
                        }
                    }

                    @Override
                    protected void onPostExecute(Boolean successfullOperaion) {
                        if (successfullOperaion) {
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else {
                            tvError.setText("Korisnik vec postoji.");
                        }
                    }
                };
                asyncTask.execute();


            }
        });


    }


}
