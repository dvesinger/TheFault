package com.example.natkobiscan.zoovrt.model.database;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Class that models guardian.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
public class Guardian extends User implements Serializable {

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = -218735886726720387L;

    /**
     * List of animal species cares. (used for guardian)
     */
    @SerializedName("zivotinjskevrstes")
    private List<ZOOAnimalSpecies> zivotinjskevrstes;

    /**
     * Constructor that initializes guardian with given parameters.
     *
     * @param korisnickoIme guardian username
     * @param lozinka       password
     * @param ime           name
     * @param prezime       lastname
     * @param email         guardian's email address
     * @param godinaRod     guardian's birth year
     * @param grad          guardian's city
     * @param ovlasti       guardian's permissions
     */
    public Guardian(String korisnickoIme, String lozinka, String ime, String prezime, String email, Integer godinaRod, String grad, Permissions ovlasti) {
        super(korisnickoIme, lozinka, ime, prezime, email, godinaRod, grad, ovlasti);
    }

    /**
     * Java bean empty constructor.
     */
    public Guardian() {
    }

    /**
     * Constructor that initializes user with general user attributes.
     *
     * @param user user instance
     */
    public Guardian(User user) {
        super(user);
    }

    /**
     * Method obtains list of animal species cares.
     *
     * @return list of animal species cares
     */
    public List<ZOOAnimalSpecies> getZivotinjskevrstes() {
        return zivotinjskevrstes;
    }

    /**
     * Method sets user's adopted animals.
     *
     * @param zivotinjskevrstes list of adopted animals
     */
    public void setZivotinjskevrstes(List<ZOOAnimalSpecies> zivotinjskevrstes) {
        this.zivotinjskevrstes = zivotinjskevrstes;
    }

}
