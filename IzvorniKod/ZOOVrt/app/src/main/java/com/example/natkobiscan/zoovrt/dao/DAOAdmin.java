package com.example.natkobiscan.zoovrt.dao;

import com.example.natkobiscan.zoovrt.model.application.PermissionType;
import com.example.natkobiscan.zoovrt.model.database.Admin;
import com.example.natkobiscan.zoovrt.model.database.Guardian;
import com.example.natkobiscan.zoovrt.model.database.User;
import com.example.natkobiscan.zoovrt.model.database.Visitor;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalSpecies;
import com.example.natkobiscan.zoovrt.model.database.ZOOIndividual;

import java.util.List;
import java.util.Map;


/**
 * Data access object interface for administrator.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 25.12.2016.
 */
public interface DAOAdmin extends DAO {
    /**
     * Method creates animal species in data storage.
     *
     * @param species species to persist
     * @throws DAOException if there was an error while accessing data
     */
    void createAnimalSpecies(ZOOAnimalSpecies species) throws DAOException;

    /**
     * Method deletes animal species from data storage.
     *
     * @param species species to persist
     * @throws DAOException if there was an error while accessing data
     */
    void deleteAnimalSpecies(ZOOAnimalSpecies species) throws DAOException;

    /**
     * Method for obtaining all visitors.
     *
     * @return list of visitors (empty list if there are no visitors)
     * @throws DAOException if there was an error while accessing data
     */
    List<Visitor> getAllVisitors() throws DAOException;

    /**
     * Method updates visitor data with given visitor.
     *
     * @param visitor visitor to be updated
     * @throws DAOException if there was an error while accessing data
     */
    void updateVisitor(Visitor visitor) throws DAOException;

    /**
     * Method for obtaining all guardians.
     *
     * @return list of guardians (empty list if there are no guardians)
     * @throws DAOException if there was an error while accessing data
     */
    List<Guardian> getAllGuardians() throws DAOException;

    /**
     * Method updates guardian data with given guardian.
     *
     * @param guardian to be updated
     * @throws DAOException if there was an error while accessing data
     */
    void updateGuardian(Guardian guardian) throws DAOException;

    /**
     * Method for obtaining all admins.
     *
     * @return list of admins (empty list if there are no admins)
     * @throws DAOException if there was an error while accessing data
     */
    List<Admin> getAllAdmins() throws DAOException;

    /**
     * Method updates admin data with given admin.
     *
     * @param admin to be updated
     * @throws DAOException if there was an error while accessing data
     */
    void updateAdmin(Admin admin) throws DAOException;

    /**
     * Method for obtaining all users.
     *
     * @return list of users (empty list if there are no users)
     * @throws DAOException if there was an error while accessing data
     */
    List<User> getAllUsers() throws DAOException;

    /**
     * Method updates user data with given user.
     *
     * @param user to be updated
     * @throws DAOException if there was an error while accessing data
     */
    void updateUser(User user) throws DAOException;

    /**
     * Method updates user permission.
     *
     * @param user       user instance
     * @param permission new permission
     */
    void updateUserPermission(User user, PermissionType permission);

    /**
     * Method removes user from data storage.
     *
     * @param user user to be removed
     * @throws DAOException if there was an error while accessing data
     */
    void deleteUser(User user) throws DAOException;

    /**
     * Method obtains visitor statistics.
     *
     * @return map which maps animal species to visit count
     * @throws DAOException if there was an error while accessing data
     */
    Map<ZOOAnimalSpecies, Integer> getVisitorStatistics() throws DAOException;

    /**
     * Method deletes given animal individual.
     *
     * @param individualDel animal individual to delete
     */
    void deleteAnimalIndividual(ZOOIndividual individualDel);

    /**
     * Method creates new animal individual.
     *
     * @param newIndividual animal individual to create
     */
    void createAnimalIndividual(ZOOIndividual newIndividual);

    /**
     * Method obtains list of all animal species.
     *
     * @return list of animal species
     */
    List<ZOOAnimalSpecies> getAnimalSpecies();

    /**
     * Method updates current Guardian cares.
     *
     * @param currGuardian list of new current guardian cares.
     */
    void updateGuardianCares(Guardian currGuardian);
}
