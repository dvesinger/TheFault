package com.example.natkobiscan.zoovrt.dao.rest;

import android.util.Log;

import com.example.natkobiscan.zoovrt.dao.DAOAdmin;
import com.example.natkobiscan.zoovrt.dao.DAOException;
import com.example.natkobiscan.zoovrt.model.application.PermissionType;
import com.example.natkobiscan.zoovrt.model.database.Admin;
import com.example.natkobiscan.zoovrt.model.database.Guardian;
import com.example.natkobiscan.zoovrt.model.database.User;
import com.example.natkobiscan.zoovrt.model.database.Visit;
import com.example.natkobiscan.zoovrt.model.database.Visitor;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalSpecies;
import com.example.natkobiscan.zoovrt.model.database.ZOOIndividual;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;

/**
 * Class implements DAOAdmin interface for administrator and uses a REST web api to provide data persistency.
 *
 * @author Vedran Ivanušić
 * @author Domagoj Pluščec
 * @version v1.0, 30.12.2016.
 */
public class DAOAdminImpl extends DAOImpl implements DAOAdmin {
    @Override
    public void createAnimalSpecies(ZOOAnimalSpecies species) throws DAOException {
        Log.d("createnimalSpecies", "start");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Boolean> call = apiService.createAnimalSpecies(species.getRazredzivotinja().getIdRazred(), species.getImeVrste());
        try {
            Boolean response = call.execute().body();
            if (response == null) {
                Log.d("create species", "response null");
                response = false;
            }
            Log.d("create species", response.toString());
            return;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createAnimalIndividual(ZOOIndividual newIndividual) {
        Log.d("createnimalIndividual", "start");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Boolean> call = apiService.createAnimalIndividual(newIndividual.getZivotinjskevrste().getIdVrste(), newIndividual.getIme());
        try {
            Boolean response = call.execute().body();
            if (response == null) {
                Log.d("create species", "response null");
                response = false;
            }
            Log.d("create species", response.toString());
            return;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<ZOOAnimalSpecies> getAnimalSpecies() {
        Log.d("getSpecies", "start");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<List<ZOOAnimalSpecies>> call = apiService.getAnimalSpecies();

        try {
            List<ZOOAnimalSpecies> response = call.execute().body();
            if (response == null) {
                return null;
            }
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateGuardianCares(Guardian currGuardian) {
        Log.d("updateGuardianCares", "start");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        List<Integer> caresIds = new ArrayList<>();
        for (ZOOAnimalSpecies spec : currGuardian.getZivotinjskevrstes()) {
            caresIds.add(spec.getIdVrste());
        }

        Call<Boolean> call = apiService.updateGuardianCares(currGuardian.getKorisnickoIme(), caresIds);

        try {
            Boolean response = call.execute().body();

            if (response == null) {
                Log.d("updateGuardianCares", "response null");
                return;
            }
            Log.d("updateGuardianCares", "response " + response.toString());
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return;
    }

    @Override
    public void deleteAnimalSpecies(ZOOAnimalSpecies species) throws DAOException {
        Log.d("deleteAnimalSpecies", "start");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = apiService.deleteAnimalSpecies(species);
        try {
            call.execute();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAnimalIndividual(ZOOIndividual individualDel) {
        Log.d("deleteAnimalIndividual", "start");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = apiService.deleteAnimalIndividual(individualDel);
        try {
            call.execute();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteUser(User user) throws DAOException {
        Log.d("deleteUser", "start");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = apiService.deleteUser(user.getKorisnickoIme());
        try {
            call.execute();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public List<Visitor> getAllVisitors() throws DAOException {
        Log.d("getAllVisitors", "start");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Visitor>> call = apiService.getAllVisitors();
        try {
            List<Visitor> response = call.execute().body();
            Log.d("getAllVisitors", "end");
            return response;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public List<Guardian> getAllGuardians() throws DAOException {
        Log.d("getAllGuardians", "start");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Guardian>> call = apiService.getAllGuardians();
        try {
            List<Guardian> response = call.execute().body();
            Log.d("getAllGuardians", "end");
            return response;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public List<Admin> getAllAdmins() throws DAOException {
        Log.d("getAllAdmins", "start");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Admin>> call = apiService.getAllAdmins();
        try {
            List<Admin> response = call.execute().body();
            Log.d("getAllAdmins", "end");
            return response;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateAdmin(Admin admin) throws DAOException {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = apiService.updateAdmin(admin);

        try {
            Void response = call.execute().body();
            return;

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void updateGuardian(Guardian guardian) throws DAOException {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = apiService.updateGuardian(guardian);

        try {
            Void response = call.execute().body();
            return;

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void updateVisitor(Visitor visitor) throws DAOException {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = apiService.updateVisitor(visitor);

        try {
            Void response = call.execute().body();
            return;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateUser(User user) throws DAOException {
        int permissionId = user.getOvlasti().getIdOvlasti();
        switch (permissionId) {
            case 1:
                updateAdmin((Admin) user);
                break;
            case 2:
                updateGuardian((Guardian) user);
                break;
            case 3:
                updateVisitor((Visitor) user);
                break;
            default:
                break;
        }
    }

    /**
     * Returns true if update successful, false otherwise.
     *
     * @param user       user instance
     * @param permission new permission
     */
    @Override
    public void updateUserPermission(User user, PermissionType permission) {
        Log.d("updateUserPermission", "start");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Boolean> call = apiService.updateUserPermission(user.getKorisnickoIme(), permission.getValue());

        try {
            Boolean response = call.execute().body();
            Log.d("updateUserPermission", "end");
            return;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public Map<ZOOAnimalSpecies, Integer> getVisitorStatistics() throws DAOException {
        Log.d("visitor statistics", "start");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<List<ZOOAnimalSpecies>> call = apiService.getAnimalSpecies();

        try {
            List<ZOOAnimalSpecies> response = call.execute().body();
            if (response == null) {
                return null;
            }
            Map<ZOOAnimalSpecies, Integer> stats = new HashMap<>();
            for (ZOOAnimalSpecies spec : response) {
                List<Visit> visits = spec.getPosjecenosts();
                stats.put(spec, visits == null ? 0 : visits.size());
            }
            return stats;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public List<User> getAllUsers() throws DAOException {
        Log.d("getAllUsers", "start");

        List<User> users = new ArrayList<>();

        List<Admin> admins = getAllAdmins();
        if (admins != null) {
            users.addAll(admins);
        }
        List<Guardian> guardians = getAllGuardians();
        if (guardians != null) {
            users.addAll(guardians);
        }
        List<Visitor> visitors = getAllVisitors();
        if (visitors != null) {
            users.addAll(visitors);

        }
        return users;
    }
}
