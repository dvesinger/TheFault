package com.example.natkobiscan.zoovrt.dao.rest;

import android.util.Log;

import com.example.natkobiscan.zoovrt.dao.DAOException;
import com.example.natkobiscan.zoovrt.dao.DAOGuardian;
import com.example.natkobiscan.zoovrt.dao.rest.structures.InterestingFactTriple;
import com.example.natkobiscan.zoovrt.model.database.ZOOIndividual;
import com.example.natkobiscan.zoovrt.model.database.ZOOInterestingFact;

import java.io.IOException;

import retrofit2.Call;


/**
 * Class implements DAOGuardian interface for guardian and uses a REST web api to provide data persistency.
 *
 * @author Vedran Ivanušić
 * @version v1.0, 2.1.2017.
 */
public class DAOGuardianImpl extends DAOImpl implements DAOGuardian {


    @Override
    public void addInterestingFact(ZOOInterestingFact interestingFact, ZOOIndividual individual) throws DAOException {
        Log.d("addInterestingFact", "start");
        InterestingFactTriple interestingFactInfo = new InterestingFactTriple(individual.getIdJedinke(), interestingFact.getVrstazanimljivosti().getIdVrsteZanimljivosti(), interestingFact.getZanimljivost());
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Boolean> call = apiService.addInterestingFact(interestingFactInfo);
        try {
            Boolean response = call.execute().body();
            if (response == null) {
                response = false;
            }
            return;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return;
    }
}
