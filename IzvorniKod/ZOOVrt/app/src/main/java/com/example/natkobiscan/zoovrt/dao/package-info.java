/**
 * Package that defines DAO interface and provides a DAOProvier for ZOOVrt aplication.
 * DAO is used to access objects without knowing the form of data storage. It is used as a middle layer.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
/**
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
package com.example.natkobiscan.zoovrt.dao;
