package com.example.natkobiscan.zoovrt.model.database;

import java.io.Serializable;
import java.util.List;

/**
 * The persistent class for the zivotinjskevrste database table.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 27.12.2016.
 */
public class ZOOAnimalSpecies implements Serializable {
    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Animal species id.
     */
    private int idVrste;

    /**
     * Animal species photography url.
     */
    private String fotografija;

    /**
     * Animal species name.
     */
    private String imeVrste;

    /**
     * Animal species description.
     */
    private String opisVrste;

    /**
     * List of users that have adopted current animal species.
     */
    private List<User> korisniks;

    /**
     * List of individuals included in current animal species.
     */
    private List<ZOOIndividual> jedinkes;

    /**
     * List of visits which have visited current animal species.
     */
    private List<Visit> posjecenosts;

    /**
     * Animal class to which animal class belongs to.
     */
    private ZOOAnimalClass razredzivotinja;

    /**
     * Java bean empty constructor.
     */
    public ZOOAnimalSpecies() {
    }

    /**
     * Method adds individual to current animal species.
     *
     * @param jedinke zoo individual
     * @return reference to given animal individual
     */
    public ZOOIndividual addJedinke(ZOOIndividual jedinke) {
        getJedinkes().add(jedinke);
        jedinke.setZivotinjskevrste(this);

        return jedinke;
    }

    /**
     * Method adds visit to current animal species.
     *
     * @param posjecenost visit
     * @return reference to given visit
     */
    public Visit addPosjecenost(Visit posjecenost) {
        getPosjecenosts().add(posjecenost);
        posjecenost.setZivotinjskevrste(this);

        return posjecenost;
    }

    /**
     * Method obtains url which represents animal species photography.
     *
     * @return url of animal species photography
     */
    public String getFotografija() {
        return this.fotografija;
    }

    /**
     * Method sets url to photography of animal species.
     *
     * @param fotografija url to photography
     */
    public void setFotografija(String fotografija) {
        this.fotografija = fotografija;
    }

    /**
     * Getter method for animal species id.
     *
     * @return animal species id.
     */
    public int getIdVrste() {
        return this.idVrste;
    }

    /**
     * Method sets animal species id.
     *
     * @param idVrste animal species id
     */
    public void setIdVrste(int idVrste) {
        this.idVrste = idVrste;
    }

    /**
     * Getter method for animal species name.
     *
     * @return animal species name
     */
    public String getImeVrste() {
        return this.imeVrste;
    }

    /**
     * Method sets animal species name.
     *
     * @param imeVrste animal species name
     */
    public void setImeVrste(String imeVrste) {
        this.imeVrste = imeVrste;
    }

    /**
     * Getter method for animal species individuals.
     *
     * @return list of animal individuals
     */
    public List<ZOOIndividual> getJedinkes() {
        return this.jedinkes;
    }

    /**
     * Method sets list of animal species individuals.
     *
     * @param jedinkes list of individuals
     */
    public void setJedinkes(List<ZOOIndividual> jedinkes) {
        this.jedinkes = jedinkes;
    }

    /**
     * Method obtains list of users that have adopted current animal species.
     *
     * @return list of adopters
     */
    public List<User> getKorisniks() {
        return this.korisniks;
    }

    /**
     * Method sets list of adopters.
     *
     * @param korisniks list of adopters
     */
    public void setKorisniks(List<User> korisniks) {
        this.korisniks = korisniks;
    }

    /**
     * Getter method for animal species description.
     *
     * @return animal species description
     */
    public String getOpisVrste() {
        return this.opisVrste;
    }

    /**
     * Method sets animal species description.
     *
     * @param opisVrste animal species description
     */
    public void setOpisVrste(String opisVrste) {
        this.opisVrste = opisVrste;
    }

    /**
     * Method obtains list of animal species visits.
     *
     * @return list of animal species visits
     */
    public List<Visit> getPosjecenosts() {
        return this.posjecenosts;
    }

    /**
     * Method sets animal species visit list.
     *
     * @param posjecenosts list of visits
     */
    public void setPosjecenosts(List<Visit> posjecenosts) {
        this.posjecenosts = posjecenosts;
    }

    /**
     * Getter method for animal class in which current animal species is
     * contained.
     *
     * @return animal class
     */
    public ZOOAnimalClass getRazredzivotinja() {
        return this.razredzivotinja;
    }

    /**
     * Method sets animal class to which current animal species belongs.
     *
     * @param razredzivotinja animal class
     */
    public void setRazredzivotinja(ZOOAnimalClass razredzivotinja) {
        this.razredzivotinja = razredzivotinja;
    }

    /**
     * Method removes individual from animal species.
     *
     * @param jedinke individual to be removed
     * @return reference to given individual
     */
    public ZOOIndividual removeJedinke(ZOOIndividual jedinke) {
        getJedinkes().remove(jedinke);
        jedinke.setZivotinjskevrste(null);

        return jedinke;
    }

    /**
     * Method removes visit from animal species.
     *
     * @param posjecenost visit to be removed
     * @return reference to given visit
     */
    public Visit removePosjecenost(Visit posjecenost) {
        getPosjecenosts().remove(posjecenost);
        posjecenost.setZivotinjskevrste(null);

        return posjecenost;
    }

}
