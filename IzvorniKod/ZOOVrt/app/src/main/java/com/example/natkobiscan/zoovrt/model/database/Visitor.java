package com.example.natkobiscan.zoovrt.model.database;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Class that models visitor.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
public class Visitor extends User implements Serializable {

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = -7879689148658837419L;

    /**
     * Visitor's list of adopted individuals.
     */
    @SerializedName("list_jedinke")
    private List<ZOOIndividual> jedinkes;

    /**
     * Visitor's list of visits.
     */
    @SerializedName("list_posjecenost")
    private List<Visit> posjecenosts;

    /**
     * Java bean empty constructor.
     */
    public Visitor() {
    }

    /**
     * Constructor that initializes user with general user attributes.
     *
     * @param user user instance
     */
    public Visitor(User user) {
        super(user);
    }

    /**
     * Construcot that initializes all visitor attributes.
     *
     * @param lozinka       password
     * @param ime           name
     * @param prezime       last name
     * @param email         email address
     * @param godinaRod     birth year
     * @param grad          city
     * @param ovlasti       permission
     * @param korisnickoIme username
     */
    public Visitor(String lozinka, String ime, String prezime, String email, Integer godinaRod, String grad, Permissions ovlasti, String korisnickoIme) {
        super(korisnickoIme, lozinka, ime, prezime, email, godinaRod, grad, ovlasti);

    }

    /**
     * Method adds a visit to list of user's visits.
     *
     * @param posjecenost visit
     * @return reference to given visit
     */
    public Visit addPosjecenost(Visit posjecenost) {
        getPosjecenosts().add(posjecenost);
        posjecenost.setKorisnik(this);

        return posjecenost;
    }

    /**
     * Getter method that obtains list of user's adopted individuals.
     *
     * @return list of user's adopted individuals
     */
    public List<ZOOIndividual> getJedinkes() {
        return jedinkes;
    }

    /**
     * Method sets list of user's adopted individuals.
     *
     * @param jedinkes list of adopted individuals
     */
    public void setJedinkes(List<ZOOIndividual> jedinkes) {
        this.jedinkes = jedinkes;
    }

    /**
     * Method obtains list of user's visits.
     *
     * @return list of user's visits
     */
    public List<Visit> getPosjecenosts() {
        return this.posjecenosts;
    }

    /**
     * Method sets users list of visits.
     *
     * @param posjecenosts list of visits
     */
    public void setPosjecenosts(List<Visit> posjecenosts) {
        this.posjecenosts = posjecenosts;
    }

    /**
     * Method removes visit from the user.
     *
     * @param posjecenost visit to be removed
     * @return reference to given visit
     */
    public Visit removePosjecenost(Visit posjecenost) {
        getPosjecenosts().remove(posjecenost);
        posjecenost.setKorisnik(null);

        return posjecenost;
    }
}
