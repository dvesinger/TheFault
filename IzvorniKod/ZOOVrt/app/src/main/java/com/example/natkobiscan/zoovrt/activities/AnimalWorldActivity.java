package com.example.natkobiscan.zoovrt.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.natkobiscan.zoovrt.R;
import com.example.natkobiscan.zoovrt.dao.DAOProvider;
import com.example.natkobiscan.zoovrt.model.application.CurrentUser;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Activity that shows animal wordls by presenting list of animal classes.
 *
 * @author Domagoj Pluščec
 * @version v1.0, 3.1.2017.
 */
public class AnimalWorldActivity extends AppCompatActivity {

    /**
     * Animal classes list view.
     */
    private ListView lvClasses;
    /**
     * Animal classes list view adapter.
     */
    private ArrayAdapter<String> listAdapter;
    /**
     * Map that maps animal class name to animal class.
     */
    private Map<String, ZOOAnimalClass> animalClasses;
    /**
     * List of animal class names.
     */
    private ArrayList<String> animalClassesList;
    /**
     * Class selection listener.
     */
    private AdapterView.OnItemClickListener classSelectListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            String className = listAdapter.getItem(position);
            Intent intent = new Intent(AnimalWorldActivity.this, AnimalClassActivity.class);
            intent.putExtra(AnimalClassActivity.ANIMAL_CLASS_EXTRA_ID, animalClasses.get(className));
            startActivity(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal_world);
        lvClasses = (ListView) findViewById(R.id.lvAnimalWorld);
        animalClasses = new HashMap<>();
        AnimalWorldTask animalWorldTask = new AnimalWorldTask();
        animalWorldTask.execute();
    }

    /**
     * Task obtains all animal classes from database.
     */
    private class AnimalWorldTask extends AsyncTask<Object, Void, List<ZOOAnimalClass>> {
        @Override
        protected List<ZOOAnimalClass> doInBackground(Object... params) {
            if (CurrentUser.isAdmin()) {
                List<ZOOAnimalClass> animalClasses = DAOProvider.getAdminDAO().getAnimalClasses();
                return animalClasses;
            } else if (CurrentUser.isVisitor()) {
                List<ZOOAnimalClass> animalClasses = DAOProvider.getVisitorDAO().getAnimalClasses();
                return animalClasses;
            }
            return null;

        }

        @Override
        protected void onPostExecute(List<ZOOAnimalClass> animalClasses) {
            animalClassesList = new ArrayList();
            Log.d("AnimalWorldtActivity", "onPostExecute");
            for (ZOOAnimalClass animalClass : animalClasses) {
                AnimalWorldActivity.this.animalClasses.put(animalClass.getNazivRazreda().toUpperCase(), animalClass);
                animalClassesList.add(animalClass.getNazivRazreda().toUpperCase());

            }
            listAdapter = new ArrayAdapter<>(AnimalWorldActivity.this, R.layout.simplerow, animalClassesList);
            lvClasses.setAdapter(listAdapter);
            lvClasses.getLayoutParams().width = (int) (ActivityUtility.getWidestView(AnimalWorldActivity.this, listAdapter) * 1.05);
            lvClasses.setOnItemClickListener(classSelectListener);
        }
    }

}
