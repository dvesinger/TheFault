package com.example.natkobiscan.zoovrt.model.database;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.regex.Pattern;

/**
 * Abstract class that models general user attributes.
 *
 * @author Domagoj Pluscec
 * @version v2.0, 28.12.2016., Vedran Ivanusic
 */
public abstract class User implements Serializable {

    /**
     * Valid email address regex.
     */
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = 6308921911018631089L;

    /**
     * Username.
     */
    @SerializedName("korisnicko_ime")
    private String korisnickoIme;

    /**
     * User email.
     */
    @SerializedName("email")
    private String email;


    /**
     * User birth year.
     */
    @SerializedName("godinaRod")
    private Integer godinaRod;

    /**
     * User's city.
     */
    @SerializedName("grad")
    private String grad;

    /**
     * User's name.
     */
    @SerializedName("ime")
    private String ime;

    /**
     * User's password.
     */
    @SerializedName("lozinka")
    private String lozinka;

    /**
     * User's last name.
     */
    @SerializedName("prezime")
    private String prezime;

    /**
     * User's permissions.
     */
    private Permissions ovlasti;

    /**
     * Constructor that initializes user with given parameters.
     *
     * @param korisnickoIme username
     * @param lozinka       password
     * @param ime           name
     * @param prezime       last name
     * @param email         email address
     * @param godinaRod     birth year
     * @param grad          city
     * @param ovlasti       user's permission
     */
    public User(String korisnickoIme, String lozinka, String ime, String prezime, String email, Integer godinaRod, String grad, Permissions ovlasti) {
        this.korisnickoIme = korisnickoIme;
        this.email = email;
        this.godinaRod = godinaRod;
        this.grad = grad;
        this.ime = ime;
        this.lozinka = lozinka;
        this.prezime = prezime;
        this.ovlasti = ovlasti;
    }

    /**
     * Java bean empty constructor.
     */
    public User() {

    }

    /**
     * Constructor that initializes user with general user attributes.
     *
     * @param user user instance
     */
    public User(User user) {
        this.korisnickoIme = user.korisnickoIme;
        this.email = user.email;
        this.godinaRod = user.godinaRod;
        this.grad = user.grad;
        this.ime = user.ime;
        this.lozinka = user.lozinka;
        this.prezime = user.prezime;
        this.ovlasti = user.ovlasti;
    }

    /**
     * Email getter method.
     *
     * @return user's email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Method sets user's email address.
     *
     * @param email email address
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Birth year getter method.
     *
     * @return user's birth year
     */
    public Integer getGodinaRod() {
        return godinaRod;
    }

    /**
     * Method sets user's birthy year.
     *
     * @param godinaRod birth year
     */
    public void setGodinaRod(Integer godinaRod) {
        this.godinaRod = godinaRod;
    }

    /**
     * City getter method.
     *
     * @return user's city.
     */
    public String getGrad() {
        return grad;
    }

    /**
     * Method set's user city.
     *
     * @param grad city
     */
    public void setGrad(String grad) {
        this.grad = grad;
    }

    /**
     * Name getter method.
     *
     * @return user's name
     */
    public String getIme() {
        return ime;
    }

    /**
     * Method sets user's first name.
     *
     * @param ime first name
     */
    public void setIme(String ime) {
        this.ime = ime;
    }

    /**
     * Method for obtaining user's username.
     *
     * @return user's username
     */
    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    /**
     * Method sets user's username.
     *
     * @param korisnickoIme username
     */
    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    /**
     * Password getter method.
     *
     * @return user's password
     */
    public String getLozinka() {
        return lozinka;
    }

    /**
     * Method sets user password.
     *
     * @param lozinka password
     */
    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    /**
     * Permissions getter method.
     *
     * @return user's permissions
     */
    public Permissions getOvlasti() {
        return this.ovlasti;
    }

    /**
     * Method sets user permission.
     *
     * @param ovlasti permission
     */
    public void setOvlasti(Permissions ovlasti) {
        this.ovlasti = ovlasti;
    }

    /**
     * Method obtains user's last name.
     *
     * @return last name
     */
    public String getPrezime() {
        return prezime;
    }

    /**
     * Method sets user's last name.
     *
     * @param prezime last name
     */
    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

}
