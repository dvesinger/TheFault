package com.example.natkobiscan.zoovrt.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.natkobiscan.zoovrt.R;
import com.example.natkobiscan.zoovrt.dao.DAOProvider;
import com.example.natkobiscan.zoovrt.dao.rest.ApiClient;
import com.example.natkobiscan.zoovrt.model.application.CurrentUser;
import com.example.natkobiscan.zoovrt.model.database.User;
import com.example.natkobiscan.zoovrt.model.database.Visitor;

/**
 * Activitiy used as application start screen. It enables login functionality and provides user a register button if the user is not registered yet.
 *
 * @author Natko Bišćan
 * @author Domagoj Pluščec
 * @version v1.0, 30.12.2016.
 */
public class LoginActivity extends AppCompatActivity {

    /**
     * Used for saving user's data when "remeber me" is clicked.
     */
    private SharedPreferences loginPreferences;
    /**
     * Shared preferences editor used for login pregerences.
     */
    private SharedPreferences.Editor loginPrefsEditor;
    /**
     * Should the login data be saved.
     */
    private Boolean saveLogin;
    /**
     * User's username and password used for logging in.
     */
    private String username, password;
    /**
     * Button used for opening registration activity.
     */
    private ImageButton btnRegister;
    /**
     * Button for logging in into the app.
     */
    private Button btnLogin;
    /**
     * Check box for remembering user's login data.
     */
    private CheckBox cbRemember;
    /**
     * Users's username.
     */
    private EditText etUserName;
    /**
     * Users's password.
     */
    private EditText etPassword;

    /**
     * Application settings - such as host url.
     */
    private ImageButton ibAppSettings;
    /**
     * Application settings listener.
     */
    private View.OnClickListener appSettingsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            builder.setTitle("Postavke");
            final EditText input = new EditText(LoginActivity.this);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            input.setText(ApiClient.getHostIP());
            LinearLayout layout = new LinearLayout(LoginActivity.this);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            TextView currTv = new TextView(LoginActivity.this);
            currTv.setText("IP adresa");
            currTv.setLayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            layout.addView(currTv);
            layout.addView(input);
            builder.setView(layout);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ApiClient.setHostIp(input.getText().toString());
                    SharedPreferences hostPreferences = getSharedPreferences(HOST_PREFERENCES_NAME, MODE_PRIVATE);
                    hostPreferences.edit().putString(HOST_IP_SHARED_PREFERENCES_KEY, input.getText().toString()).apply();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        }
    };

    /**
     * Shared preferences id for storing host ip address.
     */
    private static final String HOST_IP_SHARED_PREFERENCES_KEY = "host_ip";
    /**
     * Shared preferences name for storing host informations.
     */
    private static final String HOST_PREFERENCES_NAME = "hostPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnRegister = (ImageButton) findViewById(R.id.btnRegister);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        cbRemember = (CheckBox) findViewById(R.id.cbRemember);
        etUserName = (EditText) findViewById(R.id.etUserName);
        etPassword = (EditText) findViewById(R.id.etPassword);
        ibAppSettings = (ImageButton) findViewById(R.id.ibAppSettings);
        ibAppSettings.setOnClickListener(appSettingsListener);

        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();

        SharedPreferences hostPreferences = getSharedPreferences(HOST_PREFERENCES_NAME, MODE_PRIVATE);

        if (hostPreferences.contains(HOST_IP_SHARED_PREFERENCES_KEY)) {
            ApiClient.setHostIp(hostPreferences.getString(HOST_IP_SHARED_PREFERENCES_KEY, "192.168.0.12"));
        }

        saveLogin = loginPreferences.getBoolean("saveLogin", false);
        if (saveLogin) {
            etUserName.setText(loginPreferences.getString("username", ""));
            etPassword.setText(loginPreferences.getString("password", ""));
            cbRemember.setChecked(true);
        }

        if (CurrentUser.isActive()) {
            etUserName.setText(CurrentUser.getCurrentUser().getKorisnickoIme());
            etPassword.setText(CurrentUser.getCurrentUser().getLozinka());
            cbRemember.setChecked(false);
        }

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etUserName.getWindowToken(), 0);

                username = etUserName.getText().toString();
                password = etPassword.getText().toString();

                if (username.isEmpty()) {
                    etUserName.setHint("Korisničko ime");
                    return;
                }
                if (password.isEmpty()) {
                    etPassword.setHint("Lozinka");
                }

                User user = new Visitor();
                user.setKorisnickoIme(username);
                user.setLozinka(password);
                final User asyncUser = new Visitor(user);
                AsyncTask asyncTask = new AsyncTask<Object, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Object... users) {
                        if (!DAOProvider.getDAO().checkUser(asyncUser)) {
                            Log.d("LoginActivity", "User doesn't exist");
                            return false;
                        }
                        User currUser = DAOProvider.getDAO().getUser(asyncUser.getKorisnickoIme());
                        if (currUser == null) {
                            Log.d("LoginActivity", "User null");
                            return false;
                        }
                        CurrentUser.setCurrentUser(currUser);
                        CurrentUser.getCurrentUser().setLozinka(asyncUser.getLozinka());
                        return true;
                    }

                    @Override
                    protected void onPostExecute(Boolean o) {
                        if (o) {
                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(intent);
                        } else {
                            etUserName.setHint("Korisničko ime");
                            etPassword.setHint("Lozinka");

                            Toast toast = Toast.makeText(getApplicationContext(), "Neispravni korisnički podaci!", Toast.LENGTH_LONG);
                            toast.show();
                        }
                    }
                };
                asyncTask.execute();


                if (cbRemember.isChecked()) {
                    loginPrefsEditor.putBoolean("saveLogin", true);
                    loginPrefsEditor.putString("username", username);
                    loginPrefsEditor.putString("password", password);
                    loginPrefsEditor.commit();
                } else {
                    loginPrefsEditor.clear();
                    loginPrefsEditor.commit();
                }
            }
        });


    }


}
