package com.example.natkobiscan.zoovrt.model.application;

/**
 * Enumeration for supported types of permission.
 * Used for homeactivity.
 *
 * @author Lovro Kunović
 * @version v1.0, 30.12.2016.
 */
public enum PermissionType {
    /**
     * Administrator.
     */
    ADMIN(1),
    /**
     * Visitor.
     */
    VISITOR(3),
    /**
     * Guardian.
     */
    GUARDIAN(2);

    /**
     * Permission type id.
     */
    private final int value;

    /**
     * COnstructor that initializes permission type id.
     *
     * @param value permission type id
     */
    PermissionType(int value) {
        this.value = value;
    }

    /**
     * Method obtains permission type id.
     *
     * @return permission type id
     */
    public int getValue() {
        return value;
    }
}
