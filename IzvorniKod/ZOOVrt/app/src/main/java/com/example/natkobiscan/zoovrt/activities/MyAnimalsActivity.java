package com.example.natkobiscan.zoovrt.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.natkobiscan.zoovrt.R;
import com.example.natkobiscan.zoovrt.model.application.CurrentUser;
import com.example.natkobiscan.zoovrt.model.database.Guardian;
import com.example.natkobiscan.zoovrt.model.database.Visitor;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalSpecies;
import com.example.natkobiscan.zoovrt.model.database.ZOOIndividual;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity for displaying my animal list (visitor adoptions and guardian cares).
 *
 * @author Domagoj Pluscec
 * @version v1.0, 20.12.2016.
 */
public class MyAnimalsActivity extends AppCompatActivity {

    /**
     * List of adopted animal individuals.
     */
    private List<ZOOIndividual> individuals;
    /**
     * List of animal species cares.
     */
    private List<ZOOAnimalSpecies> species;
    /**
     * List view for displaying animal individuals.
     */
    private ListView lvAdoptions;

    /**
     * Individuals names list adapter.
     */
    private ArrayAdapter<String> listAdapter;

    /**
     * List of individuals names.
     */
    private List<String> individualsNamesList;

    /**
     * Animal species names list.
     */
    private List<String> speciesNamesList;
    /**
     * Adopted individuals select listener.
     */
    private AdapterView.OnItemClickListener adoptedSelectListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(MyAnimalsActivity.this, IndividualAnimalActivity.class);
            intent.putExtra(IndividualAnimalActivity.ANIMAL_INDIVIDUAL_EXTRA_ID, individuals.get(position));
            startActivity(intent);
        }
    };
    /**
     * Guardian cares species listener.
     */
    private AdapterView.OnItemClickListener caresSelectListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(MyAnimalsActivity.this, AnimalSpeciesActivity.class);
            intent.putExtra(AnimalSpeciesActivity.ANIMAL_SPECIES_EXTRA_ID, species.get(position));
            startActivity(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_animals);
        lvAdoptions = (ListView) findViewById(R.id.lvAnimalAdoptions);
        Log.d("myanimals", "start");
        if (CurrentUser.isVisitor()) {
            setTitle("Posvojene jedinke");
            individuals = ((Visitor) CurrentUser.getCurrentUser()).getJedinkes();
            individualsNamesList = new ArrayList<>();
            if (individuals == null) {
                Toast toast = Toast.makeText(getApplicationContext(), "Niste posvojili nijednu jedinku", Toast.LENGTH_LONG);
                toast.show();
                finish();
                individuals = new ArrayList<>();
            }
            for (ZOOIndividual indiv : individuals) {
                individualsNamesList.add(indiv.getIme());
            }

            listAdapter = new ArrayAdapter<>(MyAnimalsActivity.this, R.layout.simplerow, individualsNamesList);
            lvAdoptions.setAdapter(listAdapter);
            lvAdoptions.getLayoutParams().width = (int) (ActivityUtility.getWidestView(MyAnimalsActivity.this, listAdapter) * 1.05);
            lvAdoptions.setOnItemClickListener(adoptedSelectListener);
        } else if (CurrentUser.isGuardian()) {
            Log.d("myanimals-guaridan", "start");
            setTitle("Brige o vrstama");
            species = ((Guardian) CurrentUser.getCurrentUser()).getZivotinjskevrstes();
            speciesNamesList = new ArrayList<>();
            if (species == null) {
                Toast toast = Toast.makeText(getApplicationContext(), "Nema vrsta za prikaz", Toast.LENGTH_LONG);
                toast.show();
                finish();
                species = new ArrayList<>();
            }
            for (ZOOAnimalSpecies spec : species) {
                speciesNamesList.add(spec.getImeVrste());
            }
            listAdapter = new ArrayAdapter<>(MyAnimalsActivity.this, R.layout.simplerow, speciesNamesList);
            lvAdoptions.setAdapter(listAdapter);
            lvAdoptions.getLayoutParams().width = (int) (ActivityUtility.getWidestView(MyAnimalsActivity.this, listAdapter) * 1.05);
            lvAdoptions.setOnItemClickListener(caresSelectListener);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.itHomeBack:
                Intent intent = new Intent(MyAnimalsActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            default:
                super.onOptionsItemSelected(item);
                return true;
        }


    }
}
