package com.example.natkobiscan.zoovrt.activities.admin;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.natkobiscan.zoovrt.R;
import com.example.natkobiscan.zoovrt.dao.DAOProvider;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalSpecies;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * Activity used for displaying zoo statistics.
 *
 * @author Lovro Kunović
 * @author Domagoj Pluščec
 * @version 4.1.2017.
 */
public class StatisticsActivity extends AppCompatActivity {

    /**
     * String representations of statistics.
     */
    private ArrayList<String> listStatistics;

    /**
     * List view for displaying statistics.
     */
    private ListView lvStatistics;

    /**
     * List view array adapter.
     */
    private ArrayAdapter<String> arrayListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        lvStatistics = (ListView) findViewById(R.id.lvStat);
        listStatistics = new ArrayList<>();
        AsyncTask statTask = new AsyncTask<Object, Void, Map<ZOOAnimalSpecies, Integer>>() {

            @Override
            protected Map<ZOOAnimalSpecies, Integer> doInBackground(Object... params) {

                Map<ZOOAnimalSpecies, Integer> map = DAOProvider.getAdminDAO().getVisitorStatistics();
                return map;
            }

            @Override
            protected void onPostExecute(Map<ZOOAnimalSpecies, Integer> map) {
                Set<Map.Entry<ZOOAnimalSpecies, Integer>> entrySet = map.entrySet();


                for (Map.Entry<ZOOAnimalSpecies, Integer> entry : entrySet) {
                    String line = entry.getKey().getImeVrste() + " " + entry.getValue().intValue();
                    listStatistics.add(line);
                }
                Collections.sort(listStatistics);

                arrayListAdapter = new ArrayAdapter<>(StatisticsActivity.this, R.layout.simplerow, listStatistics);
                lvStatistics.setAdapter(arrayListAdapter);

            }
        };
        statTask.execute();


    }
}

