package com.example.natkobiscan.zoovrt.model.database;

import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the zanimljivosti database table.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 20.12.2016.
 */
public class ZOOInterestingFact implements Serializable {
    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Interesting fact id.
     */
    private int idZanimljivosti;

    /**
     * Interesting fact creation date.
     */
    private Date datum;

    /**
     * Interesting fact content.
     */
    private String zanimljivost;

    /**
     * Individual connected with interesting fact.
     */
    private ZOOIndividual jedinke;

    /**
     * Type of interesting fact.
     */
    private InterestingFactType vrstazanimljivosti;

    /**
     * Java bean empty constructor.
     */
    public ZOOInterestingFact() {
    }

    /**
     * Method obtains interesting fact creation date.
     *
     * @return creation date
     */
    public Date getDatum() {
        return this.datum;
    }

    /**
     * Method sets interesting fact creation date.
     *
     * @param datum interesting fact creation date
     */
    public void setDatum(Date datum) {
        this.datum = datum;
    }

    /**
     * Method obtains interesting fact id.
     *
     * @return interesting fact id
     */
    public int getIdZanimljivosti() {
        return this.idZanimljivosti;
    }

    /**
     * Setter method for interesting fact id.
     *
     * @param idZanimljivosti interesting fact id
     */
    public void setIdZanimljivosti(int idZanimljivosti) {
        this.idZanimljivosti = idZanimljivosti;
    }

    /**
     * Method obtains individual connected with interesting fact.
     *
     * @return zoo individual
     */
    public ZOOIndividual getJedinke() {
        return this.jedinke;
    }

    /**
     * Setter method for individual connected with interesting fact.
     *
     * @param jedinke zoo individual
     */
    public void setJedinke(ZOOIndividual jedinke) {
        this.jedinke = jedinke;
    }

    /**
     * Method obtains interesting fact type.
     *
     * @return interesting fact type
     */
    public InterestingFactType getVrstazanimljivosti() {
        return this.vrstazanimljivosti;
    }

    /**
     * Setter method for interesting fact type.
     *
     * @param vrstazanimljivosti interesting fact type
     */
    public void setVrstazanimljivosti(InterestingFactType vrstazanimljivosti) {
        this.vrstazanimljivosti = vrstazanimljivosti;
    }

    /**
     * Method obtains interesting fact content.
     *
     * @return interesting fact content
     */
    public String getZanimljivost() {
        return this.zanimljivost;
    }

    /**
     * Method sets interesting fact content.
     *
     * @param zanimljivost interesting fact content
     */
    public void setZanimljivost(String zanimljivost) {
        this.zanimljivost = zanimljivost;
    }

}
