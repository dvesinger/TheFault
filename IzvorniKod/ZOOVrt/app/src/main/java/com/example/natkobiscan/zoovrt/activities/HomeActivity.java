package com.example.natkobiscan.zoovrt.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.natkobiscan.zoovrt.R;
import com.example.natkobiscan.zoovrt.activities.admin.StatisticsActivity;
import com.example.natkobiscan.zoovrt.activities.admin.UserListActivity;
import com.example.natkobiscan.zoovrt.activities.visitor.MapActivity;
import com.example.natkobiscan.zoovrt.activities.visitor.QRActivity;
import com.example.natkobiscan.zoovrt.model.application.CurrentUser;

/**
 * Aplication main screen, it gives navigation to other application functionalities.
 *
 * @author Domagoj Pluscec
 * @author Lovro Kunović
 * @version v1.0, 2.1.2017.
 */
public class HomeActivity extends AppCompatActivity {

    /**
     * Animal world button.
     */
    private Button btnAnimalWorld;
    /**
     * My Animals button.
     */
    private Button btnMyAnimals;
    /**
     * Logout button.
     */
    private Button btnLogout;
    /**
     * QR button.
     */
    private Button btnQR;
    /**
     * Map button.
     */
    private Button btnMap;
    /**
     * Statistics button.
     */
    private Button btnStatistics;
    /**
     * User list button.
     */
    private Button btnUsers;
    /**
     * Back button press counter for logout feature.
     */
    private int backCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btnAnimalWorld = (Button) findViewById(R.id.btnAnimalWorld);
        btnMyAnimals = (Button) findViewById(R.id.btnMyAnimals);
        btnLogout = (Button) findViewById(R.id.btnLogOut);
        btnQR = (Button) findViewById(R.id.btnQR);
        btnMap = (Button) findViewById(R.id.btnMap);
        btnStatistics = (Button) findViewById(R.id.btnStat);
        btnUsers = (Button) findViewById(R.id.btnUser);

        restrictView();

        btnMyAnimals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MyAnimalsActivity.class);
                startActivity(intent);
            }
        });


        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOut();
            }
        });

        btnAnimalWorld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, AnimalWorldActivity.class);
                startActivity(intent);
            }
        });

        btnQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, QRActivity.class);
                startActivity(intent);
            }
        });

        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });

        btnStatistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, StatisticsActivity.class);
                startActivity(intent);
            }
        });

        btnUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, UserListActivity.class);
                startActivity(intent);
            }
        });

    }

    /**
     * Method logs out current user.
     */
    private void logOut() {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        CurrentUser.deactivateUser();
        startActivity(intent);
    }

    /**
     * Method that restricts current user view to his permission status.
     */
    private void restrictView() {
        if (CurrentUser.isActive()) {
            Log.d("user", "active");
        }
        if (CurrentUser.isAdmin()) {
            btnAnimalWorld.setEnabled(true);
            btnAnimalWorld.setVisibility(View.VISIBLE);
            btnStatistics.setEnabled(true);
            btnStatistics.setVisibility(View.VISIBLE);
            btnUsers.setEnabled(true);
            btnUsers.setVisibility(View.VISIBLE);
        } else if (CurrentUser.isGuardian()) {
            btnMyAnimals.setEnabled(true);
            btnMyAnimals.setVisibility(View.VISIBLE);

        } else if (CurrentUser.isVisitor()) {
            btnAnimalWorld.setEnabled(true);
            btnAnimalWorld.setVisibility(View.VISIBLE);
            btnMyAnimals.setEnabled(true);
            btnMyAnimals.setVisibility(View.VISIBLE);
            btnQR.setEnabled(true);
            btnQR.setVisibility(View.VISIBLE);
            btnMap.setEnabled(true);
            btnMap.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        backCount += 1;
        if (backCount == 2) {
            logOut();
        }
    }

    @Override
    protected void onResume() {
        backCount = 0;
        super.onResume();
    }
}
