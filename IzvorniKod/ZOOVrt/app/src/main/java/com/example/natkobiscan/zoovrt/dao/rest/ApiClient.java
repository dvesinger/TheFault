package com.example.natkobiscan.zoovrt.dao.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Class that keeps singleton Retrofit object.
 * Retrofit object handles connection to the rest server.
 *
 * @author Vedran Ivanušić
 * @author Domagoj Pluščec
 * @version v1.0, 30.12.2016.
 */
public class ApiClient {

    /**
     * Private utility class constructor.
     */
    private ApiClient() {

    }

    /**
     * HTTP url prefix.
     */
    private static final String HTTP_START = "http://";

    /**
     * Host IP address.
     */
    private static String hostIp = "192.168.0.12";

    /**
     * Host port.
     */
    private static String hostPort = "8080";

    /**
     * Rest service prefix.
     */
    private static String restPostfix = "ZOOService/rest/ZOOService/";

    /**
     * Retrofit instance reference.
     */
    private static Retrofit retrofit = null;


    /**
     * Method that returns(existing) retrofit object or creates a new one if it doesn't exist and returns it.
     *
     * @return retrofit object
     */
    public static Retrofit getClient() {
        if (retrofit == null) {
            initializeRetrofitClient();
        }
        return retrofit;
    }

    /**
     * Method initializes retrofit client instance.
     */
    private static void initializeRetrofitClient() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl(getBaseRestUrl())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    /**
     * Method obtains host's base url.
     *
     * @return base url
     */
    public static String getBaseUrl() {
        return HTTP_START + hostIp + ":" + hostPort + "/";
    }

    /**
     * Method obtains host's rest service base url.
     *
     * @return rest service base url
     */
    public static String getBaseRestUrl() {
        return getBaseUrl() + restPostfix;
    }

    /**
     * Method obtains host IP address.
     *
     * @return host IP address
     */
    public static String getHostIP() {
        return hostIp;
    }

    /**
     * Method sets host IP address and initializes client api.
     *
     * @param hostIp host IP address.
     */
    public static void setHostIp(String hostIp) {
        ApiClient.hostIp = hostIp;
        initializeRetrofitClient();
    }

}
