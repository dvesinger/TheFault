package com.example.natkobiscan.zoovrt.dao.rest.structures;

/**
 * REST structure for encapsulating interesting fact informations.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 8.1.2017.
 */
public class InterestingFactTriple {
    /**
     * Individual id.
     */
    private Integer individualId;
    /**
     * Interesting fact type id.
     */
    private Integer interestingFactTypeId;
    /**
     * Interesting fact content.
     */
    private String interestingFactContent;

    /**
     * Constructor that initializes all atributes to given values.
     *
     * @param individualId           individual id
     * @param interestingFactTypeId  interesting fact type id
     * @param interestingFactContent interesting fact content
     */
    public InterestingFactTriple(Integer individualId, Integer interestingFactTypeId, String interestingFactContent) {
        this.individualId = individualId;
        this.interestingFactTypeId = interestingFactTypeId;
        this.interestingFactContent = interestingFactContent;
    }

    /**
     * Empty java bean constructor.
     */
    public InterestingFactTriple() {
    }

    /**
     * Method obtains individual id.
     *
     * @return individual id
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * Method sets individual id.
     *
     * @param individualId individual id
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * Method obtains interesting fact type id.
     *
     * @return interesting fact type id
     */
    public Integer getInterestingFactTypeId() {
        return interestingFactTypeId;
    }

    /**
     * Method sets interesting fact type id.
     *
     * @param interestingFactTypeId interesting fact type id
     */
    public void setInterestingFactTypeId(Integer interestingFactTypeId) {
        this.interestingFactTypeId = interestingFactTypeId;
    }

    /**
     * Method obtains interesting fact content.
     *
     * @return interesting fact content
     */
    public String getInterestingFactContent() {
        return interestingFactContent;
    }

    /**
     * Method sets interesting fact content.
     *
     * @param interestingFactContent interesting fact content
     */
    public void setInterestingFactContent(String interestingFactContent) {
        this.interestingFactContent = interestingFactContent;
    }
}
