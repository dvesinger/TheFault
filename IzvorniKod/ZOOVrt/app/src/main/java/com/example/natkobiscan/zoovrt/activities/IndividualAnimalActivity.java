package com.example.natkobiscan.zoovrt.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.natkobiscan.zoovrt.R;
import com.example.natkobiscan.zoovrt.dao.DAOException;
import com.example.natkobiscan.zoovrt.dao.DAOProvider;
import com.example.natkobiscan.zoovrt.model.application.CurrentUser;
import com.example.natkobiscan.zoovrt.model.application.ZOOInterestingFactType;
import com.example.natkobiscan.zoovrt.model.database.Guardian;
import com.example.natkobiscan.zoovrt.model.database.InterestingFactType;
import com.example.natkobiscan.zoovrt.model.database.Visitor;
import com.example.natkobiscan.zoovrt.model.database.ZOOAnimalSpecies;
import com.example.natkobiscan.zoovrt.model.database.ZOOIndividual;
import com.example.natkobiscan.zoovrt.model.database.ZOOInterestingFact;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Activity that shows an animal individual.
 *
 * @author Lovro Kunović
 * @author Domagoj Pluščec
 * @version v1.0, 3.1.2017.
 */
public class IndividualAnimalActivity extends AppCompatActivity {

    /**
     * Extras id for animal individual to display.
     */
    public static final String ANIMAL_INDIVIDUAL_EXTRA_ID = "animal_individual_bundle";

    /**
     * Extras id for individual id obtained from qr scanning.
     */
    public static final String INDIVIDUAL_QR_ID_EXTRA_ID = "SCAN_RESULT";

    /**
     * Dialog age edit text.
     */
    private EditText etIndividualAgeDialog;
    /**
     * Dialog animal individual gender spinner.
     */
    private Spinner spIndividualGenderDialog;
    /**
     * Possible animal individual gender spinner values.
     */
    private String[] spIndividualGenderArray = new String[]{
            "N/A", "M", "F"
    };
    /**
     * Dialog animal individual birth place edit text.
     */
    private EditText etIndividualBirthPlaceDialog;
    /**
     * Dialog animal individual date arrival date picker.
     */
    private DatePicker dpIndividualDateArrivalDialog;
    /**
     * Dialog animal individual photography edit text.
     */
    private EditText etIndividualPhotographyDialog;
    /**
     * Edit dialog.
     */
    private AlertDialog editDialog;
    /**
     * Dialog interesting fact spinner interesting fact type values.
     */
    private String[] spInterestingFactTypeArray = new String[]{
            "tekst", "slika", "video"
    };
    /**
     * Current animal individual.
     */
    private ZOOIndividual currIndividual;
    /**
     * Animal individual description text view.
     */
    private TextView tvIndividualDescription;
    /**
     * Animal individual image view.
     */
    private ImageView ivIndividual;
    /**
     * Animal individual interesting facty linear layout container.
     */
    private LinearLayout llInterestingFacts;
    /**
     * Image button for editing animal individual.
     */
    private ImageButton ibIndividualEditButton;
    /**
     * Image button for adding interesting facts.
     */
    private ImageButton ibInterestingFactAddButton;
    /**
     * Button for individual adoption.
     */
    private Button btnAdopt;
    /**
     * Listener that manages animal individual adoption.
     */
    private View.OnClickListener adopterListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AsyncTask<Object, Void, Void> adoptTask = new AsyncTask<Object, Void, Void>() {

                @Override
                protected Void doInBackground(Object... params) {
                    DAOProvider.getVisitorDAO().adoptIndividual((Visitor) CurrentUser.getCurrentUser(), currIndividual);
                    CurrentUser.updateUser();
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    btnAdopt.setText("Posvojena jedinka");
                    btnAdopt.setEnabled(false);
                    ((Visitor) CurrentUser.getCurrentUser()).getJedinkes().add(currIndividual);
                    Toast toast = Toast.makeText(getApplicationContext(), "Poslan zahtjev za posvojenjem", Toast.LENGTH_LONG);
                    toast.show();
                    initGUI();
                }
            };
            adoptTask.execute();

        }
    };
    /**
     * Animal individual edit listener.
     */
    private View.OnClickListener individualEditListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(IndividualAnimalActivity.this);
            builder.setTitle("Uredi jedinku");
            LayoutInflater inflater = IndividualAnimalActivity.this.getLayoutInflater();

            builder.setView(inflater.inflate(R.layout.dialog_edit_animal_individual, null));


            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //leave empty - hack auto close dialog
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            editDialog = builder.create();
            editDialog.show();
            editDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int currAge = -1;
                    if (!etIndividualAgeDialog.getText().toString().trim().isEmpty()) {
                        try {
                            currAge = Integer.parseInt(etIndividualAgeDialog.getText().toString().trim());
                            if (currAge < 0) {
                                throw new IllegalArgumentException();
                            }
                        } catch (IllegalArgumentException ex) {
                            Toast toast = Toast.makeText(getApplicationContext(), "Neispravna dob jedinke.", Toast.LENGTH_SHORT);
                            toast.show();
                            return;
                        }
                        currIndividual.setDob(currAge);
                    }


                    currIndividual.setFotografija(etIndividualPhotographyDialog.getText().toString());

                    int selectedYear = dpIndividualDateArrivalDialog.getYear();
                    int selectedMonth = dpIndividualDateArrivalDialog.getMonth();
                    int selectedDay = dpIndividualDateArrivalDialog.getDayOfMonth();
                    if (!(selectedYear == 2017 && selectedMonth == 6 && selectedDay == 1)) {
                        Calendar selectedCalendar = Calendar.getInstance();
                        selectedCalendar.set(selectedYear, selectedMonth, selectedDay);
                        Long selectedTime = selectedCalendar.getTimeInMillis();
                        Date selectedDate = new Date(selectedTime);
                        Log.d("selecteddate", selectedDate.toString());
                        currIndividual.setDatumDol(selectedDate);
                    }
                    currIndividual.setMjestoRod(etIndividualBirthPlaceDialog.getText().toString());
                    String gender = (String) spIndividualGenderDialog.getSelectedItem();
                    if (gender.equals("N/A")) {
                        currIndividual.setSpol(null);
                    } else {
                        currIndividual.setSpol(gender);
                    }
                    UpdateIndividualTask updateIndividualTask = new UpdateIndividualTask();
                    updateIndividualTask.execute();
                    editDialog.dismiss();
                }
            });

            etIndividualAgeDialog = (EditText) editDialog.findViewById(R.id.etIndividualAgeDialog);
            etIndividualAgeDialog.setText(currIndividual.getDob() == null ? "" : currIndividual.getDob().toString());

            spIndividualGenderDialog = (Spinner) editDialog.findViewById(R.id.spIndividualGenderDialog);

            ArrayAdapter<String> adapter = new ArrayAdapter<>(IndividualAnimalActivity.this,
                    android.R.layout.simple_spinner_item, spIndividualGenderArray);
            spIndividualGenderDialog.setAdapter(adapter);
            if (currIndividual.getSpol() == null) {
                spIndividualGenderDialog.setSelection(0);
            } else if (currIndividual.getSpol().equals("M")) {
                spIndividualGenderDialog.setSelection(1);
            } else {
                spIndividualGenderDialog.setSelection(2);
            }

            etIndividualBirthPlaceDialog = (EditText) editDialog.findViewById(R.id.etIndividualBirthPlaceDialog);
            etIndividualBirthPlaceDialog.setText(currIndividual.getMjestoRod() == null ? "" : currIndividual.getMjestoRod().toString());
            dpIndividualDateArrivalDialog = (DatePicker) editDialog.findViewById(R.id.dpIndividualDateArrivalDialog);


            Calendar currCalendar = Calendar.getInstance();
            if (currIndividual.getDatumDol() != null) {
                Long individualDateArrival = currIndividual.getDatumDol().getTime();
                Calendar individualCalendar = Calendar.getInstance();
                individualCalendar.setTimeInMillis(individualDateArrival);
                dpIndividualDateArrivalDialog.updateDate(individualCalendar.get(Calendar.YEAR), individualCalendar.get(Calendar.MONTH), individualCalendar.get(Calendar.DAY_OF_MONTH));
            } else {
                dpIndividualDateArrivalDialog.updateDate(2017, 6, 1);
            }
            etIndividualPhotographyDialog = (EditText) editDialog.findViewById(R.id.etIndividualPhotographyDialog);
            etIndividualPhotographyDialog.setText(currIndividual.getFotografija() == null ? "" : currIndividual.getFotografija());

        }
    };
    /**
     * Dialog interesting fact add interesting fact type spinner.
     */
    private Spinner spInterrestingFactyType;
    /**
     * Dialog interesting fact add interesting fact content edit text.
     */
    private EditText etInterestingFactContent;
    /**
     * Alert dialog for adding interesting facts.
     */
    private AlertDialog addDialog;
    /**
     * Listener that manages addition of interesting facts.
     */
    private View.OnClickListener interestingFactAddListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(IndividualAnimalActivity.this);
            builder.setTitle("Dodaj zanimljivost");
            LayoutInflater inflater = IndividualAnimalActivity.this.getLayoutInflater();

            builder.setView(inflater.inflate(R.layout.dialog_add_interesting_fact, null));
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //leave empty - hack auto close dialog
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            addDialog = builder.create();
            addDialog.show();
            addDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AddInterestingFactTask addInterestingFactTask = new AddInterestingFactTask((String) spInterrestingFactyType.getSelectedItem(), etInterestingFactContent.getText().toString());
                    addInterestingFactTask.execute();
                    addDialog.dismiss();
                }
            });

            spInterrestingFactyType = (Spinner) addDialog.findViewById(R.id.spInterrestingFactyType);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(IndividualAnimalActivity.this,
                    android.R.layout.simple_spinner_item, spInterestingFactTypeArray);
            spInterrestingFactyType.setAdapter(adapter);
            etInterestingFactContent = (EditText) addDialog.findViewById(R.id.etInterestingFactContent);

        }
    };

    /**
     * Method composes animal individual description.
     *
     * @param currIndividual animal individual
     * @return animal individual description
     */
    private static String composeDescription(ZOOIndividual currIndividual) {
        StringBuilder sb = new StringBuilder();
        sb.append("Datum dolaska: ");
        if (currIndividual.getDatumDol() != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

            sb.append(dateFormat.format(currIndividual.getDatumDol()));
        }
        sb.append("\nDob jedinke: ");

        if (currIndividual.getDob() != null) {
            sb.append(currIndividual.getDob().toString());
        }
        sb.append("\nMjesto rođenja: ");
        if (currIndividual.getMjestoRod() != null) {
            sb.append(currIndividual.getMjestoRod().toString());
        }
        sb.append("\nSpol: ");
        if (currIndividual.getSpol() != null) {
            sb.append(currIndividual.getSpol());
        }
        sb.append("\n");

        return sb.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_animal);

        /**
         * Obtain data from intent.
         */
        Intent intent = getIntent();

        if (intent.hasExtra(INDIVIDUAL_QR_ID_EXTRA_ID)) {

            AsyncTask<Object, Void, ZOOIndividual> individualTask = new AsyncTask<Object, Void, ZOOIndividual>() {
                private int currID;

                @Override
                protected void onPreExecute() {
                    Intent intent = getIntent();
                    currID = (int) intent.getExtras().get(INDIVIDUAL_QR_ID_EXTRA_ID);
                }

                @Override
                protected ZOOIndividual doInBackground(Object... params) {
                    return DAOProvider.getVisitorDAO().getAnimalIndividual(currID);
                }

                @Override
                protected void onPostExecute(ZOOIndividual zooIndividual) {
                    currIndividual = zooIndividual;
                    initGUI();
                }
            };
            individualTask.execute();
        } else {
            currIndividual = (ZOOIndividual) intent.getExtras().get(ANIMAL_INDIVIDUAL_EXTRA_ID);
        /*
        * Initialize GUI references.
        */
            initGUI();
        }

    }

    @Override
    public void onBackPressed() {
        if (getIntent().hasExtra(INDIVIDUAL_QR_ID_EXTRA_ID)) {
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return;
        }

        super.onBackPressed();
    }

    /**
     * Method initializes GUI components.
     */
    private void initGUI() {
        setTitle(currIndividual.getIme());

        if (CurrentUser.isGuardian() || CurrentUser.isAdmin()) {
            ibIndividualEditButton = (ImageButton) findViewById(R.id.ibIndividualEditButton);
            ibIndividualEditButton.setEnabled(true);
            ibIndividualEditButton.setVisibility(View.VISIBLE);
            ibIndividualEditButton.setOnClickListener(individualEditListener);
        }

        if (CurrentUser.isGuardian()) {
            ibInterestingFactAddButton = (ImageButton) findViewById(R.id.ibInterestingFactAddButton);
            ibInterestingFactAddButton.setEnabled(true);
            ibInterestingFactAddButton.setVisibility(View.VISIBLE);
            ibInterestingFactAddButton.setOnClickListener(interestingFactAddListener);
        }


        if (CurrentUser.isVisitor()) {
            btnAdopt = (Button) findViewById(R.id.btnAdopt);
            btnAdopt.setVisibility(View.VISIBLE);
            if (!isAdopted()) {
                btnAdopt.setOnClickListener(adopterListener);
            } else {
                btnAdopt.setText("Posvojena jedinka");
                btnAdopt.setEnabled(false);

            }
        }


        if (currIndividual.getFotografija() != null) {

            AsyncTask imageTask = new AsyncTask<Object, Void, Bitmap>() {
                @Override
                protected Bitmap doInBackground(Object[] params) {
                    try {
                        Bitmap image = DAOProvider.getDAO().obtainImageFromUrl(currIndividual.getFotografija());
                        return image;
                    } catch (DAOException e) {
                        //return default image ?
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Bitmap image) {
                    if (image == null) {
                        return;
                    }
                    ivIndividual = (ImageView) findViewById(R.id.ivIndividual);
                    ivIndividual.setVisibility(View.VISIBLE);
                    ivIndividual.setImageBitmap(image);
                }
            };
            imageTask.execute();

        }
        tvIndividualDescription = (TextView) findViewById(R.id.tvIndividualDescription);
        tvIndividualDescription.setText(composeDescription(currIndividual));
        llInterestingFacts = (LinearLayout) findViewById(R.id.llInterestingFacts);
        tvInterestingFactsTitle = (TextView) findViewById(R.id.tvInterestingFactsTitle);
        if (CurrentUser.isVisitor() && checkAnimalAdopted()) {
            tvInterestingFactsTitle.setVisibility(View.VISIBLE);
            List<ZOOInterestingFact> interestingFacts = currIndividual.getZanimljivostis();
            fillInterestingFacts(llInterestingFacts, interestingFacts);
            llInterestingFacts.setVisibility(View.VISIBLE);
        } else if (CurrentUser.isGuardian() && checkAnimalCare()) {
            tvInterestingFactsTitle.setVisibility(View.VISIBLE);
            List<ZOOInterestingFact> interestingFacts = currIndividual.getZanimljivostis();
            fillInterestingFacts(llInterestingFacts, interestingFacts);
            llInterestingFacts.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Interesting facts title.
     */
    private TextView tvInterestingFactsTitle;

    /**
     * Method cheks if the current individual is on guardian cares list.
     *
     * @return true if the individual is on guardian cares list, false otherwise
     */
    private boolean checkAnimalCare() {
        List<ZOOIndividual> careIndividuals = new ArrayList<>();
        List<ZOOAnimalSpecies> careSpecies = ((Guardian) CurrentUser.getCurrentUser()).getZivotinjskevrstes();

        for (ZOOAnimalSpecies species : careSpecies) {
            if (species.getJedinkes() == null) {
                continue;
            }
            careIndividuals.addAll(species.getJedinkes());
        }

        for (ZOOIndividual indiv : careIndividuals) {
            if (indiv.getIdJedinke().equals(currIndividual.getIdJedinke())) {
                // currIndividual.setZanimljivostis(indiv.getZanimljivostis());
                return true;
            }
        }
        return false;
    }

    /**
     * Method checks if the current user has adopted current individual and obtains individuals interesting facts.
     *
     * @return true if the current user has adopted current individual, false otherwise
     */
    private boolean checkAnimalAdopted() {
        List<ZOOIndividual> adoptedAnimals = ((Visitor) CurrentUser.getCurrentUser()).getJedinkes();
        for (ZOOIndividual indiv : adoptedAnimals) {
            if (indiv.getIdJedinke().equals(currIndividual.getIdJedinke())) {
                currIndividual.setZanimljivostis(indiv.getZanimljivostis());
                return true;
            }
        }
        return false;
    }

    /**
     * Method cheks if the current animal is adopted.
     *
     * @return true if animal is adopted, false otherwise
     */
    private boolean isAdopted() {
        List<ZOOIndividual> adoptedAnimals = ((Visitor) CurrentUser.getCurrentUser()).getJedinkes();
        for (ZOOIndividual adopt : adoptedAnimals) {
            if (adopt.getIdJedinke().equals(currIndividual.getIdJedinke())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method fills current list of interesting facts.
     *
     * @param llInterestingFacts linear layout container for interesting facts
     * @param interestingFacts   list of interesting facts
     */
    private void fillInterestingFacts(LinearLayout llInterestingFacts, List<ZOOInterestingFact> interestingFacts) {
        Log.d("fill", "fill");
        if (llInterestingFacts.getChildCount() > 0) {
            llInterestingFacts.removeAllViews();
        }

        for (final ZOOInterestingFact fact : interestingFacts) {
            int typeId = fact.getVrstazanimljivosti().getIdVrsteZanimljivosti();
            switch (ZOOInterestingFactType.getTypeFromId(typeId)) {
                case TEXT:
                    TextView currTv = new TextView(this);
                    currTv.setText(fact.getZanimljivost());
                    currTv.setLayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    llInterestingFacts.addView(currTv);
                    break;
                case IMAGE:
                    final ImageView currIv = new ImageView(this);
                    currIv.setLayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    llInterestingFacts.addView(currIv);
                    AsyncTask imageTask = new AsyncTask<Object, Void, Bitmap>() {

                        String url;
                        ImageView iv;

                        @Override
                        protected void onPreExecute() {
                            url = fact.getZanimljivost();
                            iv = currIv;
                        }

                        @Override
                        protected Bitmap doInBackground(Object[] params) {
                            try {
                                Bitmap image = DAOProvider.getDAO().obtainImageFromUrl(url);
                                return image;
                            } catch (DAOException e) {
                                //return default image ?
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Bitmap image) {
                            if (image == null) {
                                return;
                            }
                            currIv.setImageBitmap(image);
                        }
                    };
                    imageTask.execute();
                    break;
                case VIDEO:
                    Log.d("Video", "video" + fact.getZanimljivost());
                    try {
                        Button videoView = new Button(IndividualAnimalActivity.this);
                        videoView.setBackgroundResource(R.drawable.button_background);
                        videoView.setText("Video zapis");
                        videoView.setLayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                        videoView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(fact.getZanimljivost()));
                                startActivity(browserIntent);
                            }
                        });

                        llInterestingFacts.addView(videoView);

                    } catch (Exception logable) {
                        logable.printStackTrace();
                    }
                    break;
                default:
                    continue;
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.itHomeBack:
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            default:
                super.onOptionsItemSelected(item);
                return true;
        }


    }

    @Override
    protected void onResume() {
        if (currIndividual != null) {
            UpdateIndividualTask updateTask = new UpdateIndividualTask();
            updateTask.execute();
        }
        super.onResume();
    }

    /**
     * Task for updating current individual.
     */
    private class UpdateIndividualTask extends AsyncTask<Object, Void, ZOOIndividual> {
        @Override
        protected ZOOIndividual doInBackground(Object[] params) {
            DAOProvider.getDAO().updateIndividual(currIndividual);
            return DAOProvider.getVisitorDAO().getAnimalIndividual(currIndividual.getIdJedinke());
        }

        @Override
        protected void onPostExecute(ZOOIndividual newIndividualState) {
            currIndividual = newIndividualState;
            initGUI();
        }
    }

    /**
     * Async task for persisting new interesting fact.
     */
    private class AddInterestingFactTask extends AsyncTask<Object, Void, ZOOIndividual> {
        /**
         * Interesting fact type.
         */
        private String interestingFactType;
        /**
         * Interesting fact type id.
         */
        private int interestingFactTypeId;
        /**
         * Interesting fact content.
         */
        private String interestingFactContent;

        /**
         * Construcotr that initializes async task parameters.
         *
         * @param interestingFactType    interesting fact type.
         * @param interestingFactContent interesting fact content.
         */
        public AddInterestingFactTask(String interestingFactType, String interestingFactContent) {
            super();
            this.interestingFactType = interestingFactType;
            this.interestingFactContent = interestingFactContent;
            if (interestingFactType.equals("tekst")) {
                interestingFactTypeId = 1;
            } else if (interestingFactType.equals("slika")) {
                interestingFactTypeId = 2;
            } else if (interestingFactType.equals("video")) {
                interestingFactTypeId = 3;
            }
        }

        @Override
        protected ZOOIndividual doInBackground(Object[] params) {
            ZOOInterestingFact interestingFact = new ZOOInterestingFact();
            interestingFact.setDatum(new Date(System.currentTimeMillis()));
            interestingFact.setJedinke(currIndividual);
            interestingFact.setZanimljivost(interestingFactContent);
            InterestingFactType factType = new InterestingFactType();
            factType.setVrstaZanimljivosti(interestingFactType);
            factType.setIdVrsteZanimljivosti(interestingFactTypeId);
            interestingFact.setVrstazanimljivosti(factType);
            DAOProvider.getGuadrianDAO().addInterestingFact(interestingFact, currIndividual);
            ZOOIndividual newIndividualState = DAOProvider.getVisitorDAO().getAnimalIndividual(currIndividual.getIdJedinke());
            return newIndividualState;
        }

        @Override
        protected void onPostExecute(ZOOIndividual newIndividualState) {

            currIndividual = newIndividualState;
            CurrentUser.updateUser();
            initGUI();
        }
    }
}