package com.example.natkobiscan.zoovrt.activities.visitor;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.natkobiscan.zoovrt.R;
import com.example.natkobiscan.zoovrt.activities.AnimalSpeciesActivity;
import com.example.natkobiscan.zoovrt.activities.HomeActivity;
import com.example.natkobiscan.zoovrt.activities.IndividualAnimalActivity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

/**
 * QR scanner activity.
 *
 * @author Lovro Kunović
 * @author Domagoj Pluščec
 * @version v1.0, 3.1.2017.
 */
public class QRActivity extends AppCompatActivity {

    /**
     * Internal camera permission request id.
     */
    private static final int PERMISSIONS_REQUEST_CAMERA = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(QRActivity.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(QRActivity.this,
                    Manifest.permission.CAMERA)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {
                ActivityCompat.requestPermissions(QRActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        PERMISSIONS_REQUEST_CAMERA);
            }
        }


        if (ContextCompat.checkSelfPermission(QRActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            IntentIntegrator.initiateScan(this);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    IntentIntegrator.initiateScan(this);

                } else {
                    finish();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            default:
                return;

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "You cancelled the scan!", Toast.LENGTH_LONG).show();
                onBackPressed();
            } else {
                String resultString = result.getContents().trim();
                Log.d("QRres", " " + resultString);
                try {
                    if (resultString.startsWith("INDIVIDUAL")) {
                        int resultID = extractId(resultString, "INDIVIDUAL".length());
                        Intent intent = new Intent(QRActivity.this, IndividualAnimalActivity.class);
                        intent.putExtra(IndividualAnimalActivity.INDIVIDUAL_QR_ID_EXTRA_ID, resultID);
                        startActivity(intent);
                    } else if (resultString.startsWith("SPECIES")) {
                        int resultID = extractId(resultString, "SPECIES".length());
                        Intent intent = new Intent(QRActivity.this, AnimalSpeciesActivity.class);
                        intent.putExtra(AnimalSpeciesActivity.SPECIES_QR_ID_EXTRA_ID, resultID);
                        startActivity(intent);
                    } else {
                        throw new RuntimeException("Neispravan QR kod");
                    }


                } catch (Exception e) {
                    Toast.makeText(this, "Neispravan QR kod!", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * Method extracts id from string.
     *
     * @param resultString qr result string
     * @param offset       from which index to extract id
     * @return id
     */
    private int extractId(String resultString, int offset) {
        return Integer.parseInt(resultString.substring(offset).trim());
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(QRActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }


}
