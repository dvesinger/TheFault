package hr.fer.zoovrt.dao.jpa;

import hr.fer.zoovrt.dao.DAO;
import hr.fer.zoovrt.dao.DAOException;
import hr.fer.zoovrt.model.PermissionType;
import hr.fer.zoovrt.model.Permissions;
import hr.fer.zoovrt.model.User;
import hr.fer.zoovrt.model.ZOOAnimalClass;
import hr.fer.zoovrt.model.ZOOAnimalSpecies;
import hr.fer.zoovrt.model.ZOOUser;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;

/**
 * JPA DAO implementation class for general user.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.6.2015.
 */
public class JPADAOImpl implements DAO {

    @Override
    public boolean checkUser(User user) throws DAOException {
        EntityManager em = JPAEMProvider.getEntityManager();

        List<ZOOUser> users = em
                .createQuery(
                        "select u from korisnik as u where u.korisnickoIme=:username "
                                + "and u.lozinka=:password", ZOOUser.class)
                .setParameter("username", user.getKorisnickoIme())
                .setParameter("password", user.getLozinka()).getResultList();

        if (users.size() == 0) {
            return false;
        }
        return true;
    }

    @Override
    public boolean createUser(ZOOUser user) throws DAOException {
        EntityManager em = JPAEMProvider.getEntityManager();

        if (user == null || user.getKorisnickoIme() == null || user.getKorisnickoIme().isEmpty()) {
            System.out.println("Create user - invalid user");
            return false;
        }

        if (checkUsernameExists(user.getKorisnickoIme())) {
            System.out.println("---");
            return false;
        }

        user.setOvlasti(getPermission(PermissionType.VISITOR.getValue()));
        user.getOvlasti().addKorisnik(user);
        user.setPosjecenosts(null);
        user.setZivotinjskevrstes(null);

        System.out.println("#######");
        Session session = em.unwrap(Session.class);
        session.save(user);

        return true;
    }

    @Override
    public Permissions getPermission(int idOvlasti) {
        EntityManager em = JPAEMProvider.getEntityManager();

        Permissions perm = em
                .createQuery("select p from ovlasti as p where p.idOvlasti=:id ", Permissions.class)
                .setParameter("id", idOvlasti).getSingleResult();

        return perm;
    }

    @Override
    public User getUser(String username) throws DAOException {
        if (username == null) {
            return null;
        }

        EntityManager em = JPAEMProvider.getEntityManager();

        List<ZOOUser> users = em
                .createQuery("select u from korisnik as u where u.korisnickoIme=:username",
                        ZOOUser.class).setParameter("username", username).getResultList();

        if (users.size() == 0) {
            return null;
        }

        return users.get(0);
    }

    @Override
    public boolean checkUsernameExists(String username) throws DAOException {
        EntityManager em = JPAEMProvider.getEntityManager();
        if (em.createQuery("select u from korisnik as u where u.korisnickoIme=:username",
                ZOOUser.class).setParameter("username", username).getResultList().size() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Permissions getUserPermission(String username) throws DAOException {
        return getUser(username).getOvlasti();

    }

    @Override
    public List<ZOOAnimalClass> getAnimalClasses() {
        EntityManager em = JPAEMProvider.getEntityManager();
        return em.createNamedQuery("Razredzivotinja.findAll", ZOOAnimalClass.class).getResultList();
    }

    @Override
    public List<ZOOAnimalSpecies> getAnimalSpecies() {
        EntityManager em = JPAEMProvider.getEntityManager();
        List<ZOOAnimalSpecies> species = em.createNamedQuery("zivotinjskevrste.findAll",
                ZOOAnimalSpecies.class).getResultList();
        return species;
    }

}
