package hr.fer.zoovrt.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonBackReference;

/**
 * The persistent class for the zanimljivosti database table.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 20.12.2016.
 */
@Entity(name = "zanimljivosti")
@Table(name = "zanimljivosti")
@NamedQuery(name = "Zanimljivosti.findAll", query = "SELECT z FROM zanimljivosti z")
@XmlRootElement(name = "interesting_fact")
public class ZOOInterestingFact implements Serializable {

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = 1310468520500632975L;

    /**
     * Interesting fact id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idZanimljivosti;

    /**
     * Interesting fact creation date.
     */
    @Temporal(TemporalType.DATE)
    private Date datum;

    /**
     * Interesting fact content.
     */
    @Lob
    private String zanimljivost;

    /**
     * Individual connected with interesting fact.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idJedinke")
    private ZOOIndividual jedinke;

    /**
     * Type of interesting fact.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idVrstaZanimljivosti")
    private InterestingFactType vrstazanimljivosti;

    /**
     * Java bean empty constructor.
     */
    public ZOOInterestingFact() {
    }

    /**
     * Method obtains interesting fact creation date.
     *
     * @return creation date
     */
    public Date getDatum() {
        return datum;
    }

    /**
     * Method obtains interesting fact id.
     *
     * @return interesting fact id
     */
    public int getIdZanimljivosti() {
        return idZanimljivosti;
    }

    /**
     * Method obtains individual connected with interesting fact.
     *
     * @return zoo individual
     */
    public ZOOIndividual getJedinke() {
        return jedinke;
    }

    /**
     * Method obtains interesting fact type.
     *
     * @return interesting fact type
     */
    public InterestingFactType getVrstazanimljivosti() {
        return vrstazanimljivosti;
    }

    /**
     * Method obtains interesting fact content.
     *
     * @return interesting fact content
     */
    public String getZanimljivost() {
        return zanimljivost;
    }

    /**
     * Method sets interesting fact creation date.
     *
     * @param datum
     *            interesting fact creation date
     */
    public void setDatum(Date datum) {
        this.datum = datum;
    }

    /**
     * Setter method for interesting fact id.
     *
     * @param idZanimljivosti
     *            interesting fact id
     */
    public void setIdZanimljivosti(int idZanimljivosti) {
        this.idZanimljivosti = idZanimljivosti;
    }

    /**
     * Setter method for individual connected with interesting fact.
     *
     * @param jedinke
     *            zoo individual
     */
    @JsonBackReference
    public void setJedinke(ZOOIndividual jedinke) {
        this.jedinke = jedinke;
    }

    /**
     * Setter method for interesting fact type.
     *
     * @param vrstazanimljivosti
     *            interesting fact type
     */
    public void setVrstazanimljivosti(InterestingFactType vrstazanimljivosti) {
        this.vrstazanimljivosti = vrstazanimljivosti;
    }

    /**
     * Method sets interesting fact content.
     *
     * @param zanimljivost
     *            interesting fact content
     */
    public void setZanimljivost(String zanimljivost) {
        this.zanimljivost = zanimljivost;
    }

}
