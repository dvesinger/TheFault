package hr.fer.zoovrt.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * The persistent class for the ovlasti database table.
 *
 */
@Entity(name = "ovlasti")
@Table(name = "ovlasti")
@NamedQuery(name = "Ovlasti.findAll", query = "SELECT o FROM ovlasti o")
@XmlRootElement(name = "Permission")
public class Permissions implements Serializable {

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = -3871045955570132310L;

    /**
     * Permission id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idOvlasti;

    /**
     * Permission name.
     */
    private String nazivOvlasti;

    /**
     * List of users with given permission.
     */
    // bi-directional many-to-one association to Korisnik
    @OneToMany(mappedBy = "ovlasti")
    private List<ZOOUser> korisniks;

    /**
     * Java bean empty constructor.
     */
    public Permissions() {
    }

    /**
     * Method adds user to list of users with current permission.
     *
     * @param korisnik
     *            user to be added
     * @return reference to given user
     */
    public ZOOUser addKorisnik(ZOOUser korisnik) {
        if (korisniks == null) {
            korisniks = new ArrayList<>();
        }
        getKorisniks().add(korisnik);
        korisnik.setOvlasti(this);

        return korisnik;
    }

    /**
     * Getter method for permission id.
     *
     * @return permission id
     */
    public int getIdOvlasti() {
        return idOvlasti;
    }

    /**
     * Getter method for users that have current permission.
     *
     * @return list of users that have current permission
     */
    public List<ZOOUser> getKorisniks() {
        return korisniks;
    }

    /**
     * Getter method for permission name.
     *
     * @return permission name
     */
    public String getNazivOvlasti() {
        return nazivOvlasti;
    }

    /**
     * Method removes given user from user list.
     *
     * @param korisnik
     *            user to be removed
     * @return removed user
     */
    public ZOOUser removeKorisnik(ZOOUser korisnik) {
        getKorisniks().remove(korisnik);
        korisnik.setOvlasti(null);
        return korisnik;
    }

    /**
     * Setter method for permission Id.
     *
     * @param idOvlasti
     *            permission id
     */
    @XmlElement
    public void setIdOvlasti(int idOvlasti) {
        this.idOvlasti = idOvlasti;
    }

    /**
     * Method sets list of users with current permission.
     *
     * @param korisniks
     *            list of users with current permission
     */
    @XmlTransient
    public void setKorisniks(List<ZOOUser> korisniks) {
        this.korisniks = korisniks;
    }

    /**
     * Method obtains user's usernames.
     *
     * @return list of user's usernames
     */
    @XmlElementWrapper(name = "list_user_names")
    @XmlElement
    public List<String> getUsersUsername() {
        List<String> ids = new ArrayList<>();
        for (ZOOUser user : korisniks) {
            ids.add(user.getKorisnickoIme());
        }
        return ids;
    }

    /**
     * Method sets list of user's usernames that have current permission. It is
     * used as a mock for XML api.
     *
     * @param ignorable
     *            list of user's usernames
     */
    public void setUsersUsername(List<String> ignorable) {

    }

    /**
     * Method sets permission name.
     *
     * @param nazivOvlasti
     *            permission name
     */
    @XmlElement
    public void setNazivOvlasti(String nazivOvlasti) {
        this.nazivOvlasti = nazivOvlasti;
    }

}
