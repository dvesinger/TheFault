package hr.fer.zoovrt.dao;

import hr.fer.zoovrt.model.Admin;
import hr.fer.zoovrt.model.Guardian;
import hr.fer.zoovrt.model.User;
import hr.fer.zoovrt.model.Visitor;
import hr.fer.zoovrt.model.ZOOAnimalSpecies;
import hr.fer.zoovrt.model.ZOOIndividual;

import java.util.List;
import java.util.Map;

/**
 * Data access object interface for administrator.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 25.12.2016.
 */
public interface DAOAdmin extends DAO {
    /**
     * Method creates animal species in data storage.
     *
     * @param speciesName
     *            species name to persist
     * @param animalClassId
     *            animal class id
     * @return true if creation was successful, false otherwise
     * @throws DAOException
     *             if there was an error while accessing data
     */
    boolean createAnimalSpecies(String speciesName, Integer animalClassId);

    /**
     * Method deletes animal species from data storage.
     *
     * @param species
     *            species to persist
     * @return true if deletion was successfull, false otherwise
     * @throws DAOException
     *             if there was an error while accessing data
     */
    boolean deleteAnimalSpecies(ZOOAnimalSpecies species) throws DAOException;

    /**
     * Method for obtaining all visitors.
     *
     * @return list of visitors (empty list if there are no visitors)
     * @throws DAOException
     *             if there was an error while accessing data
     */
    List<Visitor> getAllVisitors() throws DAOException;

    /**
     * Method updates visitor data with given visitor.
     *
     * @param visitor
     *            visitor to be updated
     * @throws DAOException
     *             if there was an error while accessing data
     */
    void updateVisitor(Visitor visitor) throws DAOException;

    /**
     * Method for obtaining all guardians.
     *
     * @return list of guardians (empty list if there are no guardians)
     * @throws DAOException
     *             if there was an error while accessing data
     */
    List<Guardian> getAllGuardians() throws DAOException;

    /**
     * Method updates guardian data with given guardian.
     *
     * @param guardian
     *            to be updated
     * @throws DAOException
     *             if there was an error while accessing data
     */
    void updateGuardian(Guardian guardian) throws DAOException;

    /**
     * Method for obtaining all admins.
     *
     * @return list of admins (empty list if there are no admins)
     * @throws DAOException
     *             if there was an error while accessing data
     */
    List<Admin> getAllAdmins() throws DAOException;

    /**
     * Method updates admin data with given admin.
     *
     * @param admin
     *            to be updated
     * @throws DAOException
     *             if there was an error while accessing data
     */
    void updateAdmin(Admin admin) throws DAOException;

    /**
     * Method updates user permission.
     *
     * @param username
     *            user instance
     * @param permissionId
     *            new permission id
     * @return true if update was successful, false otherwise
     */
    boolean updateUserPermission(String username, Integer permissionId);

    /**
     * Method removes user from data storage.
     *
     * @param username
     *            user to be removed
     * @throws DAOException
     *             if there was an error while accessing data
     * @return true if deletion was sucessful, false otherwise
     */
    boolean deleteUser(String username) throws DAOException;

    /**
     * Method obtains all users from data storage.
     *
     * @return list of users
     * @throws DAOException
     *             if there was an error while accessing data
     */
    List<User> getAllUsers() throws DAOException;

    /**
     * Method obtains visitor visits statistics.
     *
     * @return map with animal species visit frequency
     * @throws DAOException
     *             if there was an error while accessing data
     */
    Map<ZOOAnimalSpecies, Integer> getVisitorStatistics() throws DAOException;

    /**
     * Method creates animal individual with given individual name and animal
     * species id.
     *
     * @param individualName
     *            new animal individual name
     * @param animalSpeciesId
     *            species to whom individual belongs to
     * @return true if the creation was successful, false otherwise
     * @throws DAOException
     *             if there was an error while accessing data
     */
    boolean createAnimalIndividual(String individualName, Integer animalSpeciesId)
            throws DAOException;

    /**
     * Method deletes given animal individual.
     *
     * @param individual
     *            animal individual to delete
     * @return true if the deletion was successful, false otherwise
     * @throws DAOException
     *             if there was an error while accessing data
     */
    boolean deleteAnimalIndividual(ZOOIndividual individual) throws DAOException;

    /**
     * Method updates guardians cares.
     *
     * @param username
     *            guardian's username
     * @param animalCaresIds
     *            list of current animal cares
     * @return true if the update was successful, false otherwise
     * @throws DAOException
     *             if there was an error while accessing data
     */
    boolean updateGuardianCares(String username, List<Integer> animalCaresIds) throws DAOException;

    /**
     * Method updates given animal species.
     *
     * @param currAnimalSpecies
     *            new animal species informations
     * @return true if the update was successful, false otherwise
     * @throws DAOException
     *             if there was an error while accessing data
     */
    boolean updateSpecies(ZOOAnimalSpecies currAnimalSpecies) throws DAOException;

    /**
     * Method updates given animal individual.
     *
     * @param currIndividual
     *            new animal individual informations
     * @return true if the update was successful, false otherwise
     * @throws DAOException
     *             if there was an error while accessing data
     */
    boolean updateIndividual(ZOOIndividual currIndividual) throws DAOException;

}
