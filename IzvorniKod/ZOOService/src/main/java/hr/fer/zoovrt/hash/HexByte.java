package hr.fer.zoovrt.hash;

/**
 * Class implementing the conversions between hexadecimal strings and byte
 * arrays.
 *
 * @author Domagoj Pluscec
 * @version v1.2, 25.6.2015.
 *
 */
public class HexByte {

    /**
     * Integnal representation of hex characters.
     */
    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    /**
     * Hex characters.
     */
    private static final String HEX_STRING = "0123456789ABCDEF";
    /**
     * Hex base.
     */
    private static final int HEX_BASE = 16;

    /**
     * Method for converting byte array to hexadecimal string.
     *
     * @param bytes
     *            array of bytes to be converted
     * @return String representing the given bytes when converted to hexadecimal
     * @throws IllegalArgumentException
     *             if the given argument is a null.
     */
    public static String byteArrayToHex(byte[] bytes) {

        if (bytes == null) {
            throw new IllegalArgumentException("Given byte array cannot be null.");
        }
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * Method for converting string consisted of hexadecimal characters to byte
     * array.
     *
     * @param hex
     *            String with hexadecimal characters
     * @return bytes representing wanted conversion.
     * @throws IllegalArgumentException
     *             if the given string contains an even number of characters or
     *             if a char is not hexadecimal of if a given string is a null
     *
     */
    public static byte[] hextobyte(String hex) {
        if (hex == null) {
            throw new IllegalArgumentException("Null cannot be converted to byte array.");
        }
        hex = hex.toUpperCase();
        if ((hex.length() % 2) != 0) {
            throw new IllegalArgumentException(
                    "Input string must contain an even number of characters."
                            + " Because method doesn't know where 0 should "
                            + "be placed because of endian format.");
        }

        final byte[] result = new byte[hex.length() / 2];
        final char[] hexCharacters = hex.toCharArray();
        for (int i = 0; i < hexCharacters.length; i += 2) {
            if (HEX_STRING.indexOf(hexCharacters[i]) < 0) {
                throw new IllegalArgumentException(hexCharacters[i]
                        + " is not a valid hexadecimal character.");
            }
            StringBuilder sb = new StringBuilder(2);
            sb.append(hexCharacters[i]).append(hexCharacters[i + 1]);
            result[i / 2] = (byte) Integer.parseInt(sb.toString(), HEX_BASE);
        }
        return result;
    }

    /**
     * Private utility class constructor.
     */
    private HexByte() {
    }
}
