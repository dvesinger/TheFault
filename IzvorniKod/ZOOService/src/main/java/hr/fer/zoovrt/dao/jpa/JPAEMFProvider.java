package hr.fer.zoovrt.dao.jpa;

import javax.persistence.EntityManagerFactory;

/**
 * JPA entity manager provider class.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.6.2015.
 */
public class JPAEMFProvider {

    /**
     * Entity manager factory.
     */
    private static EntityManagerFactory emf;

    /**
     * Method for obtaining entity manager factory.
     *
     * @return entity manager factory
     */
    public static EntityManagerFactory getEmf() {
        return emf;
    }

    /**
     * Method for setting entity manager factory.
     *
     * @param emf
     *            entity manager factory
     */
    public static void setEmf(EntityManagerFactory emf) {
        JPAEMFProvider.emf = emf;
    }

    /**
     * Utility class private constructor.
     */
    private JPAEMFProvider() {
    }
}
