package hr.fer.zoovrt.dao.jpa;

import hr.fer.zoovrt.dao.DAOAdmin;
import hr.fer.zoovrt.dao.DAOException;
import hr.fer.zoovrt.dao.DAOProvider;
import hr.fer.zoovrt.model.Admin;
import hr.fer.zoovrt.model.Guardian;
import hr.fer.zoovrt.model.Permissions;
import hr.fer.zoovrt.model.User;
import hr.fer.zoovrt.model.Visit;
import hr.fer.zoovrt.model.Visitor;
import hr.fer.zoovrt.model.ZOOAnimalSpecies;
import hr.fer.zoovrt.model.ZOOIndividual;
import hr.fer.zoovrt.model.ZOOUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

/**
 * JPA DAO implementation class for administrator.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.6.2015.
 */
public class JPAAdminDAO extends JPADAOImpl implements DAOAdmin {

    @Override
    public boolean createAnimalIndividual(String individualName, Integer animalSpeciesId) {
        EntityManager em = JPAEMProvider.getEntityManager();
        em.createNativeQuery(
                "INSERT INTO `zoovrt`.`jedinke` (`ime`, `idVrste`) VALUES ('" + individualName
                        + "', '" + animalSpeciesId + "')").executeUpdate();
        System.err.println("CREATE INDIVIDUAL");
        return true;
    }

    @Override
    public boolean createAnimalSpecies(String speciesName, Integer animalClassId)
            throws DAOException {
        EntityManager em = JPAEMProvider.getEntityManager();
        em.createNativeQuery(
                "INSERT INTO `zoovrt`.`zivotinjskevrste` (`imeVrste`, `idRazredVrste`) VALUES ('"
                        + speciesName + "', '" + animalClassId + "')").executeUpdate();
        System.err.println("CREATE SPECIES");
        return true;

    }

    @Override
    public boolean deleteAnimalIndividual(ZOOIndividual individual) {
        EntityManager em = JPAEMProvider.getEntityManager();
        List<ZOOIndividual> obtainedIndividuals = em
                .createQuery("select u from jedinke as u where u.idJedinke=:IndividualID",
                        ZOOIndividual.class)
                .setParameter("IndividualID", individual.getIdJedinke()).getResultList();

        if (obtainedIndividuals.size() == 0) {
            return false;
        }

        ZOOIndividual individualToDelete = obtainedIndividuals.get(0);
        em.remove(individualToDelete);
        return true;
    }

    @Override
    public boolean deleteAnimalSpecies(ZOOAnimalSpecies species) throws DAOException {
        EntityManager em = JPAEMProvider.getEntityManager();
        List<ZOOAnimalSpecies> obtainedSpecies = em
                .createQuery("select u from zivotinjskevrste as u where u.idVrste=:SpeciesID",
                        ZOOAnimalSpecies.class).setParameter("SpeciesID", species.getIdVrste())
                .getResultList();

        if (obtainedSpecies.size() == 0) {
            return false;
        }

        ZOOAnimalSpecies speciesToDelete = obtainedSpecies.get(0);
        em.remove(speciesToDelete);
        return true;

    }

    @Override
    public boolean deleteUser(String username) throws DAOException {
        EntityManager em = JPAEMProvider.getEntityManager();
        em.createNativeQuery(
                "DELETE FROM `zoovrt`.`korisnik` WHERE `korisnickoIme`='" + username + "'")
                .executeUpdate();

        return true;

    }

    @Override
    public List<Admin> getAllAdmins() throws DAOException {
        EntityManager em = JPAEMProvider.getEntityManager();
        List<Admin> users = em.createNamedQuery("Admin.findAll", Admin.class).getResultList();
        return users;
    }

    @Override
    public List<Guardian> getAllGuardians() throws DAOException {
        EntityManager em = JPAEMProvider.getEntityManager();
        List<Guardian> users = em.createNamedQuery("Guardian.findAll", Guardian.class)
                .getResultList();
        return users;
    }

    @Override
    public List<User> getAllUsers() throws DAOException {
        List<User> users = new ArrayList<>();
        List<Admin> admins = getAllAdmins();
        List<Guardian> guardian = getAllGuardians();
        List<Visitor> visitors = getAllVisitors();
        users.addAll(admins);
        users.addAll(guardian);
        users.addAll(visitors);
        return users;
    }

    @Override
    public List<Visitor> getAllVisitors() throws DAOException {
        EntityManager em = JPAEMProvider.getEntityManager();

        List<Visitor> users = em.createNamedQuery("Posjetitelj.findAll", Visitor.class)
                .getResultList();
        return users;
    }

    @Override
    public Map<ZOOAnimalSpecies, Integer> getVisitorStatistics() {
        EntityManager em = JPAEMProvider.getEntityManager();
        List<ZOOAnimalSpecies> species = em.createNamedQuery("zivotinjskevrste.findAll",
                ZOOAnimalSpecies.class).getResultList();
        Map<ZOOAnimalSpecies, Integer> stats = new HashMap<ZOOAnimalSpecies, Integer>();
        for (ZOOAnimalSpecies spec : species) {
            List<Visit> visits = spec.getPosjecenosts();
            stats.put(spec, visits == null ? 0 : visits.size());
        }
        return stats;
    }

    @Override
    public void updateAdmin(Admin entry) throws DAOException {
        EntityManager em = JPAEMProvider.getEntityManager();
        ZOOUser user = (ZOOUser) DAOProvider.getAdminDAO().getUser(entry.getKorisnickoIme());
        user.setIme(entry.getIme());
        user.setPrezime(entry.getPrezime());
        user.setGodinaRod(entry.getGodinaRod());
        user.setGrad(entry.getGrad());
        user.setEmail(entry.getEmail());

        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }

        em.merge(user);

    }

    @Override
    public void updateGuardian(Guardian entry) throws DAOException {
        EntityManager em = JPAEMProvider.getEntityManager();
        ZOOUser user = (ZOOUser) DAOProvider.getAdminDAO().getUser(entry.getKorisnickoIme());
        user.setIme(entry.getIme());
        user.setPrezime(entry.getPrezime());
        user.setGodinaRod(entry.getGodinaRod());
        user.setGrad(entry.getGrad());
        user.setEmail(entry.getEmail());

        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }

        em.merge(user);

    }

    @Override
    public boolean updateUserPermission(String username, Integer permissionId) {
        EntityManager em = JPAEMProvider.getEntityManager();
        ZOOUser user = (ZOOUser) DAOProvider.getAdminDAO().getUser(username);
        Permissions perm = DAOProvider.getAdminDAO().getPermission(permissionId);
        if (user == null || perm == null) {
            return false;
        }
        user.setOvlasti(perm);
        em.merge(perm);
        return true;

    }

    @Override
    public void updateVisitor(Visitor entry) throws DAOException {
        System.out.println("Update visitor");
        EntityManager em = JPAEMProvider.getEntityManager();
        ZOOUser user = (ZOOUser) DAOProvider.getAdminDAO().getUser(entry.getKorisnickoIme());
        user.setIme(entry.getIme());
        user.setPrezime(entry.getPrezime());
        user.setGodinaRod(entry.getGodinaRod());
        user.setGrad(entry.getGrad());
        user.setEmail(entry.getEmail());

        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }

        em.merge(user);

    }

    @Override
    public boolean updateGuardianCares(String username, List<Integer> animalCaresIds) {
        System.out.println("Update guardian cares");
        EntityManager em = JPAEMProvider.getEntityManager();
        ZOOUser user = (ZOOUser) DAOProvider.getAdminDAO().getUser(username);
        if (user == null) {
            return false;
        }
        List<ZOOAnimalSpecies> obtainedSpecies = em.createNamedQuery("zivotinjskevrste.findAll",
                ZOOAnimalSpecies.class).getResultList();

        List<Integer> caresIds = animalCaresIds;
        List<ZOOAnimalSpecies> newCares = new ArrayList<ZOOAnimalSpecies>();
        for (ZOOAnimalSpecies spec : obtainedSpecies) {
            if (caresIds.contains(spec.getIdVrste())) {
                newCares.add(spec);
            }
        }

        user.setZivotinjskevrstes(newCares);
        em.merge(user);
        return true;
    }

    @Override
    public boolean updateSpecies(ZOOAnimalSpecies currAnimalSpecies) {
        EntityManager em = JPAEMProvider.getEntityManager();
        if (currAnimalSpecies == null) {
            return false;
        }
        ZOOAnimalSpecies databaseSpecies = DAOProvider.getVisitorDAO().getAnimalSpeciesId(
                currAnimalSpecies.getIdVrste());
        if (databaseSpecies == null) {
            return false;
        }
        databaseSpecies.setFotografija(currAnimalSpecies.getFotografija());
        databaseSpecies.setOpisVrste(currAnimalSpecies.getOpisVrste());
        em.merge(databaseSpecies);
        return true;
    }

    @Override
    public boolean updateIndividual(ZOOIndividual currIndividual) {
        EntityManager em = JPAEMProvider.getEntityManager();
        if (currIndividual == null) {
            return false;
        }
        ZOOIndividual databaseIndividual = DAOProvider.getVisitorDAO().getAnimalIndividual(
                currIndividual.getIdJedinke());
        if (databaseIndividual.getIdJedinke() == null) {
            return false;
        }
        databaseIndividual.setDatumDol(currIndividual.getDatumDol());
        databaseIndividual.setDob(currIndividual.getDob());
        databaseIndividual.setFotografija(currIndividual.getFotografija());
        databaseIndividual.setMjestoRod(currIndividual.getMjestoRod());
        databaseIndividual.setSpol(currIndividual.getSpol());

        em.merge(databaseIndividual);
        return true;
    }
}
