/**
 * Package that contains classes that are used to produce http response as part of restful web service.
 */
/**
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
package hr.fer.zoovrt.rest;

