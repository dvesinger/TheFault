package hr.fer.zoovrt.dao;

import hr.fer.zoovrt.dao.jpa.JPAAdminDAO;
import hr.fer.zoovrt.dao.jpa.JPADAOImpl;
import hr.fer.zoovrt.dao.jpa.JPAGuardianDAO;
import hr.fer.zoovrt.dao.jpa.JPAVisitorDAO;

/**
 * DAOProvider class implementation.
 *
 * @author Domagoj Pluscec
 * @version v1.2, 26.12.2016.
 */
public class DAOProvider {

    /**
     * Reference to dao implementation object.
     */
    private static DAOAdmin daoAdmin = new JPAAdminDAO();

    /**
     * Reference to dao implementation object.
     */
    private static DAOGuardian daoGuardian = new JPAGuardianDAO();

    /**
     * Reference to dao implementation object.
     */
    private static DAOVisitor daoVisitor = new JPAVisitorDAO();

    /**
     * Reference to dao implementation object.
     */
    private static DAO dao = new JPADAOImpl();

    /**
     * DAO object getter method.
     *
     * @return general dao
     */
    public static DAO getDAO() {
        return dao;
    }

    /**
     * DAOAdmin object getter method.
     *
     * @return admin's dao
     */
    public static DAOAdmin getAdminDAO() {
        return daoAdmin;
    }

    /**
     * DAOGuardian object getter method.
     *
     * @return guardian's dao
     */
    public static DAOGuardian getGuadrianDAO() {
        return daoGuardian;
    }

    /**
     * DAOVisitor object getter method.
     *
     * @return visitor's dao
     */
    public static DAOVisitor getVisitorDAO() {
        return daoVisitor;
    }

    /**
     * Private utility class constructor.
     */
    private DAOProvider() {

    }
}
