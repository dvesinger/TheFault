package hr.fer.zoovrt.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class that models guardian.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
@Table(name = "zoovrt.cuvar")
@Entity(name = "cuvar")
@NamedQuery(name = "Guardian.findAll", query = "SELECT c FROM cuvar c")
@XmlRootElement(name = "guardian")
public class Guardian extends User implements Serializable {

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = -218735886726720387L;

    /**
     * Guardian user name.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String korisnickoIme;

    /**
     * List of animal species cares. (used for guardian)
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "brigaovrsti", joinColumns = { @JoinColumn(name = "korisnickoIme") }, inverseJoinColumns = { @JoinColumn(name = "idVrste") })
    private List<ZOOAnimalSpecies> zivotinjskevrstes;

    /**
     * Java bean empty constructor.
     */
    public Guardian() {
    }

    /**
     * Constructor that initializes user with general user attributes.
     *
     * @param user
     *            user instance
     */
    public Guardian(User user) {
        super(user);
    }

    /**
     * Method obtains list of animal species cares.
     *
     * @return list of animal species cares
     */
    public List<ZOOAnimalSpecies> getZivotinjskevrstes() {
        return zivotinjskevrstes;
    }

    /**
     * Method sets user's adopted animals.
     *
     * @param zivotinjskevrstes
     *            list of adopted animals
     */
    public void setZivotinjskevrstes(List<ZOOAnimalSpecies> zivotinjskevrstes) {
        this.zivotinjskevrstes = zivotinjskevrstes;
    }

    /**
     * Factory method that constructs Guardian user from ZOOUser.
     *
     * @param user
     *            ZOOUser instance
     * @return new Guardian instance
     */
    public static Guardian fromZOOUser(ZOOUser user) {
        Guardian guardian = new Guardian(user);
        guardian.setKorisnickoIme(user.getKorisnickoIme());
        guardian.setZivotinjskevrstes(user.getZivotinjskevrstes());
        return guardian;
    }

    @Override
    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    @Override
    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

}
