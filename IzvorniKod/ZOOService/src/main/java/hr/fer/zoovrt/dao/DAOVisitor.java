package hr.fer.zoovrt.dao;

import hr.fer.zoovrt.model.ZOOAnimalClass;
import hr.fer.zoovrt.model.ZOOAnimalSpecies;
import hr.fer.zoovrt.model.ZOOIndividual;
import hr.fer.zoovrt.rest.structures.AdoptPair;
import hr.fer.zoovrt.rest.structures.VisitPair;

import java.util.List;

/**
 * Data access object interface for visitor.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 25.12.2016.
 */
public interface DAOVisitor extends DAO {

    /**
     * Method obtains all animal classes.
     *
     * @return List of animal classes.
     * @throws DAOException
     *             if there was an error while accessing data
     */
    @Override
    List<ZOOAnimalClass> getAnimalClasses() throws DAOException;

    /**
     * Method obtains animal individual with given id.
     *
     * @param individualId
     *            individual id
     * @return animal individual
     * @throws DAOException
     *             if there was an error while accessing data
     */
    ZOOIndividual getAnimalIndividual(Integer individualId) throws DAOException;

    /**
     * Method obtains animal species with given id.
     *
     * @param speciesId
     *            animal species id
     * @return animal species
     * @throws DAOException
     *             if there was an error while accessing data
     */
    ZOOAnimalSpecies getAnimalSpeciesId(Integer speciesId) throws DAOException;

    /**
     * Method persists animal adoption.
     *
     * @param adoptInfo
     *            adoption informations.
     * @return true if adoption was successful, false otherwise
     * @throws DAOException
     *             if there was an error while accessing data
     */
    boolean adoptAnimal(AdoptPair adoptInfo) throws DAOException;

    /**
     * Method persists animal species visit.
     *
     * @param visitInfo
     *            visit informations
     * @return true if the visit was successful, false otherwise
     * @throws DAOException
     *             if there was an error while accessing data
     */
    boolean visitSpecies(VisitPair visitInfo) throws DAOException;

    /**
     * Method obtains animal class with given id.
     *
     * @param classId
     *            animal class id
     * @return animal class
     * @throws DAOException
     *             if there was an error while accessing data
     */
    ZOOAnimalClass getAnimalClass(Integer classId) throws DAOException;

    /**
     * Method obtains visit recommentation for given user.
     *
     * @param username
     *            user's username
     * @param speciesId
     *            last visited species id
     * @return next speces to visit
     * @throws DAOException
     *             if there was an error while accessing data
     */
    ZOOAnimalSpecies getSpeciesRecommendation(String username, Integer speciesId)
            throws DAOException;

}
