package hr.fer.zoovrt.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * The persistent class for the vrstazanimljivosti database table.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
@Entity(name = "vrstazanimljivosti")
@Table(name = "vrstazanimljivosti")
@NamedQuery(name = "Vrstazanimljivosti.findAll", query = "SELECT v FROM vrstazanimljivosti v")
@XmlRootElement(name = "interesting_fact_type")
public class InterestingFactType implements Serializable {

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = -641646260691710762L;

    /**
     * Id interesting fact type.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idVrsteZanimljivosti;

    /**
     * Type of interesting fact.
     */
    private String vrstaZanimljivosti;

    /**
     * List of interesting facts with current type.
     */
    // bi-directional many-to-one association to Zanimljivosti
    @OneToMany(mappedBy = "vrstazanimljivosti")
    private List<ZOOInterestingFact> zanimljivostis;

    /**
     * Java bean empty constructor.
     */
    public InterestingFactType() {
    }

    /**
     * Method adds interesting fact to current interesting fact type.
     *
     * @param zanimljivosti
     *            interesting fact
     * @return reference to given interesting fact
     */
    public ZOOInterestingFact addZanimljivosti(ZOOInterestingFact zanimljivosti) {
        getZanimljivostis().add(zanimljivosti);
        zanimljivosti.setVrstazanimljivosti(this);

        return zanimljivosti;
    }

    /**
     * Getter method for interesting fact type id.
     *
     * @return interesting fact id
     */
    public int getIdVrsteZanimljivosti() {
        return idVrsteZanimljivosti;
    }

    /**
     * Getter method for interesting fact type.
     *
     * @return interesting fact type
     */
    public String getVrstaZanimljivosti() {
        return vrstaZanimljivosti;
    }

    /**
     * Method obtains interesting facts with current interesting fact type.
     *
     * @return list of interesting facts
     */
    public List<ZOOInterestingFact> getZanimljivostis() {
        return zanimljivostis;
    }

    /**
     * Method removes interesting fact from current interesting fact type.
     *
     * @param zanimljivosti
     *            interesting fact
     * @return reference to given interesting fact
     */
    public ZOOInterestingFact removeZanimljivosti(ZOOInterestingFact zanimljivosti) {
        getZanimljivostis().remove(zanimljivosti);
        zanimljivosti.setVrstazanimljivosti(null);

        return zanimljivosti;
    }

    /**
     * Method sets interesting fact type id.
     *
     * @param idVrsteZanimljivosti
     *            interesting fact type id
     */
    @XmlElement
    public void setIdVrsteZanimljivosti(int idVrsteZanimljivosti) {
        this.idVrsteZanimljivosti = idVrsteZanimljivosti;
    }

    /**
     * Method sets interesting fact type.
     *
     * @param vrstaZanimljivosti
     *            interesting fact type
     */
    @XmlElement
    public void setVrstaZanimljivosti(String vrstaZanimljivosti) {
        this.vrstaZanimljivosti = vrstaZanimljivosti;
    }

    /**
     * Method sets interesting fact list with current interesting fact type.
     *
     * @param zanimljivostis
     *            interesting fact list
     */
    @XmlTransient
    public void setZanimljivostis(List<ZOOInterestingFact> zanimljivostis) {
        this.zanimljivostis = zanimljivostis;
    }

    /**
     * Method gets list of interesting fact id with current interesting fact
     * type.
     *
     * @return list of interesting fact id's
     */
    @XmlElementWrapper(name = "list_interesting_facts")
    @XmlElement
    public List<Integer> getZanimljivostiIds() {
        List<Integer> ids = new ArrayList<Integer>();
        for (ZOOInterestingFact interestingFact : zanimljivostis) {
            ids.add(interestingFact.getIdZanimljivosti());
        }
        return ids;
    }
}
