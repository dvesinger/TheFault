package hr.fer.zoovrt.dao.jpa;

import hr.fer.zoovrt.dao.DAOException;
import hr.fer.zoovrt.dao.DAOGuardian;
import hr.fer.zoovrt.model.ZOOIndividual;
import hr.fer.zoovrt.rest.structures.InterestingFactTriple;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;

/**
 * JPA DAO implementation class for guardian.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.6.2015.
 */
public class JPAGuardianDAO extends JPADAOImpl implements DAOGuardian {
    @Override
    public void updateIndividual(ZOOIndividual entry) throws DAOException {

        EntityManager em = JPAEMProvider.getEntityManager();
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }

        em.merge(entry);
        em.getTransaction().commit();

    }

    @Override
    public boolean addInterestingFact(InterestingFactTriple interestingFactInfo) {
        EntityManager em = JPAEMProvider.getEntityManager();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = sdf.format(new Date(System.currentTimeMillis()));
        em.createNativeQuery(
                "INSERT INTO `zoovrt`.`zanimljivosti` (`idJedinke`, `zanimljivost`,"
                        + " `idVrstaZanimljivosti`, `datum`) VALUES ('"
                        + interestingFactInfo.getIndividualId() + "', '"
                        + interestingFactInfo.getInterestingFactContent() + "', '"
                        + interestingFactInfo.getInterestingFactTypeId() + "', '" + strDate + "')")
                .executeUpdate();
        System.err.println("CREATE INTerestingFact");
        return true;
    }
}
