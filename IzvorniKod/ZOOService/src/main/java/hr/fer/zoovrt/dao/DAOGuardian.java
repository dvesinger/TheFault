package hr.fer.zoovrt.dao;

import hr.fer.zoovrt.model.ZOOIndividual;
import hr.fer.zoovrt.rest.structures.InterestingFactTriple;

/**
 * Data access object interface for guardian.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 25.12.2016.
 */
public interface DAOGuardian extends DAO {
    /**
     * Method updates individual animal with given ZOOIndividual.
     *
     * @param individual
     *            ZOOIndividual
     * @throws DAOException
     *             if there was an error while accessing data
     */
    void updateIndividual(ZOOIndividual individual) throws DAOException;

    /**
     * Method persists given interesting fact.
     *
     * @param interestingFactInfo
     *            interesting fact informations
     * @return true if the interesting fact was persisted successfuly, false
     *         otherwise
     * @throws DAOException
     *             if there was an error while accessing data
     */
    boolean addInterestingFact(InterestingFactTriple interestingFactInfo) throws DAOException;

}
