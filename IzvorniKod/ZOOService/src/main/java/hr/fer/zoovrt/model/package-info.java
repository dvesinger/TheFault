/**
 * Package contains domain model classes.
 */
/**
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
package hr.fer.zoovrt.model;

