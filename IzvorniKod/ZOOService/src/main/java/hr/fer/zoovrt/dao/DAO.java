package hr.fer.zoovrt.dao;

import hr.fer.zoovrt.model.Permissions;
import hr.fer.zoovrt.model.User;
import hr.fer.zoovrt.model.ZOOAnimalClass;
import hr.fer.zoovrt.model.ZOOAnimalSpecies;
import hr.fer.zoovrt.model.ZOOUser;

import java.util.List;

/**
 * Data access object interface for general user.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.6.2015.
 */
public interface DAO {

    /**
     * Method checks if the user with user name and password is available.
     *
     * @param user
     *            user object containing at least user name and password
     * @return true if the user is contained, false otherwise
     * @throws DAOException
     *             if there was an error while accessing data
     */
    boolean checkUser(User user) throws DAOException;

    /**
     * Method persists new user.
     *
     * @param user
     *            new user
     * @throws DAOException
     *             if there was an error while accessing data
     * @return true if creation was sucessful, false otherwise
     */
    boolean createUser(ZOOUser user) throws DAOException;

    /**
     * Method checks if the user with given username exists.
     *
     * @param username
     *            user username
     * @return true if the username exists in user table, false otherwise
     * @throws DAOException
     *             if there was an error while accessing data
     */
    boolean checkUsernameExists(String username) throws DAOException;

    /**
     * Method obtains user with given user name.
     *
     * @param username
     *            user identification
     * @return User object
     * @throws DAOException
     *             if there was an error while accessing data
     */
    User getUser(String username) throws DAOException;

    /**
     * Method obtains permission for user with given username.
     *
     * @param username
     *            user's username
     * @return permission of the given user
     * @throws DAOException
     *             if there was an error while accessing data
     */
    Permissions getUserPermission(String username) throws DAOException;

    /**
     * Method obtains all animal classes.
     *
     * @return list of all animal classes
     * @throws DAOException
     *             if there was an error while accessing data
     */
    List<ZOOAnimalClass> getAnimalClasses() throws DAOException;

    /**
     * Method obtains all animal species.
     *
     * @return list of all animal species
     * @throws DAOException
     *             if there was an error while accessing data
     */
    List<ZOOAnimalSpecies> getAnimalSpecies() throws DAOException;

    /**
     * Method obtains permission with given permission id.
     *
     * @param idOvlasti
     *            permission id
     * @return permission object
     * @throws DAOException
     *             if there was an error while accessing data
     */
    Permissions getPermission(int idOvlasti) throws DAOException;
}
