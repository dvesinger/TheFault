/**
 * Package that defines DAO interface and provides a DAOProvier for ZOOService aplication.
 */
/**
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
package hr.fer.zoovrt.dao;

