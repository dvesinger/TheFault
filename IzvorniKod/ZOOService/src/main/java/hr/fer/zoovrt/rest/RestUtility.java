package hr.fer.zoovrt.rest;

/**
 * Utility class for implementing restful web service.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 27.12.2016.
 */
public class RestUtility {

    /**
     * HTTP ok status code.
     */
    public static final int STATUS_OK = 200;
    /**
     * HTTP forbidden status code.
     */
    public static final int STATUS_FORBIDDEN = 403;
    /**
     * HTTP not found status code.
     */
    public static final int STATUS_NOT_FOUND = 404;

    /**
     * HTTP bad request status code.
     */
    public static final int BAD_REQUEST = 400;

    /**
     * Utility class private constructor.
     */
    private RestUtility() {
    }

}
