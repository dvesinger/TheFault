package hr.fer.zoovrt.rest.structures;

/**
 * REST structure for encapsulating adopt informations.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 8.1.2017.
 */
public class AdoptPair {

    /**
     * User username.
     */
    private String username;
    /**
     * Animal individual id.
     */
    private Integer idJedinke;

    /**
     * Java bean constructor.
     */
    public AdoptPair() {
    }

    /**
     * Method obtains user's username.
     *
     * @return user's username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Method sets user's username.
     *
     * @param username
     *            user's username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Method obtains animal individual id.
     *
     * @return individual id
     */
    public Integer getIdJedinke() {
        return idJedinke;
    }

    /**
     * Method sets individual's id.
     *
     * @param idJedinke
     *            individual's id
     */
    public void setIdJedinke(Integer idJedinke) {
        this.idJedinke = idJedinke;
    }

}
