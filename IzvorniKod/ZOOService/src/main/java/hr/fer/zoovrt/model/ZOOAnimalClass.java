package hr.fer.zoovrt.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The persistent class for the razredzivotinja database table.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 27.12.2016.
 */
@Entity(name = "razredzivotinja")
@Table(name = "razredzivotinja")
@NamedQuery(name = "Razredzivotinja.findAll", query = "SELECT r FROM razredzivotinja r")
@XmlRootElement(name = "animal_class")
public class ZOOAnimalClass implements Serializable {

    /**
     * Number that JVM uses for serialization.
     */
    @Transient
    private static final long serialVersionUID = -6450577358072799311L;

    /**
     * Animal class id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idRazred;

    /**
     * Animal class name.
     */
    private String nazivRazreda;

    /**
     * List of animal species with current animal class.
     */
    @OneToMany(mappedBy = "razredzivotinja")
    @org.codehaus.jackson.annotate.JsonManagedReference
    private List<ZOOAnimalSpecies> zivotinjskevrstes;

    /**
     * Java bean empty constructor.
     */
    public ZOOAnimalClass() {
    }

    /**
     * Method adds animal species to the animal class.
     *
     * @param zivotinjskevrste
     *            animal species to be added
     * @return reference to given animal species
     */
    public ZOOAnimalSpecies addZivotinjskevrste(ZOOAnimalSpecies zivotinjskevrste) {
        if (zivotinjskevrstes == null) {
            zivotinjskevrstes = new ArrayList<ZOOAnimalSpecies>();
        }
        getZivotinjskevrstes().add(zivotinjskevrste);
        zivotinjskevrste.setRazredzivotinja(this);

        return zivotinjskevrste;
    }

    /**
     * Method returns animal class id.
     *
     * @return animal class id
     */
    public int getIdRazred() {
        return idRazred;
    }

    /**
     * Method obtains animal class name.
     *
     * @return animal class name
     */
    public String getNazivRazreda() {
        return nazivRazreda;
    }

    /**
     * Getter method for obtaining animal species in current animal class.
     *
     * @return list of animal species from current class
     */
    public List<ZOOAnimalSpecies> getZivotinjskevrstes() {
        return zivotinjskevrstes;
    }

    /**
     * Method removes animal species from current animal class.
     *
     * @param zivotinjskevrste
     *            animal class to be removed
     * @return reference to given animal class
     */
    public ZOOAnimalSpecies removeZivotinjskevrste(ZOOAnimalSpecies zivotinjskevrste) {
        getZivotinjskevrstes().remove(zivotinjskevrste);
        zivotinjskevrste.setRazredzivotinja(null);

        return zivotinjskevrste;
    }

    /**
     * Method sets animal class id.
     *
     * @param idRazred
     *            animal class id
     */
    public void setIdRazred(int idRazred) {
        this.idRazred = idRazred;
    }

    /**
     * Method sets animal class name.
     *
     * @param nazivRazreda
     *            animal class name
     */
    public void setNazivRazreda(String nazivRazreda) {
        this.nazivRazreda = nazivRazreda;
    }

    /**
     * Method sets list of animal species in current animal class.
     *
     * @param zivotinjskevrstes
     *            list of animal species
     */
    public void setZivotinjskevrstes(List<ZOOAnimalSpecies> zivotinjskevrstes) {
        this.zivotinjskevrstes = zivotinjskevrstes;
    }

}
