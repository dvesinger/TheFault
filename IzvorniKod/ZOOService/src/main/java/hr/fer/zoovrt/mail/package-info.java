/**
 * Package contains classes for sending an email.
 * @author Domagoj Pluscec
 * @version v1.0, 8.1.2017.
 */
package hr.fer.zoovrt.mail;

