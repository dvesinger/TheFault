package hr.fer.zoovrt.model;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * The persistent class for the posjecenost database table.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
@Entity(name = "posjecenost")
@DiscriminatorValue("1")
@Table(name = "posjecenost")
@NamedQuery(name = "Posjecenost.findAll", query = "SELECT p FROM posjecenost p")
@XmlRootElement(name = "visit")
public class Visit implements Serializable {
    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = 2540773189460742945L;

    /**
     * Visit's id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idPosjet;

    /**
     * Species id that has been visited.
     */
    @ManyToOne
    @JoinColumn(name = "idVrste")
    private ZOOAnimalSpecies zivotinjskevrste;

    /**
     * User that visited the animal species.
     */
    @ManyToOne
    @JoinColumn(name = "korisnickoIme")
    private ZOOUser korisnik;

    /**
     * Java bean empty constructor.
     */
    public Visit() {
    }

    /**
     * Getter method for visit id.
     *
     * @return visit id
     */
    public Integer getIdPosjet() {
        return idPosjet;
    }

    /**
     * Method gets visit's user.
     *
     * @return user
     */
    public ZOOUser getKorisnik() {
        return korisnik;
    }

    /**
     * Method obtains visit's user username.
     *
     * @return username
     */
    @XmlElement(name = "korisnicko_ime")
    @JsonProperty("korisnicko_ime")
    public String getKorisnikUserName() {
        if (korisnik == null) {
            return null;
        }
        return korisnik.getKorisnickoIme();
    }

    /**
     * Method sets visit's user username. Mock method for XML API.
     *
     * @param ignorable
     *            user's username
     */
    public void setKorisnikUserName(String ignorable) {
    }

    /**
     * Method obtains visited animal species.
     *
     * @return visited animal species
     */
    public ZOOAnimalSpecies getZivotinjskevrste() {
        return zivotinjskevrste;
    }

    /**
     * Method returns visited animal species id.
     *
     * @return visited animal species id
     */
    @XmlElement
    public Integer getZivotinjskevrsteId() {
        return zivotinjskevrste.getIdVrste();
    }

    /**
     * Method sets id for visit's animal species. Used as a mock for XML api.
     *
     * @param ignorable
     *            visit's animal species id
     */
    public void setZivotinjskevrsteId(Integer ignorable) {
    }

    /**
     * Method sets visit id.
     *
     * @param idPosjet
     *            visit id.
     */
    public void setIdPosjet(int idPosjet) {
        this.idPosjet = idPosjet;
    }

    /**
     * Method sets visit's user.
     *
     * @param korisnik
     *            user that made visit
     */
    @XmlTransient
    public void setKorisnik(ZOOUser korisnik) {
        this.korisnik = korisnik;
    }

    /**
     * Method sets animal species that has been visited.
     *
     * @param zivotinjskevrste
     *            visited animal species
     */
    @XmlTransient
    public void setZivotinjskevrste(ZOOAnimalSpecies zivotinjskevrste) {
        this.zivotinjskevrste = zivotinjskevrste;
    }

}
