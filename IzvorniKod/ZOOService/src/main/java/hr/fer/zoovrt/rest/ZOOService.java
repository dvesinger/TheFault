package hr.fer.zoovrt.rest;

import hr.fer.zoovrt.dao.DAOProvider;
import hr.fer.zoovrt.model.Admin;
import hr.fer.zoovrt.model.Guardian;
import hr.fer.zoovrt.model.Permissions;
import hr.fer.zoovrt.model.User;
import hr.fer.zoovrt.model.Visitor;
import hr.fer.zoovrt.model.ZOOAnimalClass;
import hr.fer.zoovrt.model.ZOOAnimalSpecies;
import hr.fer.zoovrt.model.ZOOIndividual;
import hr.fer.zoovrt.model.ZOOUser;
import hr.fer.zoovrt.rest.structures.AdoptPair;
import hr.fer.zoovrt.rest.structures.InterestingFactTriple;
import hr.fer.zoovrt.rest.structures.VisitPair;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Class used for implementation of general user restful web API.
 *
 * @author Domagoj Pluscec
 * @version 28.12.2016.
 */
@Path("/ZOOService")
public class ZOOService {

    /**
     * Method adds given interesting fact to database.
     *
     * @param interestingFactInfo
     *            interesting fact informations
     * @return response status
     */
    @POST
    @Path("animal_individual/add_interesting_fact")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addInterestingFact(InterestingFactTriple interestingFactInfo) {
        System.err.println("Add interesting fact");
        if (interestingFactInfo == null || interestingFactInfo.getIndividualId() == null
                || interestingFactInfo.getInterestingFactTypeId() == null) {
            return Response.status(RestUtility.BAD_REQUEST).build();
        }
        boolean status = DAOProvider.getGuadrianDAO().addInterestingFact(interestingFactInfo);

        if (status) {
            return Response.status(RestUtility.STATUS_OK).build();
        }
        return Response.status(RestUtility.BAD_REQUEST).build();
    }

    /**
     * Method adopts animal individual according to given informations.
     *
     * @param adoptInfo
     *            adtopt informations
     * @return http status response
     */
    @POST
    @Path("/individuals/adopt")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response adoptAnimal(AdoptPair adoptInfo) {
        System.err.println("ADOPT");
        if (adoptInfo.getUsername() == null || adoptInfo.getIdJedinke() == null) {
            return Response.status(RestUtility.BAD_REQUEST).build();
        }
        boolean status = DAOProvider.getVisitorDAO().adoptAnimal(adoptInfo);
        if (status) {
            return Response.status(RestUtility.STATUS_OK).build();
        }
        return Response.status(RestUtility.BAD_REQUEST).build();
    }

    /**
     * Method checks if the user's username and password are valid.
     *
     * @param user
     *            user object with username and password
     * @return true if the user is valid, false otherwise
     */
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response checkUser(ZOOUser user) {
        System.out.println("Check user" + user.getKorisnickoIme());
        if (user.getKorisnickoIme() == null) {

            return Response.status(RestUtility.BAD_REQUEST).entity(Boolean.FALSE).build();
        }
        if (DAOProvider.getDAO().checkUser(user)) {
            return Response.status(RestUtility.STATUS_OK).entity(Boolean.TRUE).build();
        }

        return Response.status(RestUtility.BAD_REQUEST).entity(Boolean.FALSE).build();
    }

    /**
     * Method creates animal individual with given species id and individual
     * name.
     *
     * @param animalSpeciesId
     *            animal species id.
     * @param individualName
     *            animal individual id.
     * @return true if the creation was successful, false otherwise
     */
    @POST
    @Path("/animal_individual/create/{animal_species_id}/{animal_individual_name}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createAnimalIndividual(@PathParam("animal_species_id") Integer animalSpeciesId,
            @PathParam("animal_individual_name") String individualName) {
        System.err.println("REST - CREATE ANIMAL INDIVIDUAL");
        if (animalSpeciesId == null || individualName == null || individualName.isEmpty()) {
            return Response.status(RestUtility.BAD_REQUEST).entity(Boolean.FALSE).build();
        }
        boolean status = DAOProvider.getAdminDAO().createAnimalIndividual(individualName,
                animalSpeciesId);
        if (status) {
            return Response.status(RestUtility.STATUS_OK).entity(Boolean.TRUE).build();
        }
        return Response.status(RestUtility.BAD_REQUEST).entity(Boolean.FALSE).build();
    }

    /**
     * Method creates animal species with given animal class and animal species
     * name.
     *
     * @param animalClassId
     *            animal class id
     * @param speciesName
     *            animal species name
     * @return true if the creation was successful, false otherwise
     */
    @POST
    @Path("/animal_species/create/{animal_class_id}/{animal_species_name}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createAnimalSpecies(@PathParam("animal_class_id") Integer animalClassId,
            @PathParam("animal_species_name") String speciesName) {
        System.err.println("REST - CREATE ANIMAL SPECIES");
        if (animalClassId == null || speciesName == null || speciesName.isEmpty()) {
            return Response.status(RestUtility.BAD_REQUEST).entity(Boolean.FALSE).build();
        }
        boolean status = DAOProvider.getAdminDAO().createAnimalSpecies(speciesName, animalClassId);
        if (status) {
            return Response.status(RestUtility.STATUS_OK).entity(Boolean.TRUE).build();
        }
        return Response.status(RestUtility.BAD_REQUEST).entity(Boolean.FALSE).build();
    }

    /**
     * Method deletes given animal individual.
     *
     * @param individual
     *            animal individual
     * @return http status response
     */
    @POST
    @Path("/animal_individual/delete")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteAnimalIndividual(ZOOIndividual individual) {
        if (individual == null) {
            return Response.status(RestUtility.BAD_REQUEST).build();
        }
        boolean status = DAOProvider.getAdminDAO().deleteAnimalIndividual(individual);
        if (status) {
            return Response.status(RestUtility.STATUS_OK).build();
        }
        return Response.status(RestUtility.BAD_REQUEST).build();
    }

    /**
     * Method deletes given animal species.
     *
     * @param species
     *            species to delete
     * @return http status response
     */
    @POST
    @Path("/animal_species/delete")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteAnimalSpecies(ZOOAnimalSpecies species) {
        if (species == null) {
            return Response.status(RestUtility.BAD_REQUEST).build();
        }
        boolean status = DAOProvider.getAdminDAO().deleteAnimalSpecies(species);
        if (status) {
            return Response.status(RestUtility.STATUS_OK).build();
        }
        return Response.status(RestUtility.BAD_REQUEST).build();
    }

    /**
     * Method deletes user with given username.
     *
     * @param username
     *            user to delete
     * @return http status response
     */
    @DELETE
    @Path("users/{user}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("user") String username) {
        if (username == null) {
            return Response.status(RestUtility.BAD_REQUEST).build();
        }
        boolean status = DAOProvider.getAdminDAO().deleteUser(username);
        if (status) {
            return Response.status(RestUtility.STATUS_OK).build();
        }
        return Response.status(RestUtility.BAD_REQUEST).build();
    }

    /**
     * Method produces list of all visitors.
     *
     * @return Response object with JSON content
     */
    @GET
    @Path("/admins")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAdmins() {
        List<Admin> visitors = DAOProvider.getAdminDAO().getAllAdmins();
        return Response.status(RestUtility.STATUS_OK).entity(visitors).build();
    }

    /**
     * Method obtains list of all users.
     *
     * @return list of all users
     */
    @Deprecated
    @GET
    @Path("/users")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllUsers() {
        List<User> users = DAOProvider.getAdminDAO().getAllUsers();
        return Response.status(RestUtility.STATUS_OK).entity(users).build();
    }

    /**
     * Method obtains all animal classes.
     *
     * @return list of animal classes
     */
    @GET
    @Path("/animal_classes")
    public Response getAnimalClasses() {
        List<ZOOAnimalClass> visitors = DAOProvider.getDAO().getAnimalClasses();
        return Response.status(RestUtility.STATUS_OK).entity(visitors).build();
    }

    /**
     * Method obtains animal class with given id.
     *
     * @param classId
     *            animal class id
     * @return animal class
     */
    @GET
    @Path("animal_classes/{class_id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAnimalClassId(@PathParam("class_id") Integer classId) {
        if (classId == null) {
            return Response.status(RestUtility.BAD_REQUEST).build();
        }
        ZOOAnimalClass individual = DAOProvider.getVisitorDAO().getAnimalClass(classId);
        if (individual == null) {
            return Response.status(RestUtility.STATUS_NOT_FOUND).build();
        }
        return Response.status(RestUtility.STATUS_OK).entity(individual).build();
    }

    /**
     * Method obtains animal individual with given id.
     *
     * @param individualId
     *            animal individual id
     * @return animal individual
     */
    @GET
    @Path("/individuals/{individualid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAnimalIndividual(@PathParam("individualid") Integer individualId) {
        ZOOIndividual individual = DAOProvider.getVisitorDAO().getAnimalIndividual(individualId);
        if (individual == null) {
            return Response.status(RestUtility.STATUS_NOT_FOUND).build();
        }
        return Response.status(RestUtility.STATUS_OK).entity(individual).build();

    }

    /**
     * Method obtains all animal species.
     *
     * @return list of all animal species
     */
    @GET
    @Path("/animal_species")
    public Response getAnimalSpecies() {
        List<ZOOAnimalSpecies> species = DAOProvider.getDAO().getAnimalSpecies();
        return Response.status(RestUtility.STATUS_OK).entity(species).build();
    }

    /**
     * Method obtains animal species with given id.
     *
     * @param speciesId
     *            animal species id
     * @return animal species
     */
    @GET
    @Path("/animal_species/{species_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAnimalSpeciesId(@PathParam("species_id") Integer speciesId) {
        if (speciesId == null) {
            return Response.status(RestUtility.BAD_REQUEST).build();
        }
        ZOOAnimalSpecies species = DAOProvider.getVisitorDAO().getAnimalSpeciesId(speciesId);
        if (species == null) {
            return Response.status(RestUtility.STATUS_NOT_FOUND).build();
        }
        return Response.status(RestUtility.STATUS_OK).entity(species).build();

    }

    /**
     * Method produces list of all visitors.
     *
     * @return Response object with JSON content
     */
    @GET
    @Path("/guardians")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGuardians() {
        List<Guardian> visitors = DAOProvider.getAdminDAO().getAllGuardians();
        return Response.status(RestUtility.STATUS_OK).entity(visitors).build();
    }

    /**
     * Method obtains species visit recommendation for given user.
     *
     * @param username
     *            user's username
     * @param speciesId
     *            last visited species id
     * @return species to visit next
     */
    @GET
    @Path("animal_species/recommend/{user_id}/{species_id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getSpeciesRecommendation(@PathParam("user_id") String username,
            @PathParam("species_id") Integer speciesId) {
        ZOOAnimalSpecies species = DAOProvider.getVisitorDAO().getSpeciesRecommendation(username,
                speciesId);
        if (species == null) {
            return Response.status(RestUtility.STATUS_NOT_FOUND).build();
        }
        return Response.status(RestUtility.STATUS_OK).entity(species).build();
    }

    /**
     * Method produces user with given username or
     * {@link RestUtility.STATUS_NOT_FOUND} if user wasn't found.
     *
     * @param username
     *            user's username
     * @return Response object with status and JSON content
     */
    @GET
    @Path("/users/{userid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@PathParam("userid") String username) {
        ZOOUser user = (ZOOUser) DAOProvider.getDAO().getUser(username);
        if (user == null) {
            return Response.status(RestUtility.STATUS_NOT_FOUND).build();
        }
        switch (user.getOvlasti().getIdOvlasti()) {
        case 1:
            return Response.status(RestUtility.STATUS_OK).entity(Admin.fromZOOUser(user)).build();

        case 2:
            return Response.status(RestUtility.STATUS_OK).entity(Guardian.fromZOOUser(user))
                    .build();

        case 3:
            return Response.status(RestUtility.STATUS_OK).entity(Visitor.fromZOOUser(user)).build();

        default:
            return Response.status(RestUtility.STATUS_NOT_FOUND).build();
        }

    }

    /**
     * Method obtains users permission.
     *
     * @param username
     *            user's username
     * @return user's permission
     */
    @GET
    @Path("/users/{userid}/permission")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserPermission(@PathParam("userid") String username) {
        Permissions perm = DAOProvider.getDAO().getUserPermission(username);
        if (perm == null) {
            return Response.status(RestUtility.STATUS_NOT_FOUND).build();
        }

        return Response.status(RestUtility.STATUS_OK).entity(perm).build();

    }

    /**
     * Method produces list of all visitors.
     *
     * @return Response object with JSON content
     */
    @GET
    @Path("/visitors")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVisitors() {
        List<Visitor> visitors = DAOProvider.getAdminDAO().getAllVisitors();
        return Response.status(RestUtility.STATUS_OK).entity(visitors).build();
    }

    /**
     * Method obtains visit statistics.
     *
     * @return visit statistics
     */
    @GET
    @Path("/visits/statistics")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVisitorStatistics() {
        Map<ZOOAnimalSpecies, Integer> statistics = DAOProvider.getAdminDAO()
                .getVisitorStatistics();
        if (statistics == null) {
            return Response.status(RestUtility.STATUS_NOT_FOUND).entity(Boolean.FALSE).build();
        }
        return Response.status(RestUtility.STATUS_OK).entity(statistics).build();
    }

    /**
     * Method registers given user.
     *
     * @param user
     *            user to register
     * @return http status response
     */
    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerUser(Visitor user) {
        System.out.println("Register user" + user.getKorisnickoIme());
        if (user.getKorisnickoIme() == null) {
            return Response.status(RestUtility.BAD_REQUEST).entity(Boolean.FALSE).build();
        }
        if (DAOProvider.getDAO().checkUsernameExists(user.getKorisnickoIme())) {
            System.out.println("Register user: username doesn't exist");
            return Response.status(RestUtility.BAD_REQUEST).entity(Boolean.FALSE).build();
        }
        if (DAOProvider.getDAO().createUser(ZOOUser.fromVisitor(user))) {
            System.out.println("Register user: failed to create user");
            return Response.status(RestUtility.STATUS_OK).entity(Boolean.TRUE).build();
        }
        return Response.status(RestUtility.BAD_REQUEST).entity(Boolean.FALSE).build();
    }

    /**
     * Method updates administrator.
     *
     * @param admin
     *            admin user
     * @return http status response
     */
    @POST
    @Path("/admin/update")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateAdmin(Admin admin) {
        DAOProvider.getAdminDAO().updateAdmin(admin);
        return Response.status(RestUtility.STATUS_OK).build();

    }

    /**
     * Method updates guardian user.
     *
     * @param guardian
     *            guardian user
     * @return http status response
     */
    @POST
    @Path("/guardian/update")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateGuardian(Guardian guardian) {
        DAOProvider.getAdminDAO().updateGuardian(guardian);
        return Response.status(RestUtility.STATUS_OK).build();

    }

    /**
     * Method updates guardian cares.
     *
     * @param animalCaresIds
     *            list of guardian animal cares
     * @param username
     *            guardian username
     * @return http status response
     */
    @POST
    @Path("/guardian/update/{guardianUsername}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateGuardianCares(List<Integer> animalCaresIds,
            @PathParam("guardianUsername") String username) {

        if (animalCaresIds == null || username == null) {
            return Response.status(RestUtility.BAD_REQUEST).entity(Boolean.FALSE).build();
        }
        boolean status = DAOProvider.getAdminDAO().updateGuardianCares(username, animalCaresIds);
        if (status) {
            return Response.status(RestUtility.STATUS_OK).entity(Boolean.TRUE).build();
        }
        return Response.status(RestUtility.BAD_REQUEST).entity(Boolean.FALSE).build();

    }

    /**
     * Method updates animal individual with given informations.
     *
     * @param currIndividual
     *            new animal individual informations
     * @return http status response
     */
    @POST
    @Path("/animal_individual/edit")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateIndividual(ZOOIndividual currIndividual) {
        System.err.println("Update individual");
        if (currIndividual == null) {
            return Response.status(RestUtility.BAD_REQUEST).build();
        }
        boolean status = DAOProvider.getAdminDAO().updateIndividual(currIndividual);
        System.err.println("Update individual" + currIndividual.getIme());
        if (status) {
            return Response.status(RestUtility.STATUS_OK).build();
        }
        return Response.status(RestUtility.BAD_REQUEST).build();
    }

    /**
     * Method updates given animal species.
     *
     * @param currAnimalSpecies
     *            new animal species informations.
     * @return http status response
     */
    @POST
    @Path("animal_species/edit")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateSpecies(ZOOAnimalSpecies currAnimalSpecies) {
        if (currAnimalSpecies == null) {
            return Response.status(RestUtility.BAD_REQUEST).build();
        }

        boolean status = DAOProvider.getAdminDAO().updateSpecies(currAnimalSpecies);
        if (status) {
            return Response.status(RestUtility.STATUS_OK).build();
        }
        return Response.status(RestUtility.BAD_REQUEST).build();
    }

    /**
     * Method updates user's permission.
     *
     * @param username
     *            user's username
     * @param permissionId
     *            new user permission
     * @return true if the permission change was successful, false otherwise
     */
    @POST
    @Path("/users/{user}/permission_update/{permission_id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUserPermission(@PathParam("user") String username,
            @PathParam("permission_id") Integer permissionId) {
        System.err.println("UPDATE PERMISSION");
        if (username == null || username.isEmpty() || permissionId == null) {
            return Response.status(RestUtility.BAD_REQUEST).entity(Boolean.FALSE).build();
        }
        boolean status = DAOProvider.getAdminDAO().updateUserPermission(username, permissionId);
        if (status) {
            return Response.status(RestUtility.STATUS_OK).entity(Boolean.TRUE).build();
        }
        return Response.status(RestUtility.BAD_REQUEST).entity(Boolean.FALSE).build();
    }

    /**
     * Method updates visitor with given information.
     *
     * @param visitor
     *            new visitor informations.
     * @return http status response
     */
    @POST
    @Path("/visitor/update")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateVisitor(Visitor visitor) {
        System.err.println("UPDATE VISITOR");
        DAOProvider.getAdminDAO().updateVisitor(visitor);
        return Response.status(RestUtility.STATUS_OK).build();
    }

    /**
     * Method adds visit with given visit informations.
     *
     * @param visitInfo
     *            visit informations
     * @return http status response
     */
    @POST
    @Path("/animal_species/visit")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response visitSpecies(VisitPair visitInfo) {
        System.err.println("VISIT");
        if (visitInfo.getUsername() == null || visitInfo.getSpeciesId() == null) {
            return Response.status(RestUtility.BAD_REQUEST).build();
        }
        boolean status = DAOProvider.getVisitorDAO().visitSpecies(visitInfo);
        if (status) {
            return Response.status(RestUtility.STATUS_OK).build();
        }
        return Response.status(RestUtility.BAD_REQUEST).build();
    }
}
