package hr.fer.zoovrt.example;

import hr.fer.zoovrt.dao.DAOProvider;
import hr.fer.zoovrt.dao.jpa.JPAEMFProvider;
import hr.fer.zoovrt.model.ZOOIndividual;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 * Program demonstrates how to build a xml string from object.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
public class XMLDemo {

    /**
     * Method that starts with the program run.
     *
     * @param args
     *            arguments are ignored
     * @throws JAXBException
     *             if there was an exception while trying to create xml
     *             representation of an object
     */
    public static void main(String[] args) throws JAXBException {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("database.zoovrt");
        JPAEMFProvider.setEmf(emf);

        ZOOIndividual v = DAOProvider.getAdminDAO().getAllVisitors().get(0).getJedinkes().get(0);

        JAXBContext jaxbContext = JAXBContext.newInstance(ZOOIndividual.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(v, System.out);
    }

    /**
     * Utility class private constructor.
     */
    private XMLDemo() {
    }
}
