package hr.fer.zoovrt.dao.jpa;

import hr.fer.zoovrt.dao.DAOException;
import hr.fer.zoovrt.dao.DAOVisitor;
import hr.fer.zoovrt.mail.SimpleMailClient;
import hr.fer.zoovrt.model.Visit;
import hr.fer.zoovrt.model.ZOOAnimalClass;
import hr.fer.zoovrt.model.ZOOAnimalSpecies;
import hr.fer.zoovrt.model.ZOOIndividual;
import hr.fer.zoovrt.model.ZOOUser;
import hr.fer.zoovrt.rest.structures.AdoptPair;
import hr.fer.zoovrt.rest.structures.VisitPair;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.persistence.EntityManager;

/**
 * JPA DAO implementation class for visitor.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 20.12.2016.
 */
public class JPAVisitorDAO extends JPADAOImpl implements DAOVisitor {

    @Override
    public List<ZOOAnimalClass> getAnimalClasses() throws DAOException {
        EntityManager em = JPAEMProvider.getEntityManager();

        List<ZOOAnimalClass> classes = em.createNamedQuery("Razredzivotinja.findAll",
                ZOOAnimalClass.class).getResultList();
        return classes;
    }

    @Override
    public ZOOIndividual getAnimalIndividual(Integer individualId) throws DAOException {
        if (individualId == null) {
            return null;
        }
        EntityManager em = JPAEMProvider.getEntityManager();

        List<ZOOIndividual> individuals = em
                .createQuery("select u from jedinke as u where u.idJedinke=:IndividualID",
                        ZOOIndividual.class).setParameter("IndividualID", individualId)
                .getResultList();

        if (individuals.size() == 0) {
            return null;
        }

        return individuals.get(0);
    }

    @Override
    public ZOOAnimalSpecies getAnimalSpeciesId(Integer speciesId) throws DAOException {
        if (speciesId == null) {
            return null;
        }
        EntityManager em = JPAEMProvider.getEntityManager();

        List<ZOOAnimalSpecies> individuals = em
                .createQuery("select u from zivotinjskevrste as u where u.idVrste=:SpeciesID",
                        ZOOAnimalSpecies.class).setParameter("SpeciesID", speciesId)
                .getResultList();

        if (individuals.size() == 0) {
            return null;
        }

        return individuals.get(0);
    }

    @Override
    public boolean adoptAnimal(AdoptPair adoptInfo) {
        EntityManager em = JPAEMProvider.getEntityManager();

        if (adoptInfo.getUsername() == null || adoptInfo.getIdJedinke() == null) {
            System.err.println("adopt animal - invalid user");
            return false;
        }

        System.err.println("adopt animal - get user");
        ZOOUser currUser = (ZOOUser) getUser(adoptInfo.getUsername());
        System.err.println("adopt animal - get individual");
        ZOOIndividual zooIndividual = getAnimalIndividual(adoptInfo.getIdJedinke());
        System.err.println("adopt animal - update user");
        currUser.getJedinkes().add(zooIndividual);
        System.err.println("adopt animal - update individual");
        zooIndividual.getKorisniks().add(currUser);

        System.err.println("adopt animal - merge user");
        em.merge(currUser);
        System.err.println("adopt animal - merge individual");
        em.merge(zooIndividual);

        try {
            SimpleMailClient.Send(SimpleMailClient.USERNAME, SimpleMailClient.PASSWORD,
                    currUser.getEmail(), "Uspješno posvojena jedinka", "Uspješno ste posvojili "
                            + zooIndividual.getZivotinjskevrste().getImeVrste() + " - "
                            + zooIndividual.getIme() + "\nS poštovanjem,\nVaš ZOO");
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean visitSpecies(VisitPair visitInfo) {
        EntityManager em = JPAEMProvider.getEntityManager();

        if (visitInfo.getUsername() == null || visitInfo.getSpeciesId() == null) {
            System.err.println("visit species - invalid user");
            return false;
        }

        ZOOUser currUser = (ZOOUser) getUser(visitInfo.getUsername());
        ZOOAnimalSpecies zooSpecies = getAnimalSpeciesId(visitInfo.getSpeciesId());

        Visit newVisit = new Visit();
        newVisit.setKorisnik(currUser);
        newVisit.setKorisnikUserName(currUser.getKorisnickoIme());
        newVisit.setZivotinjskevrste(zooSpecies);
        newVisit.setZivotinjskevrsteId(zooSpecies.getIdVrste());

        currUser.addPosjecenost(newVisit);
        zooSpecies.addPosjecenost(newVisit);

        em.merge(currUser);
        em.merge(zooSpecies);
        em.createNativeQuery(
                "INSERT INTO `zoovrt`.`posjecenost` (`idVrste`, `korisnickoIme`) VALUES ('"
                        + zooSpecies.getIdVrste() + "', '" + currUser.getKorisnickoIme() + "')")
                .executeUpdate();

        return true;
    }

    @Override
    public ZOOAnimalClass getAnimalClass(Integer classId) {
        if (classId == null) {
            return null;
        }
        EntityManager em = JPAEMProvider.getEntityManager();

        List<ZOOAnimalClass> individuals = em
                .createQuery("select u from razredzivotinja as u where u.idRazred=:ClassID",
                        ZOOAnimalClass.class).setParameter("ClassID", classId).getResultList();

        if (individuals.size() == 0) {
            return null;
        }

        return individuals.get(0);
    }

    @Override
    public ZOOAnimalSpecies getSpeciesRecommendation(String username, Integer speciesId) {
        List<ZOOAnimalSpecies> zooSpecies = getAnimalSpecies();
        List<ZOOAnimalSpecies> visitorSpecies = ((ZOOUser) getUser(username)).getPosjecenosts()
                .stream().map(v -> v.getZivotinjskevrste()).collect(Collectors.toList());
        if (zooSpecies == null || zooSpecies.size() == 0) {
            return null;
        }
        if (visitorSpecies == null) {
            visitorSpecies = new ArrayList<ZOOAnimalSpecies>();
        }
        List<ZOOAnimalSpecies> zooSpeciesFiltered = zooSpecies.stream()
                .filter(s -> s.getIdVrste() != speciesId.intValue()).collect(Collectors.toList());
        if (zooSpeciesFiltered.size() == 0) {
            return null;
        }
        List<Integer> visitorSpeciesId = visitorSpecies.stream().map(s -> s.getIdVrste())
                .collect(Collectors.toList());
        if (zooSpecies.size() <= visitorSpecies.size()) {
            int position = ThreadLocalRandom.current().nextInt(zooSpeciesFiltered.size());
            return zooSpeciesFiltered.get(position);

        } else {
            for (ZOOAnimalSpecies spec : zooSpeciesFiltered) {
                if (!visitorSpeciesId.contains(spec.getIdVrste())) {
                    return spec;
                }
            }
        }
        return null;
    }

}
