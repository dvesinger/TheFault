package hr.fer.zoovrt.example;

import hr.fer.zoovrt.dao.DAOProvider;
import hr.fer.zoovrt.dao.jpa.JPAEMFProvider;
import hr.fer.zoovrt.model.Visit;
import hr.fer.zoovrt.model.Visitor;
import hr.fer.zoovrt.model.ZOOIndividual;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Program demonstrates part of dao basic functionality for ZOO Service.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
public class DaoDemo {

    /**
     * Method that starts with the program run.
     *
     * @param args
     *            arguments are ignored
     */
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("database.zoovrt");
        JPAEMFProvider.setEmf(emf);
        List<Visitor> visitors = DAOProvider.getAdminDAO().getAllVisitors();
        for (Visitor v : visitors) {
            System.out.println(v.getIme() + " " + v.getPrezime());

        }

        Visitor v = visitors.get(0);
        System.out.println();
        List<ZOOIndividual> individuals = v.getJedinkes();
        final int newAge = 7;
        for (ZOOIndividual individual : individuals) {
            System.out.println(individual.getIme() + " "
                    + individual.getZivotinjskevrste().getImeVrste() + " " + individual.getDob());

            individual.setDob(newAge);
            DAOProvider.getGuadrianDAO().updateIndividual(individual);
        }

        List<Visit> visits = v.getPosjecenosts();
        System.out.println(visits.size());
        System.out.println(visits.get(0).getZivotinjskevrste().getImeVrste());
        Visit visit = new Visit();
        visit.setIdPosjet(1);
        visit.setZivotinjskevrste(individuals.get(0).getZivotinjskevrste());
        v.addPosjecenost(visit);
        System.out.println(v.getPosjecenosts().size());
        final int newBirthAge = 1995;
        v.setGodinaRod(newBirthAge);

        DAOProvider.getAdminDAO().updateVisitor(v);
        emf.close();

    }

    /**
     * Utility class private constructor.
     */
    private DaoDemo() {
    }
}
