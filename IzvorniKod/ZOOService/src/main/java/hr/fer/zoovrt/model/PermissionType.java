package hr.fer.zoovrt.model;

/**
 * Created by Lovro on 30.12.2016.. enumeration for type of permission used for
 * homeactivity
 * <p>
 * navodno nije dobro koristiti enum kod androida, bolje staticke konstante...
 * TODO provjeri
 */

public enum PermissionType {
    /**
     * Administrator permission.
     */
    ADMIN(1),
    /**
     * Visitor permission.
     */
    VISITOR(3),
    /**
     * Guardian permission.
     */
    GUARDIAN(2);

    /**
     * Permission type id.
     */
    private final int value;

    /**
     * Constructor that initializes permission type id.
     *
     * @param value
     *            permission type id.
     */
    PermissionType(int value) {
        this.value = value;
    }

    /**
     * Method obtains permission type id.
     *
     * @return permission type id
     */
    public int getValue() {
        return value;
    }
}
