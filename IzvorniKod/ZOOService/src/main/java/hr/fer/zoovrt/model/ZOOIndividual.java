package hr.fer.zoovrt.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonManagedReference;

/**
 * The persistent class for the jedinke database table.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 27.12.2016.
 */
@Table(name = "jedinke")
@Entity(name = "jedinke")
@NamedQuery(name = "Jedinke.findAll", query = "SELECT j FROM jedinke j")
@XmlRootElement(name = "individual")
public class ZOOIndividual implements Serializable {

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = -1232373296005565471L;

    /**
     * Individual id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idJedinke;

    /**
     * Individual date from which it has been in the zoo.
     */
    @Temporal(TemporalType.DATE)
    private Date datumDol;

    /**
     * Individual age.
     */
    private Integer dob;

    /**
     * Individual photography URI.
     */
    @Lob
    private String fotografija;

    /**
     * Individual's name.
     */
    private String ime;

    /**
     * Individual's birth place.
     */
    private String mjestoRod;

    /**
     * Individual's gender.
     */
    private String spol;

    /**
     * Animal species that individual belongs.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idVrste")
    @JsonBackReference
    private ZOOAnimalSpecies zivotinjskevrste;

    /**
     * List of adopters.
     */
    @ManyToMany(mappedBy = "jedinkes", fetch = FetchType.EAGER)
    private List<ZOOUser> korisniks;

    /**
     * List of interesting facts.
     */
    @OneToMany(mappedBy = "jedinke", fetch = FetchType.EAGER)
    private List<ZOOInterestingFact> zanimljivostis;

    /**
     * Java bean empty constructor.
     */
    public ZOOIndividual() {
    }

    /**
     * Method adds interesting fact to the individual.
     *
     * @param zanimljivosti
     *            interesting fact
     * @return reference to given interesting fact
     */
    public ZOOInterestingFact addZanimljivosti(ZOOInterestingFact zanimljivosti) {
        if (zanimljivostis == null) {
            zanimljivostis = new ArrayList<ZOOInterestingFact>();
        }
        getZanimljivostis().add(zanimljivosti);
        zanimljivosti.setJedinke(this);

        return zanimljivosti;
    }

    /**
     * Method obtains individual's date from which it has been in the zoo.
     *
     * @return date
     */
    public Date getDatumDol() {
        return datumDol;
    }

    /**
     * Method obtains individual's age.
     *
     * @return individual's age
     */
    public Integer getDob() {
        return dob;
    }

    /**
     * Method obtains individual's photography URI.
     *
     * @return individual's photography URI
     */
    public String getFotografija() {
        return fotografija;
    }

    /**
     * Getter method for individual's id.
     *
     * @return individual's id
     */
    public Integer getIdJedinke() {
        return idJedinke;
    }

    /**
     * Getter method for individual's name.
     *
     * @return individual's name
     */
    public String getIme() {
        return ime;
    }

    /**
     * Method obtains list of adopters.
     *
     * @return list of adopters
     */
    public List<ZOOUser> getKorisniks() {
        return korisniks;
    }

    /**
     * Method obtains individual's birth place.
     *
     * @return individual's birth place
     */
    public String getMjestoRod() {
        return mjestoRod;
    }

    /**
     * Method obtains individual's gender.
     *
     * @return individual's gender
     */
    public String getSpol() {
        return spol;
    }

    /**
     * Method obtains list of individual's interesting facts.
     *
     * @return interesting facts
     */
    public List<ZOOInterestingFact> getZanimljivostis() {
        return zanimljivostis;
    }

    /**
     * Method obtains individual's animal species.
     *
     * @return animal species
     */
    public ZOOAnimalSpecies getZivotinjskevrste() {
        return zivotinjskevrste;
    }

    /**
     * Getter method for animal species id.
     *
     * @return animal species id
     */
    @XmlElement(name = "id_vrste")
    public Integer getZivotinjskevrsteId() {
        return zivotinjskevrste.getIdVrste();
    }

    /**
     * Method sets animal species id. Mock method for xml api.
     *
     * @param ignorable
     *            animal species id.
     */
    public void setZivotinjskevrsteId(Integer ignorable) {
    }

    /**
     * Method removes interesting fact from list of interesting facts.
     *
     * @param zanimljivosti
     *            interesting fact to be removed
     * @return reference to given interesting fact
     */
    public ZOOInterestingFact removeZanimljivosti(ZOOInterestingFact zanimljivosti) {
        getZanimljivostis().remove(zanimljivosti);
        zanimljivosti.setJedinke(null);

        return zanimljivosti;
    }

    /**
     * Method sets date of arrival.
     *
     * @param datumDol
     *            date of arrival
     */
    public void setDatumDol(Date datumDol) {
        this.datumDol = datumDol;
    }

    /**
     * Method sets individual's age.
     *
     * @param dob
     *            individual's age
     */
    public void setDob(Integer dob) {
        this.dob = dob;
    }

    /**
     * Method sets individual's photography URI.
     *
     * @param fotografija
     *            photography URI
     */
    public void setFotografija(String fotografija) {
        this.fotografija = fotografija;
    }

    /**
     * Setter method for individual's id.
     *
     * @param idJedinke
     *            individual's id
     */
    @XmlAttribute
    public void setIdJedinke(Integer idJedinke) {
        this.idJedinke = idJedinke;
    }

    /**
     * Setter method for individual's name.
     *
     * @param ime
     *            individual's name
     */
    @XmlElement
    public void setIme(String ime) {
        this.ime = ime;
    }

    /**
     * Setter method for list of adopters.
     *
     * @param korisniks
     *            list of adopters
     */
    @XmlTransient
    public void setKorisniks(List<ZOOUser> korisniks) {
        this.korisniks = korisniks;
    }

    /**
     * Method sets individual's birth place.
     *
     * @param mjestoRod
     *            birth place
     */
    public void setMjestoRod(String mjestoRod) {
        this.mjestoRod = mjestoRod;
    }

    /**
     * Setter method for individual's gender.
     *
     * @param spol
     *            individual's gender
     */
    @XmlElement
    public void setSpol(String spol) {
        this.spol = spol;
    }

    /**
     * Setter method for list of interesting facts.
     *
     * @param zanimljivostis
     *            list of interesting facts
     */
    @JsonManagedReference
    public void setZanimljivostis(List<ZOOInterestingFact> zanimljivostis) {
        this.zanimljivostis = zanimljivostis;
    }

    /**
     * Method sets species of current animal individual.
     *
     * @param zivotinjskevrste
     *            animal species
     */
    @XmlTransient
    public void setZivotinjskevrste(ZOOAnimalSpecies zivotinjskevrste) {
        this.zivotinjskevrste = zivotinjskevrste;
    }

}
