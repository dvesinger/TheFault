package hr.fer.zoovrt.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Abstract class that models general user attributes.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
@MappedSuperclass
public abstract class User implements Serializable {

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = 6308921911018631089L;

    /**
     * User email.
     */
    private String email;

    /**
     * User birth year.
     */
    private Integer godinaRod;

    /**
     * User's city.
     */
    private String grad;

    /**
     * User's name.
     */
    private String ime;

    /**
     * User's password.
     */
    private String lozinka;

    /**
     * User's last name.
     */
    private String prezime;

    /**
     * User's permissions.
     */
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idOvlasti")
    private Permissions ovlasti;

    /**
     * Java bean empty constructor.
     */
    public User() {

    }

    /**
     * Constructor that initializes user with general user attributes.
     *
     * @param user
     *            user instance
     */
    public User(User user) {
        email = user.email;
        godinaRod = user.godinaRod;
        grad = user.grad;
        ime = user.ime;
        lozinka = user.lozinka;
        prezime = user.prezime;
        ovlasti = user.ovlasti;
    }

    /**
     * Email getter method.
     *
     * @return user's email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Birth year getter method.
     *
     * @return user's birth year
     */
    public Integer getGodinaRod() {
        return godinaRod;
    }

    /**
     * City getter method.
     *
     * @return user's city.
     */
    public String getGrad() {
        return grad;
    }

    /**
     * Name getter method.
     *
     * @return user's name
     */
    public String getIme() {
        return ime;
    }

    /**
     * Method for obtaining user's username.
     *
     * @return user's username
     */
    public abstract String getKorisnickoIme();

    /**
     * Password getter method.
     *
     * @return user's password
     */
    public String getLozinka() {
        return lozinka;
    }

    /**
     * Permissions getter method.
     *
     * @return user's permissions
     */
    public Permissions getOvlasti() {
        return ovlasti;
    }

    /**
     * Getter method for user's last name.
     *
     * @return last name
     */
    public String getPrezime() {
        return prezime;
    }

    /**
     * Method sets user's username.
     *
     * @param korisnickoIme
     *            username
     */
    @XmlElement(name = "korisnicko_ime")
    @JsonProperty("korisnicko_ime")
    public abstract void setKorisnickoIme(String korisnickoIme);

    /**
     * Method sets user's permission.
     *
     * @param ovlasti
     *            permission
     */
    @XmlElement
    public void setOvlasti(Permissions ovlasti) {
        this.ovlasti = ovlasti;
    }

    /**
     * Method sets user's name.
     *
     * @param ime
     *            user's name
     */
    @XmlElement
    public void setIme(String ime) {
        this.ime = ime;
    }

    /**
     * Method sets user's last name.
     *
     * @param prezime
     *            user's last name
     */
    @XmlElement
    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    /**
     * Method sets user's birth year.
     *
     * @param godinaRod
     *            user's birth year.
     */
    @XmlElement
    public void setGodinaRod(Integer godinaRod) {
        this.godinaRod = godinaRod;
    }

    /**
     * Method sets user's email.
     *
     * @param email
     *            user's email
     */
    @XmlElement
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Method sets user's city.
     *
     * @param grad
     *            user's city
     */
    @XmlElement
    public void setGrad(String grad) {
        this.grad = grad;
    }

    /**
     * Method sets user's password.
     *
     * @param lozinka
     *            password
     */
    @XmlElement
    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

}
