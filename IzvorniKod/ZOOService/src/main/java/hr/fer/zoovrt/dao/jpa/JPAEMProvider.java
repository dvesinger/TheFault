package hr.fer.zoovrt.dao.jpa;

import hr.fer.zoovrt.dao.DAOException;

import javax.persistence.EntityManager;

/**
 * JPA entity manager provider class.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.6.2015.
 */
public class JPAEMProvider {

    /**
     * Class that encapsulates enity manager.
     *
     * @author Domagoj Pluscec
     * @version v1.0, 26.6.2015.
     */
    private static class LocalData {
        /**
         * Entity manager.
         */
        private EntityManager em;

    }

    /**
     * Entity manager connection thread map.
     */
    private static ThreadLocal<LocalData> locals = new ThreadLocal<>();

    /**
     * Method closes entity manager connection.
     *
     * @throws DAOException
     *             if there was an error while making data changes.
     */
    public static void close() throws DAOException {
        LocalData ldata = locals.get();
        if (ldata == null) {
            return;
        }
        DAOException dex = null;
        try {
            ldata.em.getTransaction().commit();
        } catch (Exception ex) {
            dex = new DAOException("Unable to commit transaction.", ex);
        }
        try {
            ldata.em.close();
        } catch (Exception ex) {
            if (dex != null) {
                dex = new DAOException("Unable to close entity manager.", ex);
            }
        }
        locals.remove();
        if (dex != null) {
            throw dex;
        }
    }

    /**
     * Method returns entity manager instance.
     *
     * @return entity manager.
     */
    public static EntityManager getEntityManager() {
        LocalData ldata = locals.get();
        if (ldata == null) {
            ldata = new LocalData();
            ldata.em = JPAEMFProvider.getEmf().createEntityManager();
            ldata.em.getTransaction().begin();
            locals.set(ldata);
        }
        return ldata.em;
    }

    /**
     * Private utility class constructor.
     */
    private JPAEMProvider() {
    }

}
