package hr.fer.zoovrt.rest;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * The resource configuration for configuring a web application.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 28.12.2016.
 */
public class ZOOServiceConfig extends ResourceConfig {

    /**
     * Constructor that initializes application configuration.
     */
    public ZOOServiceConfig() {
        register(JacksonFeature.class);
        packages("hr.fer.zoovrt.rest");
    }
}
