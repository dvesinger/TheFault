package hr.fer.zoovrt.dao;

/**
 * Exception that is thrown if there is an error while accessing data object.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.6.2015.
 */
public class DAOException extends RuntimeException {

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * DAO exception constructor which initializes exception message.
     *
     * @param message
     *            exception message
     */
    public DAOException(String message) {
        super(message);
    }

    /**
     * DAO exception constructor which initializes message and cause.
     *
     * @param message
     *            exception message
     * @param cause
     *            exception cause
     */
    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
