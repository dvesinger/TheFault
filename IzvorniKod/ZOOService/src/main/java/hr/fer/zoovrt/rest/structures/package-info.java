/**
 * Package contains structures used for rest communication.
 * @author Domagoj Pluscec
 * @version v1.0, 8.1.2017.
 */
package hr.fer.zoovrt.rest.structures;

