package hr.fer.zoovrt.hash;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Class that implements methods for hashing password.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.6.2015.
 */
public class Hasher {

    /**
     * Encryption type.
     */
    private static final String ENCRYPTION_TYPE = "SHA-1";

    /**
     * Method calculates hash value for given text.
     *
     * @param text
     *            text to hash
     * @return hash value of the text
     */
    public static String calcHash(String text) {

        MessageDigest digest = null;

        try {
            digest = MessageDigest.getInstance(ENCRYPTION_TYPE);
            digest.update(text.getBytes());
        } catch (NoSuchAlgorithmException igonrable) {

        }

        return HexByte.byteArrayToHex(digest.digest());
    }

    /**
     * Utility class private constructor.
     */
    private Hasher() {
    }
}
