package hr.fer.zoovrt.rest.structures;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class for encapsulation of list object responses. Introduced because Java
 * collections need to be encapsulated to be able to put them in xml
 * representation.
 *
 * @author Domagoj Pluscec
 * @version 1.0, 26.12.2016.
 */
@XmlRootElement(name = "list_container")
public class ResponseList {

    /**
     * Object list reference.
     */
    private List<Object> objectList;

    /**
     * Empty constructor for java bean implementation.
     */
    public ResponseList() {
    }

    /**
     * Constructor that initializes object list reference.
     *
     * @param objectList
     *            list of objects
     */
    public ResponseList(List<Object> objectList) {
        this.objectList = objectList;
    }

    /**
     * Getter method for obtaining object list.
     *
     * @return encapsulated object list.
     */
    public List<Object> getList() {
        return objectList;
    }

    /**
     * Setter method for setting object list.
     *
     * @param list
     *            object list to be encapsulated
     */
    @XmlElement
    public void setList(List<Object> list) {
        objectList = list;
    }

}
