package hr.fer.zoovrt.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class that models visitor.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
@Table(name = "zoovrt.posjetitelj")
@Entity(name = "posjetitelj")
@NamedQuery(name = "Posjetitelj.findAll", query = "SELECT p FROM posjetitelj p")
@XmlRootElement(name = "visitor")
public class Visitor extends User implements Serializable {

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = -7879689148658837419L;

    /**
     * Visitor's username.
     */
    @Id
    @Column(name = "korisnickoIme")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String korisnickoIme;

    /**
     * Visitor's list of adopted individuals.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "posvojenja", joinColumns = { @JoinColumn(name = "korisnickoIme") }, inverseJoinColumns = { @JoinColumn(name = "idJedinke") })
    private List<ZOOIndividual> jedinkes;

    /**
     * Visitor's list of visits.
     */
    @OneToMany(mappedBy = "korisnik", fetch = FetchType.EAGER)
    private List<Visit> posjecenosts;

    /**
     * Java bean empty constructor.
     */
    public Visitor() {
    }

    /**
     * Constructor that initializes user with general user attributes.
     *
     * @param user
     *            user instance
     */
    public Visitor(User user) {
        super(user);
    }

    /**
     * Factory method that constructs Visitor user from ZOOUser.
     *
     * @param user
     *            ZOOUser instance
     * @return new Visitor instance
     */
    public static Visitor fromZOOUser(ZOOUser user) {
        Visitor visitor = new Visitor(user);
        visitor.setKorisnickoIme(user.getKorisnickoIme());
        visitor.setJedinkes(user.getJedinkes());
        visitor.setPosjecenosts(user.getPosjecenosts());
        return visitor;
    }

    /**
     * Method adds a visit to list of user's visits.
     *
     * @param posjecenost
     *            visit
     * @return reference to given visit
     */
    public Visit addPosjecenost(Visit posjecenost) {
        if (posjecenosts == null) {
            posjecenosts = new ArrayList<Visit>();
        }
        getPosjecenosts().add(posjecenost);
        posjecenost.setKorisnik(ZOOUser.fromVisitor(this));

        return posjecenost;
    }

    /**
     * Getter method that obtains list of user's adopted individuals.
     *
     * @return list of user's adopted individuals
     */
    public List<ZOOIndividual> getJedinkes() {
        return jedinkes;
    }

    @Override
    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    /**
     * Method obtains list of user's visits.
     *
     * @return list of user's visits
     */
    public List<Visit> getPosjecenosts() {
        return posjecenosts;
    }

    /**
     * Method removes visit from the user.
     *
     * @param posjecenost
     *            visit to be removed
     * @return reference to given visit
     */
    public Visit removePosjecenost(Visit posjecenost) {
        getPosjecenosts().remove(posjecenost);
        posjecenost.setKorisnik(null);

        return posjecenost;
    }

    /**
     * Method sets list of user's adopted individuals.
     *
     * @param jedinkes
     *            list of adopted individuals
     */
    @XmlElementWrapper(name = "list_jedinke")
    @XmlElement
    public void setJedinkes(List<ZOOIndividual> jedinkes) {
        this.jedinkes = jedinkes;
    }

    @Override
    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    /**
     * Method sets users list of visits.
     *
     * @param posjecenosts
     *            list of visits
     */
    @XmlElementWrapper(name = "list_posjecenost")
    @XmlElement
    public void setPosjecenosts(List<Visit> posjecenosts) {
        this.posjecenosts = posjecenosts;
    }
}
