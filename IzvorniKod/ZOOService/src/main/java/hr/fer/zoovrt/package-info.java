/**
 * Main package that contains packages which contains ZOOService implementation.
 * ZOOService represents web client that implements restful web API for ZOO application.
 */
/**
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
package hr.fer.zoovrt;

