package hr.fer.zoovrt.rest.structures;

/**
 * Rest structure used for encapsulating username and species id as part of a
 * visit.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 8.1.2017.
 */
public class VisitPair {
    /**
     * User username.
     */
    private String username;
    /**
     * Species id.
     */
    private Integer speciesId;

    /**
     * Constructor that initializes all attributes.
     *
     * @param username
     *            user's username
     * @param speciesId
     *            species id
     */
    public VisitPair(String username, Integer speciesId) {
        this.username = username;
        this.speciesId = speciesId;
    }

    /**
     * Empty java bean constructor.
     */
    public VisitPair() {
    }

    /**
     * Method obtains user's username.
     *
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Method sets user's username.
     *
     * @param username
     *            user's username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Method obtains species id.
     *
     * @return species id
     */
    public Integer getSpeciesId() {
        return speciesId;
    }

    /**
     * Method sets species Id.
     *
     * @param speciesId
     *            species id
     */
    public void setSpeciesId(Integer speciesId) {
        this.speciesId = speciesId;
    }
}
