package hr.fer.zoovrt.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class that models administrator.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 26.12.2016.
 */
@Table(name = "zoovrt.admin")
@Entity(name = "admin")
@NamedQuery(name = "Admin.findAll", query = "SELECT a FROM admin a")
@XmlRootElement(name = "admin")
public class Admin extends User implements Serializable {

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = -216663928616285709L;

    /**
     * Admin user name.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String korisnickoIme;

    /**
     * Java bean empty constructor.
     */
    public Admin() {
    }

    /**
     * Constructor that initializes user with general user attributes.
     *
     * @param user
     *            user instance
     */
    public Admin(User user) {
        super(user);
    }

    /**
     * Factory method that constructs Admin user from ZOOUser.
     *
     * @param user
     *            ZOOUser instance
     * @return new Admin instance
     */
    public static Admin fromZOOUser(ZOOUser user) {
        Admin admin = new Admin(user);
        admin.setKorisnickoIme(user.getKorisnickoIme());
        return admin;
    }

    @Override
    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    @Override
    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

}
