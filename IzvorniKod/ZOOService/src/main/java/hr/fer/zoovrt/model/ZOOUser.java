package hr.fer.zoovrt.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the korisnik database table.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 27.12.2016.
 */
@Entity(name = "korisnik")
@Table(name = "korisnik", schema = "zoovrt")
@NamedQuery(name = "Korisnik.findAll", query = "SELECT k FROM korisnik k")
public class ZOOUser extends User implements Serializable {

    /**
     * Number that JVM uses for serialization.
     */
    private static final long serialVersionUID = 8261013728894640280L;

    /**
     * Factory method that constructs ZOOUser from visitor.
     *
     * @param visitor
     *            visitor instance
     * @return constructed ZOOUser
     */
    public static ZOOUser fromVisitor(Visitor visitor) {
        ZOOUser user = new ZOOUser(visitor);

        user.setJedinkes(visitor.getJedinkes());
        user.setKorisnickoIme(visitor.getKorisnickoIme());

        user.setPosjecenosts(visitor.getPosjecenosts());
        return user;
    }

    /**
     * ZOOUser's username.
     */
    @Id
    private String korisnickoIme;

    /**
     * List of animal species cares. (used for guardian)
     */
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "brigaovrsti", joinColumns = { @JoinColumn(name = "korisnickoIme") }, inverseJoinColumns = { @JoinColumn(name = "idVrste") })
    private List<ZOOAnimalSpecies> zivotinjskevrstes;

    /**
     * List of animal species visits. (used for visitor)
     */
    @OneToMany(mappedBy = "korisnik")
    private List<Visit> posjecenosts;

    /**
     * List of animal species adoptions. (used for visitor)
     */
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "posvojenja", joinColumns = { @JoinColumn(name = "korisnickoIme") }, inverseJoinColumns = { @JoinColumn(name = "idJedinke") })
    private List<ZOOIndividual> jedinkes;

    /**
     * Java bean empty constructor.
     */
    public ZOOUser() {
    }

    /**
     * Constructor that initializes user with general user attributes.
     *
     * @param user
     *            user instance
     */
    public ZOOUser(User user) {
        super(user);
    }

    /**
     * Method adds a visit to list of user's visits.
     *
     * @param posjecenost
     *            visit
     * @return reference to given visit
     */
    public Visit addPosjecenost(Visit posjecenost) {
        if (posjecenosts == null) {
            posjecenosts = new ArrayList<Visit>();
        }
        getPosjecenosts().add(posjecenost);
        posjecenost.setKorisnik(this);

        return posjecenost;
    }

    /**
     * Getter method that obtains list of user's adopted individuals.
     *
     * @return list of user's adopted individuals
     */
    public List<ZOOIndividual> getJedinkes() {
        return jedinkes;
    }

    @Override
    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    /**
     * Getter method for user's visits.
     *
     * @return list of user's visits
     */
    public List<Visit> getPosjecenosts() {
        return posjecenosts;
    }

    /**
     * Method obtains list of animal species cares.
     *
     * @return list of animal species cares
     */
    public List<ZOOAnimalSpecies> getZivotinjskevrstes() {
        return zivotinjskevrstes;
    }

    /**
     * Method removes visit from the user.
     *
     * @param posjecenost
     *            visit to be removed
     * @return reference to given visit
     */
    public Visit removePosjecenost(Visit posjecenost) {
        getPosjecenosts().remove(posjecenost);
        posjecenost.setKorisnik(null);

        return posjecenost;
    }

    /**
     * Method sets list of user's adopted individuals.
     *
     * @param jedinkes
     *            list of adopted individuals
     */
    public void setJedinkes(List<ZOOIndividual> jedinkes) {
        this.jedinkes = jedinkes;
    }

    @Override
    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    /**
     * Setter method for list of user's visits.
     *
     * @param posjecenosts
     *            list of user's visits
     */
    public void setPosjecenosts(List<Visit> posjecenosts) {
        this.posjecenosts = posjecenosts;
    }

    /**
     * Method sets user's adopted animals.
     *
     * @param zivotinjskevrstes
     *            list of adopted animals
     */
    public void setZivotinjskevrstes(List<ZOOAnimalSpecies> zivotinjskevrstes) {
        this.zivotinjskevrstes = zivotinjskevrstes;
    }

}
