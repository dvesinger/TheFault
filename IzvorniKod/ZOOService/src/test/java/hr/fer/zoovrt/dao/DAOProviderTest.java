package hr.fer.zoovrt.dao;

import java.lang.reflect.Constructor;

import org.junit.Assert;
import org.junit.Test;

/**
 * DAOProvider class unit tests.
 * 
 * @author Domagoj Pluscec
 * @version v1.0, 27.12.2016.
 */
public class DAOProviderTest {

    @Test
    public void getDAOTest() {
        DAO dao = DAOProvider.getDAO();
        Assert.assertTrue(dao instanceof DAO);
        Assert.assertTrue(dao != null);
    }

    @Test
    public void getDAOAdminTest() {
        DAO dao = DAOProvider.getAdminDAO();
        Assert.assertTrue(dao instanceof DAOAdmin);
        Assert.assertTrue(dao != null);
    }

    @Test
    public void getDAOGuardianTest() {
        DAO dao = DAOProvider.getGuadrianDAO();
        Assert.assertTrue(dao instanceof DAOGuardian);
        Assert.assertTrue(dao != null);
    }

    @Test
    public void getDAOVisitorTest() {
        DAO dao = DAOProvider.getVisitorDAO();
        Assert.assertTrue(dao instanceof DAOVisitor);
        Assert.assertTrue(dao != null);
    }

    @Test
    public void testSingleton() {
        @SuppressWarnings("unchecked")
        Constructor<DAOProvider>[] constructors = (Constructor<DAOProvider>[]) DAOProvider.class
                .getDeclaredConstructors();

        for (Constructor<DAOProvider> cons : constructors) {
            Assert.assertFalse(cons.isAccessible());
        }

    }

}
