package hr.fer.zoovrt.dao;

import hr.fer.zoovrt.dao.jpa.JPAAdminDAO;
import hr.fer.zoovrt.dao.jpa.JPADAOImpl;
import hr.fer.zoovrt.dao.jpa.JPAEMFProvider;
import hr.fer.zoovrt.model.Visitor;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Assert;
import org.junit.Test;

/**
 * DAOAdmin class unit tests.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 27.12.2016.
 */
public class DAOAdminTest {

    @Test
    public void interfaceTest() {
        Assert.assertTrue(DAOAdmin.class.isInterface());

        @SuppressWarnings("rawtypes")
        Class[] interfaces = DAOAdmin.class.getInterfaces();
        Assert.assertTrue(interfaces.length == 1);
        Assert.assertTrue(interfaces[0].equals(DAO.class));
    }

    @Test
    public void jPAImplementationTest() {
        Assert.assertFalse(JPAAdminDAO.class.isInterface());
        Assert.assertFalse(JPAAdminDAO.class.isEnum());

        @SuppressWarnings("rawtypes")
        Class[] intefaces = JPAAdminDAO.class.getInterfaces();
        Assert.assertTrue(intefaces.length == 1);
        Assert.assertTrue(intefaces[0].equals(DAOAdmin.class));

        @SuppressWarnings("rawtypes")
        Class parentClasses = JPAAdminDAO.class.getSuperclass();
        Assert.assertTrue(parentClasses.equals(JPADAOImpl.class));

    }

    @Test
    public void getAdminDAOTest() {
        DAOAdmin dao = DAOProvider.getAdminDAO();
        Assert.assertTrue(dao != null);
        Assert.assertTrue(dao instanceof DAOAdmin);
    }

    @Test
    public void getVisitorsTest() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("database.zoovrt");
        JPAEMFProvider.setEmf(emf);
        DAOAdmin dao = DAOProvider.getAdminDAO();
        List<Visitor> visitors = dao.getAllVisitors();
        Assert.assertTrue(visitors != null);
        emf.close();
    }

    @Test
    public void perssistVisitorBirthYearTest() {
        final int newYear = 10;
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("database.zoovrt");
        JPAEMFProvider.setEmf(emf);
        DAOAdmin dao = DAOProvider.getAdminDAO();
        List<Visitor> visitors = dao.getAllVisitors();
        Visitor visitor = visitors.get(0);
        int oldYear = visitor.getGodinaRod();
        visitor.setGodinaRod(newYear);
        dao.updateVisitor(visitor);

        emf = Persistence.createEntityManagerFactory("database.zoovrt");
        JPAEMFProvider.setEmf(emf);
        dao = DAOProvider.getAdminDAO();
        List<Visitor> visitorsSecond = dao.getAllVisitors();
        Visitor visitorSecond = visitorsSecond.get(0);
        Assert.assertTrue(visitorSecond.getGodinaRod().equals(newYear));
        visitorSecond.setGodinaRod(oldYear);
        dao.updateVisitor(visitorSecond);

        emf = Persistence.createEntityManagerFactory("database.zoovrt");
        JPAEMFProvider.setEmf(emf);
        dao = DAOProvider.getAdminDAO();
        List<Visitor> visitorsThird = dao.getAllVisitors();
        Visitor visitorThird = visitorsThird.get(0);
        Assert.assertTrue(visitorThird.getGodinaRod().equals(oldYear));
        emf.close();

    }

}
