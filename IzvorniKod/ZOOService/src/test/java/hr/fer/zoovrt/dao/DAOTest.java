package hr.fer.zoovrt.dao;

import hr.fer.zoovrt.dao.jpa.JPADAOImpl;
import hr.fer.zoovrt.dao.jpa.JPAEMFProvider;
import hr.fer.zoovrt.model.User;
import hr.fer.zoovrt.model.ZOOUser;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Assert;
import org.junit.Test;

/**
 * DAO class unit tests.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 27.12.2016.
 */
public class DAOTest {

    @Test
    public void interfaceTest() {
        Assert.assertTrue(DAO.class.isInterface());

        @SuppressWarnings("rawtypes")
        Class[] interfaces = DAO.class.getInterfaces();
        Assert.assertTrue(interfaces.length == 0);
    }

    @Test
    public void jpaImplementationTest() {
        Assert.assertFalse(JPADAOImpl.class.isInterface());
        Assert.assertFalse(JPADAOImpl.class.isEnum());

        @SuppressWarnings("rawtypes")
        Class[] intefaces = JPADAOImpl.class.getInterfaces();
        Assert.assertTrue(intefaces.length == 1);
        Assert.assertTrue(intefaces[0].equals(DAO.class));

    }

    @Test
    public void getDAOTest() {
        DAO dao = DAOProvider.getDAO();
        Assert.assertTrue(dao != null);
        Assert.assertTrue(dao instanceof DAO);
    }

    @Test
    public void getUserTestOne() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("database.zoovrt");
        JPAEMFProvider.setEmf(emf);
        DAO dao = DAOProvider.getDAO();
        ZOOUser user = (ZOOUser) dao.getUser("Hacker");

        Assert.assertTrue(user != null);
        Assert.assertTrue(user.getKorisnickoIme().equals("Hacker"));
        Assert.assertTrue(user.getIme().equals("Domagoj"));
        Assert.assertTrue(user.getPrezime().equals("Pluscec"));
        Assert.assertTrue(user.getLozinka().equals("pass"));
        Assert.assertTrue(user.getOvlasti() != null);
        final int visitorPermission = 3;
        Assert.assertTrue(user.getOvlasti().getIdOvlasti() == visitorPermission);
        Assert.assertTrue(user.getGrad().equals("Zagreb"));
        final int birthYear = 1995;
        Assert.assertTrue(user.getGodinaRod().equals(birthYear));
        Assert.assertTrue(user.getPosjecenosts() != null);
        Assert.assertTrue(user.getZivotinjskevrstes() != null);
        Assert.assertTrue(user.getZivotinjskevrstes().size() == 0);
        Assert.assertTrue(user.getJedinkes() != null);
        Assert.assertTrue(user.getJedinkes().size() == 0);

    }

    @Test
    public void getUserTestTwo() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("database.zoovrt");
        JPAEMFProvider.setEmf(emf);
        DAO dao = DAOProvider.getDAO();
        ZOOUser user = (ZOOUser) dao.getUser("NonexistingUser");
        Assert.assertTrue(user == null);
    }

    @Test
    public void checkUserTestOne() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("database.zoovrt");
        JPAEMFProvider.setEmf(emf);
        DAO dao = DAOProvider.getDAO();
        User validUser = new ZOOUser();
        validUser.setKorisnickoIme("Hacker");
        validUser.setLozinka("pass");
        Assert.assertTrue(dao.checkUser(validUser));
    }

    @Test
    public void checkUserTestTwo() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("database.zoovrt");
        JPAEMFProvider.setEmf(emf);
        DAO dao = DAOProvider.getDAO();
        User notValidUser = new ZOOUser();
        notValidUser.setKorisnickoIme("Hacker");
        notValidUser.setLozinka("wrong_pass");
        Assert.assertFalse(dao.checkUser(notValidUser));
    }

    @Test
    public void checkUserTestThree() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("database.zoovrt");
        JPAEMFProvider.setEmf(emf);
        DAO dao = DAOProvider.getDAO();
        User notValidUser = new ZOOUser();
        notValidUser.setKorisnickoIme("NonExistinguSER");
        notValidUser.setLozinka("pass");
        Assert.assertFalse(dao.checkUser(notValidUser));
    }
}
