package hr.fer.zoovrt.model;

import org.junit.Assert;
import org.junit.Test;

/**
 * User class unit tests.
 *
 * @author Domagoj Pluscec
 * @version v1.0, 27.12.2016.
 */
public class UserTest {

    @Test
    public void emptyConstructorTest() {
        User user = new User() {

            private static final long serialVersionUID = -331106589254631631L;

            @Override
            public void setKorisnickoIme(String korisnickoIme) {
            }

            @Override
            public String getKorisnickoIme() {
                return null;
            }
        };

        Assert.assertTrue(user != null);
        Assert.assertTrue(user.getEmail() == null);
        Assert.assertTrue(user.getGrad() == null);
        Assert.assertTrue(user.getIme() == null);
        Assert.assertTrue(user.getKorisnickoIme() == null);
        Assert.assertTrue(user.getLozinka() == null);
        Assert.assertTrue(user.getPrezime() == null);
        Assert.assertTrue(user.getGodinaRod() == null);
        Assert.assertTrue(user.getOvlasti() == null);
    }

    @Test
    public void userConstructorTestOne() {
        User user = new User(new ZOOUser()) {

            private static final long serialVersionUID = 1L;

            @Override
            public String getKorisnickoIme() {
                return null;
            }

            @Override
            public void setKorisnickoIme(String korisnickoIme) {

            }

        };

        Assert.assertTrue(user != null);
        Assert.assertTrue(user.getEmail() == null);
        Assert.assertTrue(user.getGrad() == null);
        Assert.assertTrue(user.getIme() == null);
        Assert.assertTrue(user.getKorisnickoIme() == null);
        Assert.assertTrue(user.getLozinka() == null);
        Assert.assertTrue(user.getPrezime() == null);
        Assert.assertTrue(user.getGodinaRod() == null);
        Assert.assertTrue(user.getOvlasti() == null);
    }

    @Test
    public void userConstructorTestTwo() {
        ZOOUser zooUser = new ZOOUser();
        zooUser.setEmail("email");
        zooUser.setGodinaRod(1221);
        zooUser.setIme("ime");
        zooUser.setPrezime("prez");
        zooUser.setLozinka("pass");
        zooUser.setGrad("grad");
        Permissions permis = new Permissions();
        permis.addKorisnik(zooUser);
        permis.setIdOvlasti(1);
        permis.setNazivOvlasti("Admin");
        zooUser.setOvlasti(permis);
        User user = new User(zooUser) {

            /**
             *
             */
            private static final long serialVersionUID = -6558860080475818634L;

            @Override
            public String getKorisnickoIme() {
                return null;
            }

            @Override
            public void setKorisnickoIme(String korisnickoIme) {

            }

        };

        Assert.assertTrue(user != null);
        Assert.assertTrue(user.getEmail().equals("email"));
        Assert.assertTrue(user.getGrad().equals("grad"));
        Assert.assertTrue(user.getIme().equals("ime"));
        Assert.assertTrue(user.getLozinka().equals("pass"));
        Assert.assertTrue(user.getPrezime().equals("prez"));
        Assert.assertTrue(user.getGodinaRod().equals(1221));
        Assert.assertTrue(user.getOvlasti() != null);
        Assert.assertTrue(user.getOvlasti().getIdOvlasti() == 1);
        Assert.assertTrue(user.getOvlasti().getNazivOvlasti().equals("Admin"));
        Assert.assertTrue(user.getOvlasti().getKorisniks().get(0).equals(user));
    }

}
