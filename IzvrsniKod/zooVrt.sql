CREATE DATABASE  IF NOT EXISTS `zoovrt` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_general_mysql500_ci */;
USE `zoovrt`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: zoovrt
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!50001 DROP VIEW IF EXISTS `admin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `admin` AS SELECT 
 1 AS `korisnickoIme`,
 1 AS `lozinka`,
 1 AS `idOvlasti`,
 1 AS `ime`,
 1 AS `prezime`,
 1 AS `email`,
 1 AS `godinaRod`,
 1 AS `grad`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `brigaovrsti`
--

DROP TABLE IF EXISTS `brigaovrsti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brigaovrsti` (
  `korisnickoIme` char(50) COLLATE latin2_croatian_ci NOT NULL,
  `idVrste` int(11) NOT NULL,
  PRIMARY KEY (`korisnickoIme`,`idVrste`),
  KEY `idVrste_idx` (`idVrste`),
  CONSTRAINT `idVrsteBriga` FOREIGN KEY (`idVrste`) REFERENCES `zivotinjskevrste` (`idVrste`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `korisnickoImeBriga` FOREIGN KEY (`korisnickoIme`) REFERENCES `korisnik` (`korisnickoIme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_croatian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brigaovrsti`
--

LOCK TABLES `brigaovrsti` WRITE;
/*!40000 ALTER TABLE `brigaovrsti` DISABLE KEYS */;
INSERT INTO `brigaovrsti` VALUES ('AnimalLover',1),('GreenGirl',1),('jane',1),('AnimalLover',2),('GreenGirl',2),('jane',3),('PrincessCool',3),('jane',4),('KingOfThieves',4),('Cuvar',5),('Cuvar',6),('EvilRuler',6),('AnimalLover',7),('EvilRuler',8),('GreenGirl',9),('GreenGirl',10),('EvilRuler',11);
/*!40000 ALTER TABLE `brigaovrsti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `cuvar`
--

DROP TABLE IF EXISTS `cuvar`;
/*!50001 DROP VIEW IF EXISTS `cuvar`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `cuvar` AS SELECT 
 1 AS `korisnickoIme`,
 1 AS `lozinka`,
 1 AS `idOvlasti`,
 1 AS `ime`,
 1 AS `prezime`,
 1 AS `email`,
 1 AS `godinaRod`,
 1 AS `grad`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (1);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jedinke`
--

DROP TABLE IF EXISTS `jedinke`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jedinke` (
  `idJedinke` int(11) NOT NULL AUTO_INCREMENT,
  `idVrste` int(11) NOT NULL,
  `ime` char(50) COLLATE latin2_croatian_ci NOT NULL,
  `dob` tinyint(3) unsigned DEFAULT NULL,
  `spol` char(1) COLLATE latin2_croatian_ci DEFAULT NULL,
  `mjestoRod` char(50) COLLATE latin2_croatian_ci DEFAULT NULL,
  `datumDol` date DEFAULT NULL,
  `fotografija` mediumtext COLLATE latin2_croatian_ci,
  PRIMARY KEY (`idJedinke`),
  KEY `idVrste_idx` (`idVrste`),
  CONSTRAINT `idVrste` FOREIGN KEY (`idVrste`) REFERENCES `zivotinjskevrste` (`idVrste`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin2 COLLATE=latin2_croatian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jedinke`
--

LOCK TABLES `jedinke` WRITE;
/*!40000 ALTER TABLE `jedinke` DISABLE KEYS */;
INSERT INTO `jedinke` VALUES (1,3,'Rajah',7,'M',NULL,NULL,'{BASE_URL}ZOOService/images/individuals/1.jpg'),(2,4,'Abu',NULL,'M',NULL,NULL,'{BASE_URL}ZOOService/images/individuals/2.jpg'),(3,5,'Kala',NULL,'F',NULL,NULL,'{BASE_URL}ZOOService/images/individuals/3.jpg'),(4,1,'Skočko',5,'F','Hrvatsko zagorje','2007-09-09','{BASE_URL}ZOOService/images/individuals/4.jpg'),(5,2,'Malena',7,'F',NULL,NULL,'{BASE_URL}ZOOService/images/individuals/5.jpg'),(6,1,'Spavko',3,'M','Zagreb',NULL,''),(9,1,'Mimi',2,NULL,'Zagreb',NULL,''),(10,1,'Klupko',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `jedinke` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `korisnik`
--

DROP TABLE IF EXISTS `korisnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `korisnik` (
  `korisnickoIme` char(50) COLLATE latin2_croatian_ci NOT NULL,
  `lozinka` char(50) COLLATE latin2_croatian_ci NOT NULL,
  `idOvlasti` int(11) NOT NULL,
  `ime` char(50) COLLATE latin2_croatian_ci NOT NULL,
  `prezime` char(50) COLLATE latin2_croatian_ci NOT NULL,
  `email` char(50) COLLATE latin2_croatian_ci NOT NULL,
  `godinaRod` year(4) NOT NULL,
  `grad` char(50) COLLATE latin2_croatian_ci NOT NULL,
  PRIMARY KEY (`korisnickoIme`),
  KEY `idOvlasti_idx` (`idOvlasti`),
  CONSTRAINT `idOvlasti` FOREIGN KEY (`idOvlasti`) REFERENCES `ovlasti` (`idOvlasti`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_croatian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `korisnik`
--

LOCK TABLES `korisnik` WRITE;
/*!40000 ALTER TABLE `korisnik` DISABLE KEYS */;
INSERT INTO `korisnik` VALUES ('Administrator','pass',1,'a','a','a@a.com',1997,'a'),('AnimalLover','noapplesplease',2,'Snow','White','snow.white@disneyland.com',1937,'FarFarAway'),('Author','p',2,'Dunja','Vesinger','jenny.eminalld@hotmail.com',1995,'Zagreb'),('Cuvar','pass',2,'c','c','c@c.com',1996,'c'),('domi','pass',3,'Domagoj','Pluscec','domi385@gmail.com',1995,'Zagreb'),('EvilRuler','muahaha',2,'Jafar','Jamal','grand.vizir@disneyland.com',1992,'Agrabah'),('GreenGirl','frogsrock',2,'Tiana','Maldonia','princess.tiana@disneyland.com',2009,'Maldonia'),('jane','ilovetarzan',2,'jane','porter','jane.porter@dinsleyland.com',1921,'maryland'),('KingOfThieves','genie',2,'Aladdin','Aladdin','aladdin@disneyland.com',1992,'Agrabah'),('MagicKing','magickingdom',1,'Walt','Disney','Disney@disneyland.com',1950,'Disney'),('Posjetitelj','pass',3,'p','p','p@p.com',1990,'p'),('PrincessCool','independence',2,'Jasmine','Aladdin','princesscool@disneyland.com',1992,'Agrabah'),('željko','1210964',3,'ž','v','zvesinger@gmail.com',1964,'zg');
/*!40000 ALTER TABLE `korisnik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ovlasti`
--

DROP TABLE IF EXISTS `ovlasti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ovlasti` (
  `idOvlasti` int(11) NOT NULL AUTO_INCREMENT,
  `nazivOvlasti` char(15) COLLATE latin2_croatian_ci NOT NULL,
  PRIMARY KEY (`idOvlasti`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin2 COLLATE=latin2_croatian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ovlasti`
--

LOCK TABLES `ovlasti` WRITE;
/*!40000 ALTER TABLE `ovlasti` DISABLE KEYS */;
INSERT INTO `ovlasti` VALUES (1,'Administrator'),(2,'Čuvar'),(3,'Posjetitelj');
/*!40000 ALTER TABLE `ovlasti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posjecenost`
--

DROP TABLE IF EXISTS `posjecenost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posjecenost` (
  `idPosjet` int(11) NOT NULL AUTO_INCREMENT,
  `idVrste` int(11) NOT NULL,
  `korisnickoIme` char(50) COLLATE latin2_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`idPosjet`),
  KEY `idVrste_idx` (`idVrste`),
  KEY `korisnickoIme_idx` (`korisnickoIme`),
  CONSTRAINT `idVrstePosjecenost` FOREIGN KEY (`idVrste`) REFERENCES `zivotinjskevrste` (`idVrste`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `korisnickoImePosjecenost` FOREIGN KEY (`korisnickoIme`) REFERENCES `korisnik` (`korisnickoIme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin2 COLLATE=latin2_croatian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posjecenost`
--

LOCK TABLES `posjecenost` WRITE;
/*!40000 ALTER TABLE `posjecenost` DISABLE KEYS */;
INSERT INTO `posjecenost` VALUES (7,1,'Author'),(8,5,'Author'),(13,1,'domi'),(15,5,'domi'),(16,2,'domi'),(17,3,'domi'),(18,3,'domi'),(19,3,'domi'),(20,3,'domi'),(21,3,'domi'),(22,3,'domi'),(23,3,'domi'),(24,3,'domi'),(25,4,'domi'),(46,6,'željko'),(47,7,'željko');
/*!40000 ALTER TABLE `posjecenost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `posjetitelj`
--

DROP TABLE IF EXISTS `posjetitelj`;
/*!50001 DROP VIEW IF EXISTS `posjetitelj`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `posjetitelj` AS SELECT 
 1 AS `korisnickoIme`,
 1 AS `lozinka`,
 1 AS `idOvlasti`,
 1 AS `ime`,
 1 AS `prezime`,
 1 AS `email`,
 1 AS `godinaRod`,
 1 AS `grad`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `posvojenja`
--

DROP TABLE IF EXISTS `posvojenja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posvojenja` (
  `korisnickoIme` char(50) COLLATE latin2_croatian_ci NOT NULL,
  `idJedinke` int(11) NOT NULL,
  PRIMARY KEY (`korisnickoIme`,`idJedinke`),
  KEY `idJedinke_idx` (`idJedinke`),
  CONSTRAINT `idJedinkePosvojenje` FOREIGN KEY (`idJedinke`) REFERENCES `jedinke` (`idJedinke`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `korisnickoImePosvojenje` FOREIGN KEY (`korisnickoIme`) REFERENCES `korisnik` (`korisnickoIme`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_croatian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posvojenja`
--

LOCK TABLES `posvojenja` WRITE;
/*!40000 ALTER TABLE `posvojenja` DISABLE KEYS */;
INSERT INTO `posvojenja` VALUES ('domi',2),('domi',3),('domi',4),('domi',6);
/*!40000 ALTER TABLE `posvojenja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `razredzivotinja`
--

DROP TABLE IF EXISTS `razredzivotinja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `razredzivotinja` (
  `idRazred` int(11) NOT NULL AUTO_INCREMENT,
  `nazivRazreda` char(15) COLLATE latin2_croatian_ci NOT NULL,
  PRIMARY KEY (`idRazred`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin2 COLLATE=latin2_croatian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `razredzivotinja`
--

LOCK TABLES `razredzivotinja` WRITE;
/*!40000 ALTER TABLE `razredzivotinja` DISABLE KEYS */;
INSERT INTO `razredzivotinja` VALUES (1,'sisavci'),(2,'ptice'),(3,'gmazovi'),(4,'vodozemci'),(5,'ribe'),(6,'kukci');
/*!40000 ALTER TABLE `razredzivotinja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vrstazanimljivosti`
--

DROP TABLE IF EXISTS `vrstazanimljivosti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vrstazanimljivosti` (
  `idVrsteZanimljivosti` int(11) NOT NULL AUTO_INCREMENT,
  `vrstaZanimljivosti` char(15) COLLATE latin2_croatian_ci NOT NULL,
  PRIMARY KEY (`idVrsteZanimljivosti`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin2 COLLATE=latin2_croatian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vrstazanimljivosti`
--

LOCK TABLES `vrstazanimljivosti` WRITE;
/*!40000 ALTER TABLE `vrstazanimljivosti` DISABLE KEYS */;
INSERT INTO `vrstazanimljivosti` VALUES (1,'tekst'),(2,'slika'),(3,'video');
/*!40000 ALTER TABLE `vrstazanimljivosti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zanimljivosti`
--

DROP TABLE IF EXISTS `zanimljivosti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zanimljivosti` (
  `idZanimljivosti` int(11) NOT NULL AUTO_INCREMENT,
  `idJedinke` int(11) NOT NULL,
  `zanimljivost` text COLLATE latin2_croatian_ci NOT NULL,
  `idVrstaZanimljivosti` int(11) NOT NULL,
  `datum` date NOT NULL,
  PRIMARY KEY (`idZanimljivosti`),
  KEY `idJedinle_idx` (`idJedinke`),
  KEY `idVrstaZanimljivosti_idx` (`idVrstaZanimljivosti`),
  CONSTRAINT `idJedinle` FOREIGN KEY (`idJedinke`) REFERENCES `jedinke` (`idJedinke`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `idVrstaZanimljivosti` FOREIGN KEY (`idVrstaZanimljivosti`) REFERENCES `vrstazanimljivosti` (`idVrsteZanimljivosti`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin2 COLLATE=latin2_croatian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zanimljivosti`
--

LOCK TABLES `zanimljivosti` WRITE;
/*!40000 ALTER TABLE `zanimljivosti` DISABLE KEYS */;
INSERT INTO `zanimljivosti` VALUES (1,1,'{BASE_URL}ZOOService/images/facts/1.jpg',2,'2017-01-02'),(2,6,'Voli spavati',1,'2017-01-07'),(3,6,'{BASE_URL}ZOOService/images/facts/3.jpg',2,'2017-01-07'),(4,10,'https://www.youtube.com/watch?v=-8VL4PHfyM8',3,'2017-01-02'),(6,5,'Rep malene je dugačak 14 cm.',1,'2017-01-12'),(7,5,'{BASE_URL}ZOOService/images/facts/7.png',2,'2017-01-12');
/*!40000 ALTER TABLE `zanimljivosti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zivotinjskevrste`
--

DROP TABLE IF EXISTS `zivotinjskevrste`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zivotinjskevrste` (
  `idVrste` int(11) NOT NULL AUTO_INCREMENT,
  `imeVrste` char(50) COLLATE latin2_croatian_ci NOT NULL,
  `idRazredVrste` int(11) NOT NULL,
  `opisVrste` text COLLATE latin2_croatian_ci,
  `fotografija` mediumtext COLLATE latin2_croatian_ci,
  PRIMARY KEY (`idVrste`),
  KEY `idRazredVrste_idx` (`idRazredVrste`),
  CONSTRAINT `idRazredVrste` FOREIGN KEY (`idRazredVrste`) REFERENCES `razredzivotinja` (`idRazred`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin2 COLLATE=latin2_croatian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zivotinjskevrste`
--

LOCK TABLES `zivotinjskevrste` WRITE;
/*!40000 ALTER TABLE `zivotinjskevrste` DISABLE KEYS */;
INSERT INTO `zivotinjskevrste` VALUES (1,'Divlji kunić',1,'Lat. Oryctolagus cuniculus\nPorodica: Zečevi\nPodrijetlo: Južna Europa\nStanište: polja na kojima u skupinama kopaju tunele\nIshrana: kora mladog drveća, plodovi, pupoljci..\nObilježja: 40cm, 2kg, snažna osjetila, sivo krzno, crno \nbijeli rep, noćna životinja, život u skupinama od 10-30 \n','{BASE_URL}ZOOService/images/species/1.jpg'),(2,'Crvena vjeverica',1,'lat. Sciurus vulgaris\nRed: glodavci\nPodrijetlo: Europa\nStanište: parkovi i šume,\ngrade gnijezda u krošnjama\nIshrana: lješnjaci, orašasti plodovi\nObilježja: kitnjasti rep \ndugačak oko 20 cm,\nspremištanje hrane za zimu, \nvijek 8-10 god, može\nopstati i u podzemnim jazbinama','{BASE_URL}ZOOService/images/species/2.jpg'),(3,'Bengalski tigar',1,'lat.Panthera tigris \nPorodica: mačaka\nPodrijetlo: Indija i obližnji otoci\nStanište: šume i prašume\nIshrana: prevladavaju bilovi i jeleni\nObilježja: ugrožen, 130-265 kg, crveno \nzlatan s crnim prugama i bijelim trbuhom,\nlovci s brzinom do 90 km/h,\ndvogodišnja briga majke o mladuncima','{BASE_URL}ZOOService/images/species/3.jpg'),(4,'Smeđi kapucin',1,'lat. Cebus apella\nPorodica: širokonošci\nPodrijetlo: Sjeverna i Srednja Amerika\nStanište: šume, uglavnom na krošnjama\nIshrana: bilje, kukci, manji sisavci\nObilježja: malo tijelo i dugi rep koji \nmu služi za prihvaćanje, golo lice te\nfacijalna ekspresija, glasa se oštrim\nzviždanjem','{BASE_URL}ZOOService/images/species/4.jpg'),(5,'Brdska gorila',1,'lat. Gorilla gorilla\nPorodica: čovjekoliki majmuni\nPodrijetlo: Središnja Afrika\nStanište: sekundarne šume, obronci vulkana\nIshrana: lišće i kukci\nObilježja: dnevne životinje, grade gnijezda\nna tlu u kojima spavaju, robustne građe,\nduge svilenkaste sive dlake izuzev lica,\nizražen luk iznad očiju, život u skupinama','{BASE_URL}ZOOService/images/species/5.jpg'),(6,'Zelenokrila ara',2,'lat.Ara chloroptera\nRed: Papigašice\nPodrijetlo: Južna i Središnja Amerika\nStanište: zone tropskih šuma do 450 m\nIshrana: voće, orasi, sjmemenje\nObilježja: visine do 90 cm, crveno tijelo, \nplavo-zelena krila, život u paru ili manjoj \nskupini, snažni udarci krila, grubo skričanje,\ngrade gnijezda. \n','{BASE_URL}ZOOService/images/species/6.jpg'),(7,'Velika ušara',2,'lat. Bubo bubo\nPorodica: Prave sove\nPodrijetlo: Europa\nStanište: stare šume, brdsko-planinske doline\nIshrana: ježevi, ptice, mali sisavci\nObilježja: najveća sova od 59 do 73 cm,\nraspona krila 160-190 cm, krupna, snažnih\nkandži, narančastih očiju i izraženih ušiju,\npoznata po glasnom huktanju kojim \nmužjaci označavaju teritorij','{BASE_URL}ZOOService/images/species/7.jpg'),(8,'Kraljevska kobra',3,'lat. Ophiophagus hannah\nPorodica: guje\nPodrijetlo: Južna Azija\nStanište: kišne šume i ravnice\nIshrana: druge zmije\nObilježja: može narasti do 5.6 metara i\ntežiti do 9 kg, ljuskastog tijela,\notrovnica, mogu širiti vrat \nda oblikuju klobuk','{BASE_URL}ZOOService/images/species/8.jpg'),(9,'Zlatna otrovna žaba',4,'lat. Phyllobates terribilis\nRed: Bezrepci\nPodrijetlo: Južna i Srednja Amerika\nStanište: kišne šume\nIshrana: mravi, cvrčci, termiti\nObilježja: endemska vrsta, otrovnica,\nglamoruzno žuto boje kojom upozorava\nna opasnost, u slučaju opasnosti koža\njoj ispušta otrov, veličine do 5 cm\n','{BASE_URL}ZOOService/images/species/9.jpg'),(10,'Riba klaun',5,'lat. Amphiprionpercula\nPorodica: Čeljoustke\nPodrijetlo: Indijski i Tihi ocean\nStanište: koraljni grebeni\nIshrana: alge, zooplanktoni\nObilježja:živi u simbiozi s vlasuljama\nkoje joj pružaju zaklon te ju\nopskrbljuje ostacima svoje hrane,\na riba odbija napadače od vlasulje, \nčisti je od nametnika te \npoboljšava strujanje vode \n','{BASE_URL}ZOOService/images/species/10.jpg'),(11,'Skakavac selac',6,'lat.Locusta migratoria\nPorodica: Insekti\nPodrijetlo: Afrika, Australija\nStanište: polja i livade\nIshrana: dijelovi biljaka\nObilježja: malen, zelenkasto-siv, kako se broj jedinki u populaciji povećava prelaze iz samotnjačke faze u zadružnu i počinju se rojiti\n','{BASE_URL}ZOOService/images/species/11.jpg');
/*!40000 ALTER TABLE `zivotinjskevrste` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'zoovrt'
--

--
-- Dumping routines for database 'zoovrt'
--

--
-- Final view structure for view `admin`
--

/*!50001 DROP VIEW IF EXISTS `admin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `admin` AS select `korisnik`.`korisnickoIme` AS `korisnickoIme`,`korisnik`.`lozinka` AS `lozinka`,`korisnik`.`idOvlasti` AS `idOvlasti`,`korisnik`.`ime` AS `ime`,`korisnik`.`prezime` AS `prezime`,`korisnik`.`email` AS `email`,`korisnik`.`godinaRod` AS `godinaRod`,`korisnik`.`grad` AS `grad` from `korisnik` where (`korisnik`.`idOvlasti` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `cuvar`
--

/*!50001 DROP VIEW IF EXISTS `cuvar`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `cuvar` AS select `korisnik`.`korisnickoIme` AS `korisnickoIme`,`korisnik`.`lozinka` AS `lozinka`,`korisnik`.`idOvlasti` AS `idOvlasti`,`korisnik`.`ime` AS `ime`,`korisnik`.`prezime` AS `prezime`,`korisnik`.`email` AS `email`,`korisnik`.`godinaRod` AS `godinaRod`,`korisnik`.`grad` AS `grad` from `korisnik` where (`korisnik`.`idOvlasti` = 2) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `posjetitelj`
--

/*!50001 DROP VIEW IF EXISTS `posjetitelj`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `posjetitelj` AS select `korisnik`.`korisnickoIme` AS `korisnickoIme`,`korisnik`.`lozinka` AS `lozinka`,`korisnik`.`idOvlasti` AS `idOvlasti`,`korisnik`.`ime` AS `ime`,`korisnik`.`prezime` AS `prezime`,`korisnik`.`email` AS `email`,`korisnik`.`godinaRod` AS `godinaRod`,`korisnik`.`grad` AS `grad` from `korisnik` where (`korisnik`.`idOvlasti` = 3) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-12 16:25:34
